//
//  MapPinCustomImageView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/31/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapPinCustomImageView : UIImageView

@end
