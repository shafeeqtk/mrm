//
//  RouteResultContentView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/23/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RouteResultContentViewDelegate;

@interface RouteResultContentView : UIView

@property (nonatomic, assign) id<RouteResultContentViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *headerInfoView;
@property (strong, nonatomic) IBOutlet UIView *footerInfoView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headerViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *footerViewHeightConstraint;

@property (strong, nonatomic) NSMutableArray *stationsList;

@property int startStationId;
@property int endStationId;
@property int lineId;
@property int timeInMins;
@property (strong, nonatomic) NSString *direction;


@property (strong, nonatomic) IBOutlet UILabel *departureTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *startStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *endStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *startStationPlatformNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *endStationPlatformNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *endStationPlatformSideLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *endStationPlatformSideWidthConstraint;

@property (strong, nonatomic) IBOutlet UIView *trainInfoView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *trainInfoViewTrailingSpaceConstraint;


@property (strong, nonatomic) IBOutlet UILabel *trainInfoTowardsStationCode;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoSpeedLabel;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoTowardsStationName;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoFromStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoNoOfCar;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoSpecialTrainInfo;
@property (strong, nonatomic) IBOutlet UILabel *trainInfoStopsAndTimeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *trainInfoToggleRouteDetailsImageView;


@property (strong, nonatomic) IBOutlet UIButton *trainInfoToggleRouteDetailsButton;
@property (strong, nonatomic) IBOutlet UIButton *trainInfoShowMoreTrainsButton;


@property (strong, nonatomic) IBOutlet UIView *intermediateStationsView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *intermediateStationsListHeightConstraint;

@property (strong, nonatomic) IBOutlet UIImageView *lineColorImageView;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *stationMarkerImageViews;

@property (strong, nonatomic) UIColor *lineColor;

@property bool tryingToOpenStationsList;
@property float eachStationHeight;

-(void) addIntermediateStations;
-(void)expandIntermediateStationsListWithToggleFlag:(bool)toggleFlag;
-(void)addStationToList:(NSString *) stationName atTime:(NSString *)time;
-(void)setLineColorForLine:(UIColor *)lineColor;
@end




@protocol RouteResultContentViewDelegate <NSObject>

@optional

-(void)didClickExpandStations:(int)atSubViewIndex withHeightChange:(float)changedValue;
-(void)loadAdvancedTrainSelectionTableViewFromStationId:(int)fromStationId toStationId:(int)toStationId onLineId:(int)lineId atTimeInMins:(int)timeInMins inDirection:(NSString *)direction;

@end
