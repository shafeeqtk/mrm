//
//  StatsDump.m
//  MRM
//
//  Created by Shafeeq Rahiman on 2/21/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import "StatsDump.h"


@implementation StatsDump

@dynamic srcId;
@dynamic destId;
@dynamic jxIdList;
@dynamic jxStrList;
@dynamic isFav;
@dynamic active;
@dynamic addedTs;
@dynamic lastModifiedTs;

@end
