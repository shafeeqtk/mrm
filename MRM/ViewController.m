//
//  ViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 10/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ViewController.h"
#import "PageContentViewController.h"

@interface ViewController()
@property (strong, nonatomic) IBOutlet UIButton *finishButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property NSUInteger currentPageIndex;
@end


@implementation ViewController

enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self findPhoneType];

//    [self checkTutorialDisplayNeeded];

    // Do any additional setup after loading the view, typically from a nib.

//    UIButton *btn = [[UIButton alloc] init];
//    [btn setTitle:@"Actionsheet" forState:UIControlStateNormal];
    
    self.currentPageIndex = 0;
    self.nextButton.hidden = YES;
    self.finishButton.hidden = NO;

    
    
//        self.navigationController.navigationBarHidden = YES;
    
        _pageTitles = @[@"One", @"Two", @"Three", @"Four", @"Five", @"Six"];
        _pageImages = @[@"MrmApp_iOS_intro1.jpg", @"MrmApp_iOS_intro2.jpg", @"MrmApp_iOS_intro3.jpg", @"MrmApp_iOS_intro4.jpg", @"MrmApp_iOS_intro5.jpg", @"MrmApp_iOS_intro6.jpg"];
        
        // Create page view controller
        self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        self.pageViewController.dataSource = self;
        
        PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        // Change the size of page view controller
        self.pageViewController.view.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height - 100 );
        [self addChildViewController:_pageViewController];
        [self.view addSubview:_pageViewController.view];
        [self.pageViewController didMoveToParentViewController:self];
        
        UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 60, self.view.frame.size.width, 60)];
        UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height - 50, 60,40)];
        doneButton.titleLabel.text = @"Done";
        [self.view addSubview:vw];
        
        //        for (UIView *view in self.pageViewController.view.subviews ) {
        //            if ([view isKindOfClass:[UIScrollView class]]) {
        //                UIScrollView *scroll = (UIScrollView *)view;
        //                scroll.bounces = NO;
        //            }
        //        }
        
        
        //self.navigationController.navigationBarHidden = NO;
        
        for ( UIView *subview in [self.view subviews])
        {
            if(![subview isKindOfClass:UIButton.class])
            {
                [self.view sendSubviewToBack:subview];
            }
        }

    
    
}

//- (IBAction)previousButtonPressed:(id)sender {
//    [self.pageViewController setViewControllers:self.vi direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
//}

- (IBAction)nextButtonTapped:(UIButton *)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (IBAction)finishButtonTapped:(UIButton *)sender {
    [self.pageViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}


- (IBAction)startWalkthrough:(id)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    _currentPageIndex = index;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    _currentPageIndex = index;
    if (index == [self.pageTitles count]) {
//        self.finishButton.hidden = NO;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorialWatched"];
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return self.currentPageIndex;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"startMRMApp"])
    {
        self.navigationController.navigationBarHidden = NO;
    }
}

-(BOOL) checkTutorialDisplayNeeded
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"tutorialWatched"]) {
        return YES;
    } else {
        return NO;
    }
}


@end
