//
//  MapStationMarkersView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 6/30/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MapStationMarkersView.h"

@interface MapStationMarkersView()



@end

@implementation MapStationMarkersView

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (void)layoutSubviews {
//    [self setLayerProperties];
//    [self attachAnimations];
}

- (void)setLayerProperties {
    CAShapeLayer *layer = (CAShapeLayer *)self.layer;
//    CGPoint center = CGPointMake(self.markerFrame.origin.x+self.markerFrame.size.width/2, self.markerFrame.origin.y+self.markerFrame.size.height/2);
    
    layer.path = [UIBezierPath bezierPathWithOvalInRect:self.markerFrame].CGPath;
    layer.fillColor = self.markerColor.CGColor;//[UIColor colorWithHue:0 saturation:1 brightness:.8 alpha:1].CGColor;
}


- (void)attachAnimations {
    [self attachPathAnimation];
    [self attachColorAnimation];
}

- (void)attachPathAnimation {
    CABasicAnimation *animation = [self animationWithKeyPath:@"path"];
    animation.toValue = (__bridge id)[UIBezierPath bezierPathWithOvalInRect:CGRectInset(self.markerFrame, 1, 1)].CGPath;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    [self.layer addAnimation:animation forKey:animation.keyPath];
}

- (void)attachColorAnimation {
    CABasicAnimation *animation = [self animationWithKeyPath:@"fillColor"];
    [self.markerColor colorWithAlphaComponent:0.5];
    animation.fromValue = (__bridge id)self.markerColor.CGColor;//[UIColor colorWithHue:0 saturation:.9 brightness:.9 alpha:1].CGColor;
    [self.layer addAnimation:animation forKey:animation.keyPath];
}

- (CABasicAnimation *)animationWithKeyPath:(NSString *)keyPath {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
    animation.autoreverses = YES;
    animation.repeatCount = HUGE_VALF;
    animation.duration = 1;
    return animation;
}

@end
