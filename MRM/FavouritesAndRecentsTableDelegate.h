//
//  FavouritesAndRecentsTableDelegate.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/10/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SavedJourney.h"

@protocol ShowSavedSearchProtocol;

@interface FavouritesAndRecentsTableDelegate : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) id<ShowSavedSearchProtocol> delegate;

@property (strong, nonatomic) NSMutableArray *favList, *recentList;

- (id) initWithTableView: (UITableView *) tableView;

@end

@protocol ShowSavedSearchProtocol <NSObject>

@optional 
-(void) showResultsPageWithSavedSearchObject:(SavedJourney *)savedJourney;
@end
