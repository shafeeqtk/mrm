//
//  ViaSelectionTableViewDelegate.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@protocol ViaSelectionTableViewProtocol;


@interface ViaSelectionTableViewDelegate : UITableViewController <UITableViewDelegate, UITableViewDataSource>

- (id) initWithTableView: (UITableView *) tableView;

@property (strong, nonatomic) id<ViaSelectionTableViewProtocol> delegate;

@end

@protocol ViaSelectionTableViewProtocol

@optional
-(void)viaRouteSelected:(NSInteger)viaRouteIndex name:(NSString *)viaRouteName;

@end