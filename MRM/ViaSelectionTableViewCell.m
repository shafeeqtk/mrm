//
//  ViaSelectionTableView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ViaSelectionTableViewCell.h"

@implementation ViaSelectionTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



@end
