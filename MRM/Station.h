//
//  Station.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Station : NSObject

@property (strong, nonatomic) NSString *stationName;
@property (strong, nonatomic) NSString *stationCode;
@property int stationId;
@property (strong, nonatomic) NSString *lines;
@property (strong, nonatomic) NSString *lineNames;

@end
