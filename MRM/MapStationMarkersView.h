//
//  MapStationMarkersView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 6/30/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapStationMarkersView : UIView

@property (nonatomic) CGRect markerFrame;
@property (strong, nonatomic) UIColor *markerColor;

@end
