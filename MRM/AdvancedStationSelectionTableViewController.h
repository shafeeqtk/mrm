//
//  AdvancedStationSelectionTVCTableViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationTrain.h"

@protocol AdvancedStationSelectionDelegate;

@interface AdvancedStationSelectionTableViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) id<AdvancedStationSelectionDelegate> delegate;

@property (strong, nonatomic) NSArray *trainsList;  //list of StationTrain objects
@property (strong, nonatomic) IBOutlet UILabel *lineNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *fromStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *trainTowardsInfoLabel;

@property (strong, nonatomic) NSString *lineNameTxt;
@property (strong, nonatomic) NSString *fromStationTxt;
@property (strong, nonatomic) NSString *trainTowardsInfoTxt;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

-(void)setHeaderInfoWithTrainTowardsStationName:(NSString *)towardsStationName fromStationName:(NSString *)fromStationName onLineName:(NSString *)lineName;
@property (strong, nonatomic) NSIndexPath *nextTrainRowIndex;

@end



@protocol AdvancedStationSelectionDelegate <NSObject>

@optional

-(void) hideTrainsListAndShowResultsScreen;
-(void) updatePartOfJourneyAndSuccessorsAtTime:(int)startTime fromStationId:(int)stationId onLine:(int)lineId withTrainId:(int)trainId;

@end

