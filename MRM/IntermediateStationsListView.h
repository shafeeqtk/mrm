//
//  IntermediateStationsListView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntermediateStationsListView : UIView

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *stationMarkerImageViews;

@property (strong, nonatomic) UIColor *lineColor;
@property (strong, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalStationNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *arrivalPlatformNoLabel;

-(void)setLineColorForLine:(UIColor *)lineColor;
@end
