//
//  SavedJourney.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "SavedJourney.h"

@implementation SavedJourney

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.fromStationName forKey:@"fromStationName"];
    [encoder encodeObject:self.toStationName forKey:@"toStationName"];
    [encoder encodeObject:self.selectedViaText forKey:@"selectedViaText"];
    [encoder encodeInt:self.fromStationId forKey:@"fromStationId"];
    [encoder encodeInt:self.toStationId forKey:@"toStationId"];
    [encoder encodeInt:self.selectedViaIndex forKey:@"selectedViaIndex"];
    [encoder encodeObject:self.savedDate forKey:@"savedDate"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.fromStationName = [decoder decodeObjectForKey:@"fromStationName"];
        self.toStationName  = [decoder decodeObjectForKey:@"toStationName"];
        self.selectedViaText = [decoder decodeObjectForKey:@"selectedViaText"];
        self.fromStationId = [decoder decodeIntForKey:@"fromStationId"];
        self.toStationId = [decoder decodeIntForKey:@"toStationId"];
        self.selectedViaIndex = [decoder decodeIntForKey:@"selectedViaIndex"];
        self.savedDate = [decoder decodeObjectForKey:@"savedDate"];
    }
    return self;
}
@end
