//
//  AppDelegate.m
//  MRM
//
//  Created by Shafeeq Rahiman on 10/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "AppDelegate.h"
#import "Station.h"
#import "RouteStation.h"
#import "Line.h"
#import "LineDraw.h"
#import "LineRouteDetail.h"
#import "RoutePlannerViewController.h"

@interface AppDelegate ()

@property (strong, nonatomic) NSString *dbPath;
@property (strong, nonatomic) NSString *dbType;
@property BOOL createEncryptedDatabase;

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

-(NSArray *)lineDraw{
    if(!_lineDraw)
        _lineDraw = [[NSArray alloc] init];
    return _lineDraw;
}

-(NSArray *)linesMaster{
    if(!_linesMaster)
        _linesMaster = [[NSArray alloc] init];
    return _linesMaster;
}

-(NSArray *)stationsMaster{
    if(!_stationsMaster)
        _stationsMaster = [[NSArray alloc] init];
    return _stationsMaster;
}


-(FMDatabase *)db{
    if(!_db){
        NSString *path = [[NSBundle mainBundle] pathForResource:_dbPath ofType:_dbType];
        _db = [FMDatabase databaseWithPath:path];
        [_db setKey:self.dbkey];
    }
    return _db;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.createEncryptedDatabase = NO;
    
    //[self initializeStoryBoardBasedOnScreenSize];

    _dbPath = @"encrypted_MRM";
    _dbType = @"sqlite";
    _dbkey = @"glassbox";
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSLog(@"%@",documentDir);
    self.databasePath = [documentDir stringByAppendingPathComponent:@"encrypted_MRM.sqlite"];
    if(self.createEncryptedDatabase)
    {
        self.databasePath = [documentDir stringByAppendingPathComponent:@"mrm_v3.db3"];
    }

    [self createAndCheckDatabase];
    
    if(!_db){
        _db = [FMDatabase databaseWithPath:self.databasePath];
        [_db open];
        [_db setKey:self.dbkey];
    }
    
    [self updateScripts];
    
    _stationsMaster = [self loadStationsMaster];
    _lineDraw = [self loadLineDraw];
    _linesMaster = [self loadLinesMaster];
    _lineRouteDetails = [self loadLineRouteDetails];
    
    
//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
//    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
//    {
//        UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 20)];
//        view.backgroundColor=[UIColor colorWithRed:129/255.0 green:96/255.0 blue:1/255.0 alpha:1];
//        [self.window.rootViewController.view addSubview:view];
//    }
    
    
    return YES;
}

-(void) updateScripts
{
    //add
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"patch001"]) {
        NSString *sql = @"update STATION_MASTER set name = 'Bharat Petroleum' where name = 'Bharat Petrolium'";
        BOOL success = [_db executeUpdate:sql, nil];
        if(success)
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"patch001"];
        }
    }
    
    return;
}

-(void) createAndCheckDatabase
{
    /* DB ENCRYPTION - START */
     BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:self.databasePath];
    
    if(success) {
        // If file exists, do nothing
        if(!self.createEncryptedDatabase)
        {
            return;
        }
    }
    
    // if file does not exist, make a copy of the one in the Resources folder
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"encrypted_MRM.sqlite"]; // File path
    
    if(self.createEncryptedDatabase)
    {
        databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"mrm_v3.db3"]; // Unencrypted path
    }
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:self.databasePath error:nil]; // Make a copy from the resource file to the Documents folder
    
    if(self.createEncryptedDatabase){
        
        // Set the new encrypted database path to be in the Documents Folder
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDir = [documentPaths objectAtIndex:0];
        NSString *ecDB = [documentDir stringByAppendingPathComponent:@"encrypted_MRM.sqlite"];
        
        // SQL Query. NOTE THAT DATABASE IS THE FULL PATH NOT ONLY THE NAME
        const char* sqlQ = [[NSString stringWithFormat:@"ATTACH DATABASE '%@' AS encrypted KEY 'glassbox';",ecDB] UTF8String];
        
        sqlite3 *unencrypted_DB;
        if (sqlite3_open([self.databasePath UTF8String], &unencrypted_DB) == SQLITE_OK) {
            
            // Attach empty encrypted database to unencrypted database
            sqlite3_exec(unencrypted_DB, sqlQ, NULL, NULL, NULL);
            
            // export database
            sqlite3_exec(unencrypted_DB, "SELECT sqlcipher_export('encrypted');", NULL, NULL, NULL);
            
            // Detach encrypted database
            sqlite3_exec(unencrypted_DB, "DETACH DATABASE encrypted;", NULL, NULL, NULL);
            
            sqlite3_close(unencrypted_DB);
        }
        else {
            sqlite3_close(unencrypted_DB);
            NSAssert1(NO, @"Failed to open database with message '%s'.", sqlite3_errmsg(unencrypted_DB));
        }
        
        self.databasePath = [documentDir stringByAppendingPathComponent:@"encrypted_MRM.sqlite"];
        
    }
    /* DB ENCRYPTION - END*/
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if(!_db){
        _db = [FMDatabase databaseWithPath:self.databasePath];
        [_db open];
        [_db setKey:self.dbkey];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self.db close];
}



#pragma mark - Database call methods
-(NSArray *) loadStationsMaster{
    NSLog(@"LOAD STATIONS MASTER");
    if(!_stationsMaster && _stationsMaster.count > 0 ){
        NSLog(@"Returning previous stn master obj");
        return _stationsMaster;
    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    FMResultSet *rs = [self.db executeQuery:@"SELECT distinct name,_id,line_ids,code FROM STATION_MASTER ORDER BY NAME ASC"];
    while ([rs next]) {
        int stationid = [rs intForColumn:@"_id"];
        NSString *name = [rs stringForColumn:@"name"];
        NSString *lines = [rs stringForColumn:@"line_ids"];
        NSString *code = [rs stringForColumn:@"code"];
        
        Station *station = [[Station alloc] init];
        station.stationName = name;
        station.stationId = stationid;
        station.lines = lines;
        station.stationCode = code;
        [tempArray addObject:station];
    }
    
    [rs close];
    
    for(Station *station in tempArray){
        station.lineNames = [self lineNamesFromCommaSeparatedIds:station.lines];
    }
    
    return [NSArray arrayWithArray:tempArray];
}

-(NSArray *) loadLinesMaster{
    NSLog(@"LOAD LINES MASTER");
    if(!_linesMaster && _linesMaster.count >0){
        NSLog(@"Returning previous lines master obj");
        return _linesMaster;
    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    FMResultSet *rs = [self.db executeQuery:@"SELECT name,_id,code,color from line"];
    
    while([rs next]){
        Line *line = [[Line alloc] init];
        line.lineId = [rs stringForColumn:@"_id"];
        line.lineCode = [rs stringForColumn:@"code"];
        line.lineName = [rs stringForColumn:@"name"];
        line.lineColor = [rs stringForColumn:@"color"];
        [tempArray addObject:line];
    }
    
    [rs close];
    
    return [NSArray arrayWithArray:tempArray];
}

-(NSArray *) loadLineDraw {
    NSLog(@"LOAD LINE DRAW");
    if(!_lineDraw && _lineDraw.count > 0){
        NSLog(@"Returning previous line draw obj");
        return _lineDraw;
    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    FMResultSet *rs = [self.db executeQuery:@"SELECT _id,line_id,from_stn_id,to_stn_id,draw_script,stroke_color,stroke_width from line_draw"];
    
    while([rs next]){
        LineDraw *lineDraw = [[LineDraw alloc] init];
        lineDraw._id = [rs intForColumn:@"_id"];
        lineDraw.lineId = [rs intForColumn:@"line_id"];
        lineDraw.fromStationId = [rs intForColumn:@"from_stn_id"];
        lineDraw.toStationId = [rs intForColumn:@"to_stn_id"];
        lineDraw.drawScript = [rs stringForColumn:@"draw_script"];
        lineDraw.strokeColor = [rs stringForColumn:@"stroke_color"];
        lineDraw.strokeWidth = [rs intForColumn:@"stroke_width"];
        
        [tempArray addObject:lineDraw];
    }
    
    [rs close];
    
    return [NSArray arrayWithArray:tempArray];
}

-(NSString *) getLineNameFromLineId:(int)lineId
{
    for(Line *line in self.linesMaster){
        if ([line.lineId intValue] == lineId)
        {
            return line.lineName;
        }
    }
    return @"Not found";
}

-(NSString *) lineNamesFromCommaSeparatedIds:(NSString *)lines
{
    NSMutableString *linesStr = [[NSMutableString alloc] init];
    NSArray *lineIdArray = [lines componentsSeparatedByString:@","];
    
    bool firstItem = YES;
    
    //if lines master is not loaded, do it before continuing
    if(!_linesMaster){
        _linesMaster = [self loadLinesMaster];
    }
    
    //loop through each split in line Ids string
    for(NSString *eachLine in lineIdArray){
        //compare each with all the 8 lines in line master
        for(Line *line in self.linesMaster){
            if(firstItem){
                if([line.lineId isEqualToString:eachLine]){
                    [linesStr appendString:line.lineName];
                    firstItem = NO;
                    break;
                }
            } else {
                if([line.lineId isEqualToString:eachLine]){
                    [linesStr appendString:@", "];
                    [linesStr appendString:line.lineName];
                    firstItem = NO;
                    break;
                }
            }
        }
        
    }
    
    return [linesStr description];
}

-(NSArray *) loadLineRouteDetails{
    NSLog(@"LOAD LINE ROUTE DETAILS");
    if(!_lineRouteDetails && _lineRouteDetails.count > 0){
        NSLog(@"Returning previous line route details");
        return _lineRouteDetails;
    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    FMResultSet *rs = [self.db executeQuery:@"SELECT _id,line_id,route_info,order_no from line_route_details"];
    
    while([rs next]){
        LineRouteDetail *lineRouteDetail = [[LineRouteDetail alloc] init];
        lineRouteDetail._id = [rs intForColumn:@"_id"];
        lineRouteDetail.lineId = [rs intForColumn:@"line_id"];
        lineRouteDetail.routeInfo = [rs stringForColumn:@"route_info"];
        lineRouteDetail.orderNo = [rs intForColumn:@"order_no"];
        
        [tempArray addObject:lineRouteDetail];
    }
    
    [rs close];
    
    return [NSArray arrayWithArray:tempArray];
    
}


-(BOOL)isStationOnSameLine:(int)firstStationId asComparedToStationId:(int)secondStationId
{
    NSString *firstStationLines =@"", *secondStationLines = @"";
    
    bool firstFound = NO, secondFound = NO;
    for(Station *station in self.stationsMaster){
        if(station.stationId == firstStationId){
            firstStationLines = station.lines;
            firstFound = YES;
        }else if(station.stationId == secondStationId){
            secondStationLines = station.lines;
            secondFound = YES;
        }
        
        if(firstFound && secondFound){
            break;
        }
    }
    
    for(NSString *eachLineOnFirstStation in [firstStationLines componentsSeparatedByString:@","])
    {
        for(NSString *eachLineOnSecondStation in [secondStationLines componentsSeparatedByString:@","])
        {
            if([eachLineOnFirstStation isEqualToString:eachLineOnSecondStation]){
                return YES;
            }
        }
    }
    return NO;
}

-(NSArray *)listOfStationsBetweenStation:(int)stationAId andStation:(int)stationBId onLine:(int)lineId
{
    //has to be on same line first!
//    int lineId = [self getCommonLineIdBetweenStation:stationAId secondStationId:stationBId];

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    NSString *stnAMatchStr = [NSString stringWithFormat:@",%d,",stationAId];
    NSString *stnBMatchStr = [NSString stringWithFormat:@",%d,",stationBId];
    
    
    NSString *like1 = [NSString stringWithFormat:@"*%@*%@*",stnAMatchStr,stnBMatchStr];
    NSString *like2 = [NSString stringWithFormat:@"*%@*%@*",stnBMatchStr,stnAMatchStr];
    
    NSString *like3 = [NSString stringWithFormat:@"*,%@,%@,*",@(stationAId).stringValue,@(stationBId).stringValue];
    NSString *like4 = [NSString stringWithFormat:@"*,%@,%@,*",@(stationBId).stringValue,@(stationAId).stringValue];
    
    for( LineRouteDetail *lineRouteDetail in self.lineRouteDetails)
    {
        if(lineId == lineRouteDetail.lineId){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF LIKE %@",like1];
            bool isMatchingFromLike1 = [predicate evaluateWithObject:lineRouteDetail.routeInfo];
            
            predicate = [NSPredicate predicateWithFormat:@"SELF LIKE %@",like2];
            bool isMatchingFromLike2 = [predicate evaluateWithObject:lineRouteDetail.routeInfo];
            
            predicate = [NSPredicate predicateWithFormat:@"SELF LIKE %@",like3];
            bool isMatchingFromLike3 = [predicate evaluateWithObject:lineRouteDetail.routeInfo];
            
            predicate = [NSPredicate predicateWithFormat:@"SELF LIKE %@",like4];
            bool isMatchingFromLike4 = [predicate evaluateWithObject:lineRouteDetail.routeInfo];
            
            if(isMatchingFromLike1 || isMatchingFromLike2 || isMatchingFromLike3 || isMatchingFromLike4)
            {
                NSRange r1 = [lineRouteDetail.routeInfo rangeOfString:isMatchingFromLike1?stnAMatchStr:stnBMatchStr];
                NSRange r2 = [lineRouteDetail.routeInfo rangeOfString:isMatchingFromLike1?stnBMatchStr:stnAMatchStr];
                NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
                NSString *sub = [lineRouteDetail.routeInfo substringWithRange:rSub];
                
                NSArray *subArr = [sub componentsSeparatedByString:@","];
                
                //If string is reverse match, reverse the array
                if(isMatchingFromLike2)
                {
                    subArr = [[subArr reverseObjectEnumerator] allObjects];

                }
                NSMutableArray *stnObjs = [[NSMutableArray alloc] init];
                for (NSString *eachStnIdStr in subArr){
                    if(eachStnIdStr && ![eachStnIdStr isEqualToString:@""]){
                        RouteStation *stn = [[RouteStation alloc] init];
                        stn.station_id = eachStnIdStr;
                        [stnObjs addObject:stn];
                    }
                }

                //first station
                RouteStation *stn = [[RouteStation alloc] init];
                stn.station_id = @(stationAId).stringValue;
                [tempArray addObject:stn];
                
                [tempArray addObjectsFromArray:stnObjs];
                
                //last station
                stn = [[RouteStation alloc] init];
                stn.station_id = @(stationBId).stringValue;
                [tempArray addObject:stn];
                
                break;
            }
        }
    }
    NSLog(@"Intermediate Stations List - START");
    for(RouteStation *stn in tempArray){
        NSLog(stn.station_id);
    }
    NSLog(@"Intermediate Stations List - END");
    return tempArray;
}

-(int)getCommonLineIdBetweenStation:(int)firstStationId secondStationId:(int)secondStationId
{
    NSString *firstStationLines =@"", *secondStationLines = @"";
    
    bool firstFound = NO, secondFound = NO;
    for(Station *station in self.stationsMaster){
        if(station.stationId == firstStationId){
            firstStationLines = station.lines;
            firstFound = YES;
        }else if(station.stationId == secondStationId){
            secondStationLines = station.lines;
            secondFound = YES;
        }
        
        if(firstFound && secondFound){
            break;
        }
    }
    
    for(NSString *eachLineOnFirstStation in [firstStationLines componentsSeparatedByString:@","])
    {
        for(NSString *eachLineOnSecondStation in [secondStationLines componentsSeparatedByString:@","])
        {
            if([eachLineOnFirstStation isEqualToString:eachLineOnSecondStation]){
                return [eachLineOnSecondStation intValue];
            }
        }
    }
    return 0;

}


-(Station *)getStationFromStationId:(int)stationId
{
    for(Station *station in self.stationsMaster) {
        if(station.stationId == stationId){
            return station;
        }
    }
    return nil;
}

-(UIColor *)getUIColorForLine:(int)lineNumber
{
    switch(lineNumber){
        case 1: return [UIColor colorWithRed:241/255.0 green:51/255.0 blue:138/255.0 alpha:1];  //WR
        case 2: return [UIColor colorWithRed:249/255.0 green:149/255.0 blue:0/255.0 alpha:1];   //CR
        case 3: return [UIColor colorWithRed:88/255.0 green:196/255.0 blue:174/255.0 alpha:1];  //HR
        case 4: return [UIColor colorWithRed:135/255.0 green:128/255.0 blue:191/255.0 alpha:1]; //TH
        case 5: return [UIColor grayColor];                                                     //NS
        case 6: return [UIColor grayColor];                                                     //SH
        case 7: return [UIColor colorWithRed:247/255.0 green:129/255.0 blue:110/255.0 alpha:1]; //MN
        case 8: return [UIColor colorWithRed:0/255.0 green:174/255.0 blue:242/255.0 alpha:1];   //MT
        default: return [UIColor grayColor];
    }
}

#pragma mark - Core Data Methods
/*
- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MRMStats" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MRMStats.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}
*/
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
