//
//  MRMStationsLine.h
//  MRM
//
//  Created by Shafeeq Rahiman on 6/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRMStationsLine : NSObject

@property (nonatomic) int line_id;
@property (strong,nonatomic) NSArray *stationsListWithinLine;

@end
