//
//  CustomBottomAlignedTableViewHeaderCellTableViewCell.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/21/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBottomAlignedTableViewHeaderCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@end
