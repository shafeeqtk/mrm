//
//  SavedJourney.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavedJourney : NSObject

@property (strong, nonatomic) NSString *fromStationName;
@property (strong, nonatomic) NSString *toStationName;
@property (strong, nonatomic) NSString *selectedViaText;
@property int fromStationId;
@property int toStationId;
@property int selectedViaIndex;
@property NSDate *savedDate;

@end
