//
//  StationTrainTableViewCell.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "StationTrainTableViewCell.h"

@implementation StationTrainTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
