//
//  Draw2D.h
//  MRMMapTest
//
//  Created by Shafeeq Rahiman on 6/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapImageView : UIView

//+ (CGPathRef) newPathForRoundedRect:(CGRect)rect radius:(CGFloat)radius;
@property (strong,nonatomic) UIImage *fullMapImage;

@property (strong,nonatomic) UIImageView *fullMapView;

-(void) toggleOpacity;
-(void) toggleVisibility;

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@end
