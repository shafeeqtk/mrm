//
//  HomeScreenViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 10/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "SavedSearchTableViewCell.h"

@interface HomeScreenViewController ()
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *browseMapWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *startMyJourneyWidthConstraint;

@end

@implementation HomeScreenViewController

int favHeaderRow, recentHeaderRow;
int noOfFavourites = 5;
int noOfRecents = 5;

enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self findPhoneType];
    // Do any additional setup after loading the view, typically from a nib.
    
    if(phonemodel == Iphone6){
        self.startMyJourneyWidthConstraint.constant = 150;
        self.browseMapWidthConstraint.constant = 90;
        
    }else if(phonemodel == Iphone5_5s || phonemodel == Iphone4_4s){
        self.startMyJourneyWidthConstraint.constant = 143;
        self.browseMapWidthConstraint.constant = 75;
    }else if(phonemodel == Iphone6plus){
        self.startMyJourneyWidthConstraint.constant = 180;
        self.browseMapWidthConstraint.constant = 96;
    }
    
    [self recalculateIndexes];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"MRM.png"] forBarMetrics:UIBarMetricsDefault];

}

-(void) recalculateIndexes{
    favHeaderRow = 0;
    recentHeaderRow = favHeaderRow + noOfFavourites+1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    cell.textLabel.text=[NSString stringWithFormat:@"%@",[array objectAtIndex:indexPath.section]];
    SavedSearchTableViewCell *cell;
    
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.text =  (indexPath.section == 0) ? @"FAVOURITE JOURNEYS" : @"RECENT SEARCHES";
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.textColor = [UIColor grayColor];
        
        cell.detailTextLabel.text = @"Show All";
        cell.detailTextLabel.font = [UIFont systemFontOfSize:8];
        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        cell.contentView.clipsToBounds = YES;
        return cell;
    }
    else if(indexPath.section > recentHeaderRow){
        cell = [tableView dequeueReusableCellWithIdentifier:@"recentCell"];
        cell.favouriteIconImage.image = [UIImage imageNamed:@"MrmApp_ios_SpriteSheet_0084_home-fev-off"];
        cell.leftViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.rightViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.mainBackgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = [[UIColor grayColor] CGColor];
        cell.layer.cornerRadius = 7.0f;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"favouriteCell"];
        cell.layer.cornerRadius = 7.0f;
        cell.layer.masksToBounds = YES;
        cell.layer.borderWidth = 0.5f;
        cell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        return 20;
    }
    if(indexPath.section > recentHeaderRow){
        return 55;
    }
    return 74;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(SavedSearchTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > recentHeaderRow){
        //        cell.backgroundColor = [UIColor clearColor];
        cell.favouriteIconImage.image = [UIImage imageNamed:@"MrmApp_ios_SpriteSheet_0084_home-fev-off"];
        cell.leftViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.rightViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.mainBackgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = [[UIColor grayColor] CGColor];
        cell.layer.cornerRadius = 7.0f;
    }

}


// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        return NO;
    }
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section > recentHeaderRow){
        return nil;
    }
    return @"Edit";
}
-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath{
    return;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete

        if ([[tableView cellForRowAtIndexPath:indexPath].reuseIdentifier isEqualToString:@"favouriteCell"]){
            noOfFavourites--;
        } else if([[tableView cellForRowAtIndexPath:indexPath].reuseIdentifier isEqualToString:@"recentCell"]){
            noOfRecents--;
        }
        [self recalculateIndexes];
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(editingStyle == UITableViewCellEditingStyleNone) {
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return noOfFavourites+noOfRecents+2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return;
}

@end