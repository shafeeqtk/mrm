//
//  SpriteData.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/23/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpriteData : NSObject

@property int stationId;
@property int lineId;
@property NSString *spriteType;
@property int spriteX;
@property int spriteY;
@property int mapX;
@property int mapY;
@property int mapW;
@property int mapH;
@property int mActive;
@property int mIdle;
@property BOOL isStop;
@end
