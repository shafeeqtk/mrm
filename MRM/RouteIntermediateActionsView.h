//
//  RouteIntermediateActionsView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteIntermediateActionsView : UIView

@property (strong, nonatomic) IBOutlet UILabel *actionMessageLabel;
@end
