//
//  NSObject+ImageWithAlpha.h
//  MRMMapTest
//
//  Created by Shafeeq Rahiman on 6/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (ImageWithAlpha)
- (UIImage *)imageByApplyingAlpha:(CGFloat) alpha;
@end
