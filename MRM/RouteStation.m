//
//  MRMStation.m
//  MRM
//
//  Created by Shafeeq Rahiman on 6/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "RouteStation.h"

@implementation RouteStation

-(NSString *)stationName {
    if(!_stationName)
        _stationName = @"";
    return _stationName;
}

-(NSString *)station_id {
    if(!_station_id)
        _station_id = @"";
    return _station_id;
}

-(NSString *)speed {
    if(!_speed)
        _speed = @"";
    return _speed;
}

//-(NSString *)line {
//    if(!_line)
//        _line = @"";
//    return _line;
//}

-(NSString *)time {
    if(!_time)
        _time = @"";
    return _time;
}

-(NSString *)platform_no {
    if(!_platform_no)
        _platform_no = @"";
    return _platform_no;
}

-(NSString *)platform_side {
    if(!_platform_side)
        _platform_side = @"";
    return _platform_side;
}

-(NSString *)all_lines {
    if(!_all_lines)
        _all_lines = @"";
    return _all_lines;
}

-(NSString *) stationDescription {
    NSMutableString *mutStr = [[NSMutableString alloc] init];
    [mutStr appendString:[NSString stringWithFormat:@" %@ , %@, Platform %@ ", _time, _speed, _platform_no ]];
    return mutStr;
}

-(void) initializeStationWithstationName:(NSString *)name stationId:(int)stationId line:(int)line{
    _station_id = [NSString stringWithFormat:@"%d",stationId];
    _stationName = name;
    _line = line;
}

@end
