//
//  BrowseMapViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/28/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationSelectionViewController.h"

@interface BrowseMapViewController : UIViewController <UIScrollViewDelegate, StationSelectionDelegate>

-(void) setupMap;
@end
