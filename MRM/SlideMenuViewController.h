//
//  SlideMenuViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SlideMenuDelegate;

@interface SlideMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, retain) id<SlideMenuDelegate> delegate;

@end

@protocol SlideMenuDelegate <NSObject>

@optional

-(void) selectedMenuItemAtIndex:(NSInteger) itemIndex;
-(void) closeMenuScreen;

@end

