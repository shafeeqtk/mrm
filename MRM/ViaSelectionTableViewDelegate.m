//
//  ViaSelectionTableViewDelegate.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ViaSelectionTableViewDelegate.h"
#import "ViaSelectionTableViewCell.h"
#import "AppDelegate.h"

@interface ViaSelectionTableViewDelegate()

@property (strong, retain) AppDelegate *appdelegate;

@end


@implementation ViaSelectionTableViewDelegate


-(AppDelegate *)appdelegate{
    if(!_appdelegate)
        _appdelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appdelegate;
}


- (id) initWithTableView: (UITableView *) tableView {
    self = [super init];
    if (self) {
        tableView.dataSource = self;
        tableView.delegate = self;
    }
    return self;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.appdelegate.journey.viaRoutesList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ViaSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"viaCell"];
    //[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viaCustomCell"];
    
//    cell.textLabel.text = [NSString stringWithFormat:@"Hello %ld",indexPath.row];
    if(indexPath.row > 0){
        cell.viaImageView.image = nil;
        cell.viaImageView.hidden = YES;
    }
    
    NSString *routeName = [self.appdelegate.journey.viaRoutesNameList objectAtIndex:indexPath.row];
    NSInteger noOfViaStations = [routeName componentsSeparatedByString:@"-"].count;
    cell.viaNoOfInterchangesLabel.text = [NSString stringWithFormat:@"%ld interchanges", noOfViaStations];
    cell.viaRouteNameLabel.text = [self.appdelegate.journey.viaRoutesNameList objectAtIndex:indexPath.row];
    cell.viaDurationLabel.text = [self.appdelegate.journey.viaRoutesTimes objectAtIndex:indexPath.row];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:254/255.0  green:196/255.0 blue:22/255.0 alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.appdelegate.journey.selectedViaIndex = indexPath.row;
    [self.delegate viaRouteSelected:indexPath.row name:@""];
}

@end
