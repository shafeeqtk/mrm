//
//  AdvancedStationSelectionTVCTableViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "AdvancedStationSelectionTableViewController.h"
#import "StationTrainTableViewCell.h"
#import "AppDelegate.h"

@interface AdvancedStationSelectionTableViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *gradLineImageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leftMaskViewWidthConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leftMaskImageWidthConstraint;

@end

enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;

@implementation AdvancedStationSelectionTableViewController

#pragma mark - Find Phone Model


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self findPhoneType];
    
    self.gradLineImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shadow H"]];
    
    self.lineNameLabel.text = self.lineNameTxt;
    self.fromStationLabel.text = self.fromStationTxt;
    self.trainTowardsInfoLabel.text = self.trainTowardsInfoTxt;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    if(phonemodel == Iphone6 || phonemodel == Iphone6plus)
    {
        self.leftMaskViewWidthConstraint.constant = 44;
    }
    else
    {
        self.leftMaskViewWidthConstraint.constant = 44;
    }
    self.leftMaskImageWidthConstraint.constant = 7;//self.leftMaskViewWidthConstraint.constant;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.trainsList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StationTrainTableViewCell *cell;
    StationTrain *stationTrain = [self.trainsList objectAtIndex:indexPath.row];

    if(!stationTrain.isFutureTrain)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"pastTrainCell" forIndexPath:indexPath];
    }
    else if(stationTrain.isNextTrain)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"trainListHeaderCell" forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"trainCell" forIndexPath:indexPath];
    }
    
    cell.trainComingFromLabel.text = [NSString stringWithFormat:@"Coming from %@",stationTrain.start_stn_name];
    cell.trainLastStopAndSpeedLabel.text = [NSString stringWithFormat:@"%@ - %@",stationTrain.end_stn_name,[stationTrain.speed isEqualToString:@"S"]?@"Slow":@"Fast" ];
    cell.platformNoLabel.text = [NSString stringWithFormat:@"PF: %@",stationTrain.platf_no];
    cell.timeLabel.text = stationTrain.trainTimeStr;
    cell.carInfoLabel.text = [NSString stringWithFormat:@"%d Car",stationTrain.noOfCar];
    cell.trainSpecialInfoLabel.text = stationTrain.special_info;
    cell.towardsStationCodeLabel.text = stationTrain.end_stn_code;
    cell.towardsStationCodeLabel.textColor = [(AppDelegate*)[[UIApplication sharedApplication] delegate] getUIColorForLine:stationTrain.lineId];
    // Configure the cell...
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    StationTrain *stationTrain = [self.trainsList objectAtIndex:indexPath.row];
    if(stationTrain.isNextTrain){
        return 156;
    }
    return 82;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closeButtonPressed:(UIButton *)sender {
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseModal" object:self];
//    [self.navigationController popViewControllerAnimated:YES];
//    self.view.layer.shadowOpacity = 0;
//    self.view.layer.shadowRadius = 2;
//    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.view.layer.shadowOffset = CGSizeMake(0, 0);
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.view.superview setFrame:CGRectMake(0, 0, self.view.superview.frame.size.width, self.view.superview.frame.size.height)];
//        self.view.hidden = YES;
//    }];
    [self.delegate hideTrainsListAndShowResultsScreen];
}

-(void)setHeaderInfoWithTrainTowardsStationName:(NSString *)towardsStationName fromStationName:(NSString *)fromStationName onLineName:(NSString *)lineName
{
    self.fromStationLabel.text = fromStationName;
    self.trainTowardsInfoLabel.text = towardsStationName;
    self.lineNameLabel.text = lineName;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger row = indexPath.row;
    StationTrain *stationTrain = [self.trainsList objectAtIndex:row];
    [self.delegate updatePartOfJourneyAndSuccessorsAtTime:stationTrain.train_time fromStationId:stationTrain.currentStationId onLine:stationTrain.lineId withTrainId:stationTrain.trainId ];
    
    return;
}

@end
