//
//  MapCustomDrawingView.h
//  MRMMapTest
//
//  Created by Shafeeq Rahiman on 6/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCustomDrawingView : UIView

@property (nonatomic) BOOL swapDrawing;

@property (nonatomic) float scrollZoomScale;

-(void) toggleVisibility;
-(void)swapDrawingCall;


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

-(void) findAllLinksFromJourneyInformation;

@end
