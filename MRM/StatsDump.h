//
//  StatsDump.h
//  MRM
//
//  Created by Shafeeq Rahiman on 2/21/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface StatsDump : NSManagedObject

@property (nonatomic, retain) NSNumber * srcId;
@property (nonatomic, retain) NSNumber * destId;
@property (nonatomic, retain) NSString * jxIdList;
@property (nonatomic, retain) NSString * jxStrList;
@property (nonatomic, retain) NSNumber * isFav;
@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSDate * addedTs;
@property (nonatomic, retain) NSDate * lastModifiedTs;

@end
