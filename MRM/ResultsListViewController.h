//
//  ResultsListViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/3/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FullJourney.h"
#import "RouteResultContentView.h"
#import "AdvancedStationSelectionTableViewController.h"
#import "ResultsExtrasView.h"
#import "MessageUI/MessageUI.h"

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;


@interface ResultsListViewController : UIViewController <UIScrollViewDelegate, RouteResultContentViewDelegate,AdvancedStationSelectionDelegate, ResultsExtrasViewDelegate, UIDocumentInteractionControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>


@property (retain) UIDocumentInteractionController * documentInteractionController;

@property (strong, nonatomic) IBOutlet UITextView *debugTextView;
@property (strong, nonatomic) NSString *debugText;

@property (strong, nonatomic) FullJourney *journey;

@property (strong, nonatomic) IBOutlet UILabel *fromStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *toStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedViaRouteNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedViaRouteInterchangesLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedViaRouteEstimatedTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedDayLabel;

@property (strong, nonatomic) NSString *fromStationText;
@property (strong, nonatomic) NSString *toStationText;
@property (strong, nonatomic) NSString *selectedViaRouteNameText;
@property (strong, nonatomic) NSString *selectedViaRouteInterchangesText;
@property (strong, nonatomic) NSString *selectedViaRouteEstimatedTimeText;
@property (strong, nonatomic) NSString *selectedTimeText;
@property (strong, nonatomic) NSString *selectedDayText;

@property (strong, nonatomic) IBOutlet UIView *viaListContainerView;
@property (strong, nonatomic) IBOutlet UITableView *viaListTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viaListTableViewHeightConstraint;
@property (strong, nonatomic) IBOutlet UIView *maskViewFullScreen;

@property BOOL isFavourite;

@end
