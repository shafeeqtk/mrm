//
//  MapPinCustomImageView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/31/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MapPinCustomImageView.h"

@implementation MapPinCustomImageView

-(instancetype)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    return self;
}

- (void)setTransform:(CGAffineTransform)transform
{
    [super setTransform:transform];
    
    CGAffineTransform invertedTransform = CGAffineTransformInvert(transform);
    
    for (id obj in self.subviews)
    {
        if ([obj isKindOfClass:[UIImageView class]])
        {
            [((UIView *)obj) setTransform:invertedTransform];
        }
    }
}

@end
