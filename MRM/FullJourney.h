//
//  FullJourney.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PartJourney.h"

@interface FullJourney : NSObject

@property int startStationId;
@property (strong, nonatomic) NSString *startStationName;
@property int endStationId;
@property (strong, nonatomic) NSString *endStationName;

@property (strong, nonatomic) NSArray *viaRoutesList;   //intermediate station Ids
@property int selectedViaIndex;

@property (strong, nonatomic) NSArray *viaRoutesTimes; //duration for the journey

@property (strong, nonatomic) NSArray *viaRoutesNameList;   //intermediate station Names

@property int departureTimeInMinutes; //departure time in minutes(after 00:00 HRS). Default to now.
@property int departureDay;

// NSArray of NSArray(RouteStation) objects, i.e, each leg of the journey.
@property (strong, nonatomic) NSMutableArray *partJournies;

-(NSArray *) generateViaList;
-(void) findTrainsBetweenStartAndEndStation: (int) afterTimeInMins;
-(BOOL) findTrainsBetweenIntermediateStationId:(int)startStationId andEndStation:(int)endStationId atTime:(int)startimeInMins withTrainId:(int)trainId;

-(NSArray *) listTrainsFromStationId:(int)fromStationId
                         toStationId:(int)toStationId
                            onLineId:(int)lineId
                        atTimeInMins:(int)timeInMins
                         inDirection:(NSString *)trainDirection;

-(NSString *) findStationNameById:(int)stationId;
-(NSString *) findLineNameById:(int)lineId;

@end
