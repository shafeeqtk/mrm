//
//  BrowseMapViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/28/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "BrowseMapViewController.h"
#import "AppDelegate.h"
#import "MapStationHighlightView.h"
#import "SpriteData.h"
#import "UIImageView+GeometryConversion.h"

@interface BrowseMapViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *mapImageView;
@property (strong, nonatomic) IBOutlet UIImage *mapImage;

@property (strong, nonatomic) IBOutlet UIView *fillerView;
@property (strong, nonatomic) IBOutlet MapStationHighlightView *mapDrawView;

@property (strong, nonatomic) AppDelegate *appdelegate;
@property (strong, nonatomic) IBOutlet UIButton *searchStationButton;
@property (strong, nonatomic) IBOutlet UIView *searchInputView;
@property (strong, nonatomic) IBOutlet UIView *blankView;
@property (strong, nonatomic) IBOutlet UIView *droppedPinView;
@property (strong, nonatomic) IBOutlet UIImageView *pinImageView;
@property CGFloat currentPinZoomFactor;

@property (strong, nonatomic) NSString *selectedStationName;

@property (nonatomic,retain) UIView *activityView;
@property int selectedStationId;
@property CGFloat x,y;
@property BOOL viewLoaded;
@end

@implementation BrowseMapViewController

-(AppDelegate *)appdelegate{
    if(!_appdelegate){
        _appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appdelegate;
}
/*
- (void)viewDidLoad_old {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *image = self.mapImageView.image;
    
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat ratio = CGRectGetWidth(self.scrollView.bounds) / image.size.width;
    self.mapImageView.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), image.size.height * ratio);
    self.mapImageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    self.mapImageView.image = image;
    self.scrollView.clipsToBounds = YES;
    self.scrollView.contentSize = self.mapImageView.bounds.size;
    self.scrollView.zoomScale = 0.25;
    self.scrollView.maximumZoomScale = 0.75;
    self.scrollView.minimumZoomScale = 0.25;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    
    self.scrollView.bounces = NO;
    self.scrollView.bouncesZoom = NO;
    
    float offsetX = 20, offsetY = 120;
    CGPoint shiftedOrigin = CGPointMake(offsetX*self.scrollView.zoomScale /4,offsetY*self.scrollView.zoomScale/4);
    self.scrollView.contentOffset = shiftedOrigin;
    self.scrollView.contentOffset = shiftedOrigin;
    
    
//    [self initZoom];
    
    /n*
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    self.scrollView.clipsToBounds = YES; // default is NO, we want to restrict drawing within our scrollview
    self.scrollView.bounces = NO;
    self.scrollView.bouncesZoom = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;

//    CGFloat ratio = CGRectGetWidth(self.scrollView.bounds) / self.mapImageView.image.size.width;
//    CGFloat ratioH = CGRectGetHeight(self.scrollView.bounds) / self.mapImageView.image.size.height;
//    self.mapImageView.bounds = CGRectMake(0, 0, self.mapImageView.image.size.width * ratio, self.mapImageView.image.size.height * ratioH);
//    self.mapImageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    
    self.scrollView.minimumZoomScale = 0.1;
    self.scrollView.maximumZoomScale = 1;
    
    self.scrollView.zoomScale = self.scrollView.minimumZoomScale;
    
    float offsetX = 20, offsetY = 120;
    CGPoint shiftedOrigin = CGPointMake(offsetX*self.scrollView.zoomScale /4,offsetY*self.scrollView.zoomScale/4);
    self.scrollView.contentOffset = shiftedOrigin;
    
    [self.scrollView setContentSize:CGSizeMake(self.mapImageView.image.size.width, self.mapImageView.image.size.height)];

//    self.scrollView.contentSize = CGSizeMake(self.mapImageView.frame.size.width*self.scrollView.zoomScale /4, self.mapImageView.frame.size.height*self.scrollView.zoomScale /4);
    
//    self.scrollView.contentOffset = shiftedOrigin;

//    CGFloat ratio = CGRectGetWidth(self.scrollView.bounds) / self.mapImageView.image.size.width;
//    self.mapImageView.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), self.mapImageView.image.size.height * ratio);
//    self.mapImageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    
    NSLog(@"content size = %f x %f",self.mapImageView.image.size.width,self.mapImageView.image.size.height);
    
    
//    [self initZoom];
    
     * -/
}
*/
CGFloat ratio;

-(void)viewDidLoad{
    [super viewDidLoad];
    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    [self.searchStationButton setTitle:[NSString stringWithFormat:@"Search"] forState:UIControlStateNormal];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(0, 0)];
    self.navigationController.navigationBar.topItem.title = @"Back";
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setupMap];
    self.viewLoaded = YES;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    self.navigationController.navigationBar.topItem.title = @"MRM";
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil ];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"searchSingleStation"]){
        
        StationSelectionViewController *ssvc = nil;
        
        if([[segue destinationViewController] isKindOfClass:StationSelectionViewController.class]){
            ssvc = [segue destinationViewController];
        }else{
            return;
        }
        ssvc.delegate = self;
        ssvc.selectionType = @"browse";
        ssvc.headerText = @"Select A Station";
        
    }
}


-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.mapImageView;
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    NSLog(@"Zooming... zoom scale = %f",self.scrollView.zoomScale);
    
    UIView *subView = self.mapImageView;
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
//    self.mapDrawView.scrollZoomScale = scrollView.zoomScale;
    
//    if(self.pinImageView)
//    {
//        self.pinImageView.frame = CGRectMake(((self.x*4/self.scrollView.zoomScale)+(15*4/self.scrollView.zoomScale))*ratio/self.scrollView.zoomScale, ((self.y*4/self.scrollView.zoomScale)+(350*4/self.scrollView.zoomScale))*ratio/self.scrollView.zoomScale,100*ratio/self.scrollView.zoomScale,100*ratio/self.scrollView.zoomScale);
//    }
//    NSLog(@"Pin Origin X = %f , Y = %f",
//                        ((self.x*4/self.scrollView.zoomScale)+(15*4/self.scrollView.zoomScale))*ratio/self.scrollView.zoomScale,
//                        ((self.y*4/self.scrollView.zoomScale)+(350*4/self.scrollView.zoomScale))*ratio/self.scrollView.zoomScale);
    
//    self.pinImageView.frame = CGRectMake( ((self.x+15)*0.25*ratio) - ((25*ratio/self.scrollView.zoomScale)/self.scrollView.zoomScale), ((self.y-750)*0.25*ratio) - ((25*ratio/self.scrollView.zoomScale)/self.scrollView.zoomScale),25/self.scrollView.zoomScale,25/self.scrollView.zoomScale);
    
        self.pinImageView.frame = CGRectMake( ((self.x+15)*0.25*ratio), ((self.y-750)*0.25*ratio),25/self.scrollView.zoomScale,25/self.scrollView.zoomScale);
    
//    [self rescaleItemMarkers];
    
    [self.mapDrawView setNeedsDisplay];
    [self.mapImageView setNeedsDisplay];
}


-(void) didSelectStationWithName:(NSString *)stationName andStationId:(int)stationId selectedAs:(NSString *)type
{
    [self.searchStationButton setTitle:[NSString stringWithFormat:@"%@",stationName ] forState:UIControlStateNormal];
    [self.searchStationButton setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    
        self.selectedStationId = stationId;
    self.selectedStationName = stationName;
    [self zoomOnMapToStationId:stationId];
    return;
}


- (void) zoomOnMapToStationId:(int)stationId
{
//    [self loadStationMarkersFromDB];
    FMResultSet *rs = [self.appdelegate.db executeQuery:@"SELECT SPRITE_X,SPRITE_Y FROM SPRITE_DATA WHERE STATION_ID=? AND SPRITE_TYPE = 'Marker' LIMIT 1" withArgumentsInArray:@[[NSNumber numberWithInt:stationId]]];

    self.x = 0.0;
    self.y = 0.0;
    while ([rs next]) {
        self.x = [rs intForColumn:@"SPRITE_X"];
        self.y = [rs intForColumn:@"SPRITE_Y"];
    }
    
    [self.mapImageView setNeedsDisplay];
//    self.droppedPinView.backgroundColor = [UIColor whiteColor];
    for(UIView *vw in self.scrollView.subviews)
    {
        if(vw.tag == 901)
        {
            [vw removeFromSuperview];
        }
    }
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.scrollView setZoomScale:4.0f animated:NO];
                         NSLog(@"Zoom scale = %f, ratio = %f",self.scrollView.zoomScale, ratio);
                         NSLog(@"Image x= %f, y=%f",self.x,self.y);
                         CGRect frame = CGRectMake((self.x)*0.25*ratio, (self.y)*0.25*ratio, 1, 1);
                         NSLog(@"Frame = %f,%f %f,%f",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height);
                         [self.scrollView zoomToRect:frame animated:NO];

//                         self.pinImageView.frame = CGRectMake((self.x+15)*0.25*ratio, (self.y+350)*0.25*ratio,100*0.25*ratio,100*0.25*ratio);
                         self.pinImageView.frame = CGRectMake((self.x+15)*0.25*ratio, (self.y-750)*0.25*ratio,21/self.scrollView.zoomScale,31/self.scrollView.zoomScale);
//                         self.pinImageView.frame = CGRectMake( ((self.x+15)*0.25*ratio), ((self.y-700)*0.25*ratio),25/self.scrollView.zoomScale,25/self.scrollView.zoomScale);
                         NSLog(@"Pin Original origin X = %f , Y = %f",(self.x+15)*0.25*ratio, (self.y+350)*0.25*ratio);
                         self.currentPinZoomFactor = self.scrollView.zoomScale;
/*                         [self.droppedPinView addSubview:self.pinImageView];
                         [self.droppedPinView setNeedsDisplay];
 */
                     }
                     completion:nil];
    
    [rs close];
    
}

-(void) loadStationMarkersFromDB
{
    [self.mapDrawView.drawStationsArray removeAllObjects];
    
    NSString *sprite_qry = [NSString stringWithFormat: @"SELECT STATION_ID, LINE_ID, SPRITE_TYPE, SPRITE_X, SPRITE_Y, MAP_X, MAP_Y, MAP_W, MAP_H, M_ACTIVE, M_IDLE FROM SPRITE_DATA T WHERE T.STATION_ID = ? ORDER BY T.STATION_ID,T.LINE_ID"];
    
    FMResultSet *rs = [self.appdelegate.db executeQuery:sprite_qry
                      withArgumentsInArray:@[@(self.selectedStationId)]] ;
    
    while ([rs next]) {
        SpriteData *eachSpriteData = [[SpriteData alloc] init];
        eachSpriteData.spriteType = [rs stringForColumn:@"SPRITE_TYPE"];
        eachSpriteData.spriteX = [rs intForColumn:@"SPRITE_X"];
        eachSpriteData.spriteY = [rs intForColumn:@"SPRITE_Y"];
        eachSpriteData.mapX = [rs intForColumn:@"MAP_X"];
        eachSpriteData.mapY = [rs intForColumn:@"MAP_Y"];
        eachSpriteData.mapW = [rs intForColumn:@"MAP_W"];
        eachSpriteData.mapH = [rs intForColumn:@"MAP_H"];
        eachSpriteData.mActive = [rs intForColumn:@"M_ACTIVE"];
        eachSpriteData.mIdle = [rs intForColumn:@"M_IDLE"];
        
        [self.mapDrawView.drawStationsArray addObject:eachSpriteData];
    }
    
    [rs close];
    
    self.mapDrawView.scrollZoomScale = ratio*self.scrollView.zoomScale;
    self.blankView.alpha = 0.7;
    [self.mapDrawView setNeedsDisplay];
    [self.mapImageView setNeedsDisplay];
}

-(void)reDrawMapDrawing
{
//    self.mapDrawView.scrollZoomScale = self.scrollView.zoomScale;
//    self.mapDrawView.scrollZoomScale =ratio*self.scrollView.zoomScale;
//    [self.mapDrawView setNeedsDisplay];
    [self.mapImageView setNeedsDisplay];
}



#pragma mark - Activity Indicator methods

-(void)showActivityView
{
    UIWindow *window = self.appdelegate.window;
    self.activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [self.activityView addSubview:activityWheel];
    [window addSubview: self.activityView];
    
    [[[self.activityView subviews] objectAtIndex:0] startAnimating];
}

-(void)hideActivityView
{
    [[[self.activityView subviews] objectAtIndex:0] stopAnimating];
    [self.activityView removeFromSuperview];
    self.activityView = nil;
}

-(void) setupMap
{

    if(self.viewLoaded)
        return;
    
    CGRect viewRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, [self.navigationController navigationBar].frame.size.height+[UIApplication sharedApplication].statusBarFrame.size.height ); //+46 with search
    
    //to cover our navigation bar, add a filler view.
    self.fillerView = [[UIView alloc] initWithFrame:viewRect];
    
    [self.view addSubview:self.fillerView];
    
    viewRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+self.fillerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.fillerView.frame.size.height);
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:viewRect];
    
    self.scrollView.delegate = self;
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    self.scrollView.clipsToBounds = YES; // default is NO, we want to restrict drawing within our scrollview
    
    self.scrollView.bounces = NO;
    self.scrollView.bouncesZoom = NO;
    self.scrollView.minimumZoomScale = 1.0f;
    self.scrollView.maximumZoomScale = 4.0f;
    
    NSString *imageFile = [NSString stringWithFormat:@"%@/mapall.png", [[NSBundle mainBundle] resourcePath]];
    
    self.mapImage = [UIImage imageWithContentsOfFile:imageFile];
    self.mapImageView = [[UIImageView alloc] initWithImage:self.mapImage];
    self.mapImageView.userInteractionEnabled = YES;
    
    self.blankView = [[UIView alloc] initWithFrame:self.mapImageView.frame];
    self.blankView.backgroundColor = [UIColor whiteColor];
    self.blankView.alpha = 0.0f;
    [self.mapImageView addSubview:self.blankView];
    
//    self.mapDrawView = [[MapStationHighlightView alloc] initWithFrame:self.mapImageView.frame];
//    self.mapDrawView.backgroundColor = [UIColor clearColor];
//    [self.mapImageView addSubview:self.mapDrawView];
    
    viewRect = CGRectMake(0,self.fillerView.frame.size.height,self.mapImageView.image.size.width,self.mapImageView.image.size.height);
    
    [self.scrollView addSubview:self.mapImageView];
    
    ratio = CGRectGetWidth(self.scrollView.bounds) / self.mapImage.size.width;
    self.mapImageView.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), self.mapImage.size.height * ratio);
    self.mapImageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    self.mapImageView.image = self.mapImage;
    
//    self.mapDrawView.baseImageRatio = ratio;
    
    [self.scrollView setContentSize:CGSizeMake(viewRect.size.width, viewRect.size.height)];
    
    [self.view addSubview:self.scrollView];
    
    NSLog(@"Ratio = %f", ratio);
    NSLog(@"ZOOM SCALE: %f",self.scrollView.zoomScale);
//    self.mapDrawView.scrollZoomScale = ratio*self.scrollView.zoomScale;
    
//    [self initZoom];
    
    //    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    
//    self.pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"marker point"]];
//    self.droppedPinView = [[UIView alloc] init];
//    self.droppedPinView.frame = self.mapImageView.frame;
//    self.droppedPinView.backgroundColor = [UIColor clearColor];
//    self.droppedPinView.tag = 901;
//    [self.mapImageView addSubview:self.droppedPinView];

    
    [self hideActivityView];

    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(reDrawMapDrawing)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [self.view bringSubviewToFront:self.searchInputView];

}

-(void)rescaleItemMarkers {
    
    float initialMapScale = self.currentPinZoomFactor;
    float finalMapScale = self.scrollView.zoomScale;
    
    // Clamp final map scales
    if (finalMapScale < 0.25) {
        finalMapScale = 0.25;
    } else if (finalMapScale > 1.0){
        finalMapScale = 1.0;
    }
    
    float scalingFactor = finalMapScale / initialMapScale;
    
    float pinCorrectionFactor = 1 / scalingFactor;
    
    CAMediaTimingFunction *easingCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CABasicAnimation *xScaleAnimation;
    xScaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
    xScaleAnimation.timingFunction = easingCurve;
    xScaleAnimation.duration=0.3;
    xScaleAnimation.repeatCount=0;
    xScaleAnimation.autoreverses=NO;
    xScaleAnimation.removedOnCompletion = NO;
    xScaleAnimation.fillMode = kCAFillModeForwards;
    xScaleAnimation.fromValue = [NSNumber numberWithFloat:self.currentPinZoomFactor];
    xScaleAnimation.toValue=[NSNumber numberWithFloat:pinCorrectionFactor];
    
    CABasicAnimation *yScaleAnimation;
    yScaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.y"];
    yScaleAnimation.timingFunction = easingCurve;
    yScaleAnimation.duration=0.3;
    yScaleAnimation.repeatCount=0;
    yScaleAnimation.autoreverses=NO;
    yScaleAnimation.removedOnCompletion = NO;
    yScaleAnimation.fillMode = kCAFillModeForwards;
    yScaleAnimation.fromValue = [NSNumber numberWithFloat:self.currentPinZoomFactor];
    yScaleAnimation.toValue=[NSNumber numberWithFloat:pinCorrectionFactor];
    
/*    for (UIView *theView in self.droppedPinView.subviews) {
//        if(theView.tag == 901){
            CALayer *layer = theView.layer;
            [layer addAnimation:xScaleAnimation forKey:@"animateScaleX"];
            [layer addAnimation:yScaleAnimation forKey:@"animateScaleY"];
//        }
    }
 */
    
    self.currentPinZoomFactor = pinCorrectionFactor;
    
}
@end
