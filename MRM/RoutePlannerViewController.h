//
//  RoutePlannerViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/30/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationSelectionViewController.h"
#import "FavouritesAndRecentsTableDelegate.h"
#import "SlideMenuViewController.h"
#import "MessageUI/MessageUI.h"

@interface RoutePlannerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, StationSelectionDelegate, UIScrollViewDelegate, ShowSavedSearchProtocol, SlideMenuDelegate,MFMailComposeViewControllerDelegate>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@property (strong, nonatomic) IBOutlet UILabel *fromStationLabel;
@property (strong, nonatomic) IBOutlet UILabel *toStationLabel;
@property (strong, nonatomic) IBOutlet UIImageView *viaDropdownArrowImage;
@property (strong, nonatomic) IBOutlet UIImageView *swapButtonImage;
@property (strong, nonatomic) IBOutlet UIView *searchJourneyBgView;

@property int fromStationId;
@property int toStationId;
@property (strong, nonatomic) NSArray *viaOptionsList;
@property int selectedViaOptionIndex;
@property (strong, nonatomic) IBOutlet UIScrollView *searchPageScrollView;

-(void)didSelectStationWithName:(NSString *)stationName andStationId:(NSString *)stationId selectedAs:(NSString *)type;
-(void) showResultsPageWithSavedSearchObject:(SavedJourney *)savedJourney;
@end

