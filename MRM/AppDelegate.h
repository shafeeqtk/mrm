//
//  AppDelegate.h
//  MRM
//
//  Created by Shafeeq Rahiman on 10/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "FullJourney.h"
#import "Station.h"
//#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FMDatabase* db;
@property (strong, nonatomic) NSString *databasePath;
@property (strong, nonatomic) NSString *dbkey;

@property (strong, nonatomic) NSArray* stationsMaster;
@property (strong, nonatomic) NSArray* linesMaster;
@property (strong, nonatomic) NSArray* lineRouteDetails; //all stations in each route(every stop)
@property (strong, nonatomic) NSArray* lineDraw;
@property (strong, nonatomic) FullJourney *journey;

@property (retain, nonatomic) UIWindow *statusBarBackground;

@property CGPoint startStationMapPoint;
@property CGPoint endStationMapPoint;


@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory; // nice to have to reference files for core data


-(NSArray *) loadStationsMaster;
-(NSArray *) loadLinesMaster;
-(NSArray *) loadLineDraw;

-(BOOL)isStationOnSameLine:(int)firstStationId asComparedToStationId:(int)secondStationId;
-(int)getCommonLineIdBetweenStation:(int)firstStationId secondStationId:(int)secondStationId;
-(NSArray *)listOfStationsBetweenStation:(int)stationAId andStation:(int)stationBId onLine:(int)lineId;

-(Station *)getStationFromStationId:(int)stationId;
//-(NSArray *) listOfStationsBetweenStation:(int)stationAId andStation:(int)stationBId;

-(UIColor *)getUIColorForLine:(int)lineNumber;
-(NSString *) getLineNameFromLineId:(int)lineId;
@end

