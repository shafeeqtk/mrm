//
//  LineDraw.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LineDraw : NSObject

@property int _id;
@property int lineId;
@property int fromStationId;
@property int toStationId;
@property NSString *drawScript;
@property NSString *strokeColor;
@property int strokeWidth;

@end
