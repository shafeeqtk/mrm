//
//  ExpandingTableViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 1/2/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpandingTableViewController : UITableViewController


@property (retain) NSIndexPath* selectedIndexPath;

@end
