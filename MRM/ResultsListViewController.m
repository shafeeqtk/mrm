//
//  ResultsListViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/3/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ResultsListViewController.h"
#import "RouteIntermediateActionsView.h"
#import "RouteResultContentView.h"
#import "IntermediateStationsListView.h"
#import "AppDelegate.h"
#import "RouteStation.h"
#import "AdvancedStationSelectionTableViewController.h"
#import "ViaSelectionTableViewDelegate.h"
#import "SavedJourney.h"
#import "ResultsExtrasView.h"
#import "ResultsMapViewController.h"



#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define URLEMail @"contact@mrmapp.in"


@interface ResultsListViewController ()
@property (strong, nonatomic) IBOutlet UIBarButtonItem *mapViewButton;
@property (strong, nonatomic) IBOutlet UIImageView *swapButtonImage;
@property (strong, nonatomic) IBOutlet UIView *viaSelectionView;
@property (strong, nonatomic) IBOutlet UIView *fromToSwapView;

@property (strong, nonatomic) IBOutlet UIView *timeSelectionView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *timeSelectionViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viaRouteViewHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopVerticalSpaceConstraint;
@property (strong, nonatomic) AdvancedStationSelectionTableViewController *tlc;
@property (strong, nonatomic) UIView *maskFullScreenView;

@property (nonatomic, strong, retain) AppDelegate *appdelegate;

@property (strong, nonatomic) IBOutlet UIScrollView *contentScrollView;

@property (nonatomic,retain) UIView *activityView;

//@property int journeyStartTimeInMins;
@property bool isReverseJourney;
@property bool viaTableDelegateObserverAdded;
@property (strong, nonatomic) IBOutlet UIView *datePickerBgView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UILabel *datePickerSelectedTimeLabel;

@property (strong, nonatomic) ViaSelectionTableViewDelegate *viaTableDelegate;
@property (strong, nonatomic) ResultsExtrasView *extraView;

@property (strong, nonatomic) UIView* statusBg;
@property bool isDateChangedFromDatePicker;

@property (strong, nonatomic) NSString *routeSMSStr;
@property (strong, nonatomic) NSString *routeSMSTitleStr;
@end

@implementation ResultsListViewController



enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;

#pragma mark - Find Phone Model


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}

-(AppDelegate *)appdelegate{
    if(!_appdelegate)
        _appdelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appdelegate;
}

-(void)dealloc
{
    self.contentScrollView.delegate = nil;
}

CGFloat floatingBarOriginalY;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self findPhoneType];
    self.viaTableDelegateObserverAdded = NO;
    // Do any additional setup after loading the view.
    
    self.fromStationLabel.text = self.fromStationText;
    self.toStationLabel.text = self.toStationText;
    self.selectedViaRouteNameLabel.text = self.selectedViaRouteNameText;
    self.selectedViaRouteInterchangesLabel.text = self.selectedViaRouteInterchangesText;
    self.selectedViaRouteEstimatedTimeLabel.text = self.selectedViaRouteEstimatedTimeText;
    self.selectedTimeLabel.text = self.selectedTimeText;
    self.selectedDayLabel.text = self.selectedDayText;
    
    self.debugTextView.text = self.debugText;
    
    self.timeSelectionView.layer.shadowOpacity = 1;
    self.timeSelectionView.layer.shadowRadius = 2;
    self.timeSelectionView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.timeSelectionView.layer.shadowOffset = CGSizeMake(0, 0);
    
    floatingBarOriginalY = self.timeSelectionView.frame.origin.y;
    
    [self populateJourneyContent];
    
    self.viaTableDelegate = [[ViaSelectionTableViewDelegate alloc] initWithTableView:self.viaListTableView];
    //    [self.favRecentTableView setNeedsDisplay];
    
    [self.viaTableDelegate addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    self.viaTableDelegateObserverAdded = YES;
    self.viaTableDelegate.delegate = (id<AdvancedStationSelectionDelegate>)self;
    self.viaListTableView.hidden = YES;
    
    
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, self.timeSelectionView.frame.size.height-1, self.view.frame.size.width, 1.0f);
    
    bottomBorder.backgroundColor = [[UIColor blackColor]
                                    colorWithAlphaComponent:0.1f].CGColor;
    
    [self.timeSelectionView.layer addSublayer:bottomBorder];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    if (self.viaTableDelegateObserverAdded)
    {
        [self.viaTableDelegate removeObserver:self forKeyPath:@"contentSize" context:NULL];
        self.viaTableDelegateObserverAdded = NO;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"Memory warning from ResultsListViewController");
}

bool swapFlag=YES;
- (IBAction)swapRouteButtonTapped:(UIButton *)sender {
    
    self.isReverseJourney = YES;
    bool isRecalculationSuccessful = [self recalculateRoute];
    
    if( ! isRecalculationSuccessful )
    {
        return;
    }
    
    [self rotateImage:self.swapButtonImage duration:0.2 curve:UIViewAnimationCurveEaseIn degrees:swapFlag?180:-360];
    swapFlag = !swapFlag;
    
    CGRect view1Frame = self.fromStationLabel.frame;
    CGRect view2Frame = self.toStationLabel.frame;
    
    if (!CGRectIntersectsRect(self.fromStationLabel.frame, self.toStationLabel.frame)) {
        
        self.fromStationLabel.frame = view2Frame;
        self.toStationLabel.frame = view1Frame;
        NSString *tmp;
        tmp = self.fromStationLabel.text;
        self.fromStationLabel.text = self.toStationLabel.text;
        self.toStationLabel.text = tmp;
        
        view1Frame = self.fromStationLabel.frame;
        view2Frame = self.toStationLabel.frame;
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.fromStationLabel.frame = view2Frame;
                             self.toStationLabel.frame = view1Frame;
                         }
                         completion:nil
         ];
    }
    
    
}

-(BOOL)recalculateRoute
{
    bool errorFlag = NO;
    
    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    
    if(self.isReverseJourney)
    {
        int tmpStnId = self.appdelegate.journey.startStationId;
        self.appdelegate.journey.startStationId = self.appdelegate.journey.endStationId;
        self.appdelegate.journey.endStationId = tmpStnId;
        
        NSString *tmpStnName = self.appdelegate.journey.startStationName;
        self.appdelegate.journey.startStationName = self.appdelegate.journey.endStationName;
        self.appdelegate.journey.endStationName = tmpStnName;
    
//    self.journeyStartTimeInMins = [self minutesSinceMidnight:[NSDate date]];
        
    if(self.isDateChangedFromDatePicker)
    {
        self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:self.datePicker.date];
        self.appdelegate.journey.departureDay = [self dayOfWeekFromDate:self.datePicker.date];
    }
    else{
        self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
    }
    [self.appdelegate.journey generateViaList];
    
        if(self.appdelegate.journey.viaRoutesList.count > 0)
        {
            self.appdelegate.journey.selectedViaIndex = 0;
            [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
        }else
        {
            errorFlag = YES;
        }
    }else{
        if(self.isDateChangedFromDatePicker)
        {
            self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:self.datePicker.date];
        }
        else{
            self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
        }
        [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
    }
    
    NSArray *selectedViaTextArray;
    int totalNoOfJourneyLegs = -1;
    
    if(!errorFlag)
    {
     selectedViaTextArray = [[self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex] componentsSeparatedByString:@"-"];
    totalNoOfJourneyLegs = self.appdelegate.journey.partJournies.count;
    }
    
    
    if(errorFlag){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Return journey unavailable" message:@"No return journeys could be found." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else if ([selectedViaTextArray.firstObject isEqualToString:@"Direct Connectivity"]){
        if(totalNoOfJourneyLegs == 0 )
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            errorFlag = YES;
        }
    }else if(totalNoOfJourneyLegs == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        errorFlag = YES;;
    }else if(! selectedViaTextArray.count+1 == totalNoOfJourneyLegs)  //+1 to account for the source and destination switches
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        errorFlag = YES;
    }
    
    if(errorFlag)
    {
        if(self.isReverseJourney){
            int tmpStnId = self.appdelegate.journey.startStationId;
            self.appdelegate.journey.startStationId = self.appdelegate.journey.endStationId;
            self.appdelegate.journey.endStationId = tmpStnId;
            NSString *tmpStnName = self.appdelegate.journey.startStationName;
            self.appdelegate.journey.startStationName = self.appdelegate.journey.endStationName;
            self.appdelegate.journey.endStationName = tmpStnName;
            

        }
        [self hideActivityView];
        return NO;
    }
    
    self.journey = self.appdelegate.journey;
    
    
    for (UIView *subview in self.contentScrollView.subviews)
    {
        [subview removeFromSuperview];
    }
    [self.view layoutIfNeeded];
    
    self.selectedViaRouteNameLabel.text = [self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex];
    int noOfInterchanges = self.appdelegate.journey.partJournies.count - 1;
    self.selectedViaRouteInterchangesLabel.text = [NSString stringWithFormat:@"%@ %@",noOfInterchanges == 0 ?@"No":@(noOfInterchanges).stringValue, noOfInterchanges > 1?@"interchanges":@"interchange"];
    
    NSArray *firstPartJourney = [self.appdelegate.journey.partJournies firstObject];
    NSArray *lastPartJourney = [self.appdelegate.journey.partJournies lastObject];
    
    //START - On Results page, set header source and destination station names.
    RouteStation *homeStation = [firstPartJourney firstObject];
    RouteStation *ultimateStation = [lastPartJourney lastObject];
    
    int hrs = self.appdelegate.journey.departureTimeInMinutes/60;
    int mins = self.appdelegate.journey.departureTimeInMinutes%60;
    if(self.isDateChangedFromDatePicker)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm, EEE, dd/MM "];
        self.selectedTimeLabel.text = [formatter stringFromDate:self.datePicker.date];
//        self.selectedTimeLabel.text = [NSString stringWithFormat:@"%@:%@ ",@(hrs>12?hrs%12:hrs).stringValue, @(mins).stringValue];
    }else
    {
        self.selectedTimeLabel.text = @"Now";
        self.datePickerSelectedTimeLabel.text = @"Now";
    }
//    self.selectedViaRouteEstimatedTimeLabel.text = [NSString stringWithFormat:@"%@:%@",@(hrs>12?hrs%12:hrs).stringValue, @(mins).stringValue];
//    
//    self.selectedViaRouteEstimatedTimeLabel.text = @"Now";
    
    if(ultimateStation.timeInMins-homeStation.timeInMins > 0)
    {
        hrs = (ultimateStation.timeInMins-homeStation.timeInMins)/60;
        mins = (ultimateStation.timeInMins-homeStation.timeInMins)%60;
    }
    else
    {
        hrs = (ultimateStation.timeInMins+1440-homeStation.timeInMins)/60;
        mins = (ultimateStation.timeInMins+1440-homeStation.timeInMins)%60;
    }
    
    if(hrs >= 1)
    {
        self.selectedViaRouteEstimatedTimeLabel.text = [NSString stringWithFormat:@"%@ %@ %@ Mins",@(hrs).stringValue,hrs == 1 ?@"Hr":@"Hrs", @(mins).stringValue];
    }else {
        self.selectedViaRouteEstimatedTimeLabel.text = [NSString stringWithFormat:@"%@ Mins", @(mins).stringValue];
    }

    
//    self.selectedViaRouteEstimatedTimeLabel.text = self.selectedViaRouteEstimatedTimeText;
//    self.selectedTimeLabel.text = self.selectedTimeText;
//    self.selectedDayLabel.text = self.selectedDayText;
    
    
    [self populateJourneyContent];
    [self hideActivityView];
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"showMapView"])
    {
        ResultsMapViewController *mapvc = [segue destinationViewController];
//        [mapvc setupMap];
        return;
    }else if([segue.identifier isEqualToString:@"advancedTrainSelection"])
    {
        
    }
}


#pragma mark - Image rotation code

// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

- (void)rotateImage:(UIImageView *)image duration:(NSTimeInterval)duration
              curve:(int)curve degrees:(CGFloat)degrees
{
    // Setup the animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // The transform matrix
    CGAffineTransform transform =
    CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    image.transform = transform;
    
    // Commit the changes
    [UIView commitAnimations];
}

-(void) changeTextForLabel:(UILabel *) textLabel withText:(NSString *)newText
{
    // Add transition (must be called after myLabel has been displayed)
    CATransition *animation = [CATransition animation];
    animation.duration = 0.5;
    animation.type = kCATransitionFade;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [textLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
    
    // Change the text
    textLabel.text = newText;
}


CGFloat lastContentOffset;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollDirection scrollDirection=ScrollDirectionCrazy;
    /*if (lastContentOffset > scrollView.contentOffset.x)
     scrollDirection = ScrollDirectionRight;
     else if (lastContentOffset < scrollView.contentOffset.x)
     scrollDirection = ScrollDirectionLeft;
     else*/
    if (lastContentOffset > scrollView.contentOffset.y)
        scrollDirection = ScrollDirectionDown;
    else if (lastContentOffset < scrollView.contentOffset.y)
        scrollDirection = ScrollDirectionUp;
    
//    NSLog(@"%f",lastContentOffset);
    
    if(abs(scrollView.contentOffset.y - lastContentOffset) < 1) return;
    
    if(scrollView.contentOffset.y <= 20) return;
    lastContentOffset = scrollView.contentOffset.y;
    
    // do whatever you need to with scrollDirection here.
    
    if(scrollDirection == ScrollDirectionUp)
    {
        if(self.timeSelectionView.frame.origin.y < floatingBarOriginalY )
            return;
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
        self.timeSelectionView.frame = CGRectMake(self.timeSelectionView.frame.origin.x,self.timeSelectionView.frame.origin.y-100, self.timeSelectionView.frame.size.width, self.timeSelectionView.frame.size.height);
            self.viaSelectionView.frame = CGRectMake(self.viaSelectionView.frame.origin.x,self.viaSelectionView.frame.origin.y-50, self.viaSelectionView.frame.size.width, self.viaSelectionView.frame.size.height);
            self.contentViewTopVerticalSpaceConstraint.constant = 0;
        }
                         completion: nil
         ];
//        self.timeSelectionViewHeightConstraint.constant = 0;
//        self.viaRouteViewHeightConstraint.constant = 0;
    }else if (scrollDirection == ScrollDirectionDown)
    {
        if(self.timeSelectionView.frame.origin.y == floatingBarOriginalY )
            return;
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
        self.timeSelectionView.frame = CGRectMake(self.timeSelectionView.frame.origin.x,self.timeSelectionView.frame.origin.y+100, self.timeSelectionView.frame.size.width, self.timeSelectionView.frame.size.height);
        self.viaSelectionView.frame = CGRectMake(self.viaSelectionView.frame.origin.x,self.viaSelectionView.frame.origin.y+50, self.viaSelectionView.frame.size.width, self.viaSelectionView.frame.size.height);
            self.contentViewTopVerticalSpaceConstraint.constant = 0;
        }
                         completion:nil
         ];
//        self.timeSelectionViewHeightConstraint.constant = 50;
//        self.viaRouteViewHeightConstraint.constant = 50;
    }
}


-(void) populateJourneyContent
{
    
    self.contentScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIScrollView *sv = self.contentScrollView;
    
    
    //////////////////
    RouteResultContentView* previousPartialJourneyDetailView = nil;
    RouteIntermediateActionsView *previousActionView = nil;
    NSMutableString *mutStr = [[NSMutableString alloc] init];
    NSMutableString *mutStrTitle = [[NSMutableString alloc] init];
    bool routeExists = NO;
//    [mutStr appendString:@"\n===== START JOURNEY =====\n"];
    int actionNumber = 1;
    
    
    for(NSArray *partJourney in self.journey.partJournies)
    {
        
        RouteIntermediateActionsView *actionView = [[RouteIntermediateActionsView alloc] init];
        if(partJourney == [self.journey.partJournies firstObject]){
            //Start journey View
            RouteStation *routeStation = [partJourney firstObject];
            UIColor *currentLineColor = [self.appdelegate getUIColorForLine:routeStation.line];
            NSString *lineName = [self.appdelegate getLineNameFromLineId:routeStation.line];
            actionView.actionMessageLabel.textColor = currentLineColor;
            actionView.actionMessageLabel.text = [NSString stringWithFormat:@"%d. START ON %@", actionNumber++, [lineName uppercaseString]];
            
            actionView.translatesAutoresizingMaskIntoConstraints = NO;
            [sv addSubview:actionView];
            [sv addConstraints:
             [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[actionView]|"
                                                     options:0 metrics:nil
                                                       views:@{@"actionView":actionView}]];
            [sv addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(40)-[actionView]"
                                                                        options:0
                                                                        metrics:nil
                                                                          views:@{@"actionView":actionView}]];
           [sv addConstraint:
             [NSLayoutConstraint constraintWithItem:actionView
                                          attribute:NSLayoutAttributeCenterX
                                          relatedBy:NSLayoutRelationEqual
                                             toItem:actionView.superview
                                          attribute:NSLayoutAttributeCenterX
                                         multiplier:1.f constant:0.f]];
            previousActionView = actionView;
        } else {
            //Change train View
            RouteStation *routeStation = [partJourney firstObject];
            UIColor *currentLineColor = [self.appdelegate getUIColorForLine:routeStation.line];
            NSString *lineName = [self.appdelegate getLineNameFromLineId:routeStation.line];
            actionView.actionMessageLabel.textColor = currentLineColor;
            actionView.actionMessageLabel.text = [NSString stringWithFormat:@"%d. NEXT TRAIN ON %@", actionNumber++,  [lineName uppercaseString] ];
            actionView.translatesAutoresizingMaskIntoConstraints = NO;
            [sv addSubview:actionView];
            [sv addConstraints:
             [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[actionView]|"
                                                     options:0 metrics:nil
                                                       views:@{@"actionView":actionView}]];
            [sv addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousPartialJourneyDetailView]-(0)-[actionView]"
                                                                        options:0 metrics:nil
                                                                          views:@{@"previousPartialJourneyDetailView":previousPartialJourneyDetailView, @"actionView":actionView}]];
            [sv addConstraint:
             [NSLayoutConstraint constraintWithItem:actionView
                                          attribute:NSLayoutAttributeCenterX
                                          relatedBy:NSLayoutRelationEqual
                                             toItem:actionView.superview
                                          attribute:NSLayoutAttributeCenterX
                                         multiplier:1.f constant:0.f]];
            previousActionView = actionView;
            //            [mutStr appendString:@"\n===== Change Train =====\n"];
        }
        int noOfHaltsInThisLeg = 0;
        int minutesOnThisLeg = 0;
        int noOfStationsInThisLeg = [partJourney count];
        
        RouteResultContentView* partialJourneyDetailView = [[RouteResultContentView alloc] init];
        partialJourneyDetailView.delegate = self;
        for(RouteStation *routeStation in partJourney){
            routeExists = YES;
            if (routeStation == partJourney.firstObject)
            {
                minutesOnThisLeg = routeStation.timeInMins;
//                partialJourneyDetailView.lineColor = [self.appdelegate getUIColorForLine:routeStation.line];
                UIColor *lineColor = [self.appdelegate getUIColorForLine:routeStation.line];
                [partialJourneyDetailView setLineColorForLine: lineColor];
                partialJourneyDetailView.startStationLabel.text = routeStation.stationName;
                partialJourneyDetailView.startStationId = routeStation.station_id.intValue;
                partialJourneyDetailView.lineId = routeStation.line;
                partialJourneyDetailView.direction = routeStation.trainDirection;
                
                partialJourneyDetailView.departureTimeLabel.text = routeStation.time;
                partialJourneyDetailView.startStationPlatformNoLabel.text = [routeStation.platform_no isEqualToString:@""]?@"":[NSString stringWithFormat:@"PF: %@",routeStation.platform_no];
                
                partialJourneyDetailView.trainInfoFromStationLabel.text = [NSString stringWithFormat:@"Coming from %@",routeStation.trainStartedFromStationName];
                partialJourneyDetailView.trainInfoTowardsStationName.text = [NSString stringWithFormat:@"%@ - %@", routeStation.trainGoingTowardsStationName, [routeStation.speed isEqualToString:@"F"]?@"Fast":@"Slow"];
                partialJourneyDetailView.trainInfoTowardsStationCode.text = routeStation.trainGoingTowardsStationCode;
                partialJourneyDetailView.trainInfoTowardsStationCode.textColor = lineColor;
                partialJourneyDetailView.trainInfoNoOfCar.text = [NSString stringWithFormat:@"%d Car",routeStation.no_of_car];
                partialJourneyDetailView.trainInfoNoOfCar.textAlignment = NSTextAlignmentLeft;
                partialJourneyDetailView.trainInfoSpecialTrainInfo.text = routeStation.specialInfo;
//                partialJourneyDetailView.trainInfoFromStationLabel = routeStation
                partialJourneyDetailView.timeInMins = routeStation.timeInMins;
                
                [mutStrTitle appendString: [NSString stringWithFormat:@"%@ > ",routeStation.stationName]];
                
                if(partJourney == [self.journey.partJournies firstObject])
                {
                    [mutStr appendString:@"\nFrom "];
                    
                }else{
                    [mutStr appendString:@"\n\nBoard at "];
                }
                [mutStr appendString:[NSString stringWithFormat:@"%@ %@ %@\n%@ %@ %@ %d%@",routeStation.stationName,
                                      [partialJourneyDetailView.startStationPlatformNoLabel.text isEqualToString:@""]?@"":@"on",
                                      partialJourneyDetailView.startStationPlatformNoLabel.text,
                                      routeStation.time,
                                      (routeStation.trainGoingTowardsStationCode == nil) ?@"":[NSString stringWithFormat:@"[%@]",routeStation.trainGoingTowardsStationCode],
                                      routeStation.trainGoingTowardsStationName,
                                      routeStation.no_of_car,
                                      routeStation.speed]];

            }else if(routeStation == partJourney.lastObject)
            {
                minutesOnThisLeg = routeStation.timeInMins - minutesOnThisLeg;
                partialJourneyDetailView.trainInfoStopsAndTimeLabel.text = [NSString stringWithFormat:@"%@ halts, %d stations, %d mins",noOfHaltsInThisLeg==0?@"No":@(noOfHaltsInThisLeg).stringValue, noOfStationsInThisLeg-2,minutesOnThisLeg];
                [partialJourneyDetailView addIntermediateStations];
                partialJourneyDetailView.endStationLabel.text = routeStation.stationName;
                partialJourneyDetailView.endStationId = routeStation.station_id.intValue;
                
                partialJourneyDetailView.arrivalTimeLabel.text = routeStation.time;
                partialJourneyDetailView.endStationPlatformNoLabel.text = [routeStation.platform_no isEqualToString:@""]?@"":[NSString stringWithFormat:@"PF: %@",routeStation.platform_no];
                
                if([routeStation.platform_side isEqualToString:@"-"] || [routeStation.platform_side isEqualToString:@""])
                {
                    partialJourneyDetailView.endStationPlatformSideWidthConstraint.constant = 0;
                }
                partialJourneyDetailView.endStationPlatformSideLabel.text = routeStation.platform_side;
                
                if(noOfStationsInThisLeg == 2) //ie, if only start and end stations and nothing in between
                {
                    partialJourneyDetailView.trainInfoToggleRouteDetailsImageView.hidden = YES;
                    partialJourneyDetailView.trainInfoToggleRouteDetailsButton.enabled = NO;
                }
                
            }else{
                if(![routeStation.time isEqualToString:@""])
                {
                    noOfHaltsInThisLeg++;
                }
                [partialJourneyDetailView addStationToList:routeStation.stationName atTime:routeStation.time];
            }
//            [mutStr appendString:[NSString stringWithFormat:@"%@ at %@ ",routeStation.stationName, routeStation.time]];
//            [mutStr appendString:@"\n"];
            //TODO: Check if this change affected anything
//            partialJourneyDetailView.timeInMins = routeStation.timeInMins;
        }
        //Add the journey view to ScrollView
        partialJourneyDetailView.translatesAutoresizingMaskIntoConstraints = NO;
        [sv addSubview:partialJourneyDetailView];
        [sv addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[partialJourneyDetailView]|"
                                                 options:0 metrics:nil
                                                   views:@{@"partialJourneyDetailView":partialJourneyDetailView}]];
        
        /*Use this code to center the journey horizontally - START*/
//        [sv addConstraint:
//         [NSLayoutConstraint constraintWithItem:partialJourneyDetailView
//                                      attribute:NSLayoutAttributeCenterX
//                                      relatedBy:NSLayoutRelationEqual
//                                         toItem:partialJourneyDetailView.superview
//                                      attribute:NSLayoutAttributeCenterX
//                                     multiplier:1.f constant:0.f]];
        /*Use this code to center the journey horizontally - END*/
//        if (!previousPartialJourneyDetailView) { // first one, pin to top
            [sv addConstraints:
             [NSLayoutConstraint constraintsWithVisualFormat:@"V:[actionView]-(0)-[partialJourneyDetailView]"
                                                     options:0 metrics:nil
                                                       views:@{@"partialJourneyDetailView":partialJourneyDetailView, @"actionView":previousActionView}]];
//        } else { // all others, pin to previous Action View
           /* [sv addConstraints:
             [NSLayoutConstraint
              constraintsWithVisualFormat:@"V:[prev]-(-10)-[partialJourneyDetailView]"
              options:0 metrics:nil
              views:@{@"partialJourneyDetailView":partialJourneyDetailView, @"prev":previousPartialJourneyDetailView}]];
        }*/
        previousPartialJourneyDetailView = partialJourneyDetailView;
    }
    //End journey View
    RouteIntermediateActionsView *actionView = [[RouteIntermediateActionsView alloc] init];
    actionView.actionMessageLabel.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    RouteStation *routeStation = [[self.appdelegate.journey.partJournies lastObject] lastObject];
    NSString *lineName = [self.appdelegate getLineNameFromLineId:routeStation.line];
    actionView.actionMessageLabel.text = [NSString stringWithFormat:@"%d. ARRIVE AT %@, ON %@", actionNumber++, routeStation.stationName  .uppercaseString, [lineName uppercaseString] ];
    [mutStr appendString:@"\n\nReaches "];
    [mutStr appendString:[NSString stringWithFormat:@"%@ at %@",routeStation.stationName,routeStation.time]];
    [mutStrTitle appendString: [NSString stringWithFormat:@"%@\n",routeStation.stationName]];
    self.routeSMSTitleStr = mutStrTitle;
    self.routeSMSStr = mutStr;
    actionView.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:actionView];
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[actionView]|"
                                             options:0 metrics:nil
                                               views:@{@"actionView":actionView}]];
        [sv addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousPartialJourneyDetailView]-(0)-[actionView]"
                                                                options:0 metrics:nil
                                                                  views:@{@"previousPartialJourneyDetailView":previousPartialJourneyDetailView, @"actionView":actionView}]];
    
    [sv addConstraint:
     [NSLayoutConstraint constraintWithItem:actionView
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:actionView.superview
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1.f constant:0.f]];
    
    self.extraView = [[ResultsExtrasView alloc] init];
    
    self.extraView.delegate = self;
    //set labels/etc here
    
    if (self.isFavourite)
    {
        self.extraView.favouriteIconImageView.image = [UIImage imageNamed:@"fev on"];
        self.extraView.favouriteTextLabel.text = @"Remove from Favourites";
        self.extraView.isFavouriteAdd = NO;
    }else {
        self.extraView.favouriteIconImageView.image = [UIImage imageNamed:@"fev off"];
        self.extraView.favouriteTextLabel.text = @"Add to Favourites";
        self.extraView.isFavouriteAdd = YES;
    }
    
    self.extraView.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:self.extraView];
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[extraView]|"
                                             options:0 metrics:nil
                                               views:@{@"extraView":self.extraView}]];
    [sv addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:[actionView]-(0)-[extraView]"
                                                                options:0 metrics:nil
                                                                  views:@{@"actionView":actionView, @"extraView":self.extraView}]];
    
    [sv addConstraint:
     [NSLayoutConstraint constraintWithItem:self.extraView
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.extraView.superview
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1.f constant:0.f]];

    // last one, pin to bottom and right, this dictates content size height
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:[extraView]-(10)-|"
                                             options:0 metrics:nil
                                               views:@{@"extraView":self.extraView}]];
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:[extraView]-(0)-|"
                                             options:0 metrics:nil
                                               views:@{@"extraView":self.extraView}]];
    
//    [mutStr appendString:@"\n===== END JOURNEY ====="];
//    if(!routeExists){
//        mutStr = [NSMutableString stringWithString:@"No trains to destination at this time. Try alternate Via route."];
//    }
    //////////////////
    
    
    
/*    RouteResultContentView* previousPartialJourneyDetailView = nil;
    for (int i=0; i<4; i++) {
        RouteResultContentView* partialJourneyDetailView = [[RouteResultContentView alloc] init];
//        partialJourneyDetailView.frame = CGRectMake(partialJourneyDetailView.frame.origin.x, partialJourneyDetailView.frame.origin.y, self.view.frame.size.width, partialJourneyDetailView.frame.size.height);
        partialJourneyDetailView.translatesAutoresizingMaskIntoConstraints = NO;
//        partialJourneyDetailView.startStationLabel.text = [NSString stringWithFormat:@"Station %i", i+1];
        [sv addSubview:partialJourneyDetailView];
        [sv addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[partialJourneyDetailView]|"
                                                 options:0 metrics:nil
                                                   views:@{@"partialJourneyDetailView":partialJourneyDetailView}]];

        / *Use this code to center the journey horizontally - START* /
        [sv addConstraint:
         [NSLayoutConstraint constraintWithItem:partialJourneyDetailView
                                      attribute:NSLayoutAttributeCenterX
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:partialJourneyDetailView.superview
                                      attribute:NSLayoutAttributeCenterX
                                     multiplier:1.f constant:0.f]];
        / *Use this code to center the journey horizontally - END* /
        
        if (!previousPartialJourneyDetailView) { // first one, pin to top
            [sv addConstraints:
             [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(50)-[partialJourneyDetailView]"
                                                     options:0 metrics:nil
                                                       views:@{@"partialJourneyDetailView":partialJourneyDetailView}]];
        } else { // all others, pin to previous
            [sv addConstraints:
             [NSLayoutConstraint
              constraintsWithVisualFormat:@"V:[prev]-(10)-[partialJourneyDetailView]"
              options:0 metrics:nil
              views:@{@"partialJourneyDetailView":partialJourneyDetailView, @"prev":previousPartialJourneyDetailView}]];
        }
        previousPartialJourneyDetailView = partialJourneyDetailView;
    }
    // last one, pin to bottom and right, this dictates content size height
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"V:[partialJourneyDetailView]-(10)-|"
                                             options:0 metrics:nil
                                               views:@{@"partialJourneyDetailView":previousPartialJourneyDetailView}]];
    [sv addConstraints:
     [NSLayoutConstraint constraintsWithVisualFormat:@"H:[partialJourneyDetailView]-(0)-|"
                                             options:0 metrics:nil
                                               views:@{@"partialJourneyDetailView":previousPartialJourneyDetailView}]];
    
    
*/
}

CGFloat topMargin = 50;
CGFloat originalWidth = 0;

-(void)loadAdvancedTrainSelectionTableViewFromStationId:(int)fromStationId
                                            toStationId:(int)toStationId
                                               onLineId:(int)lineId
                                           atTimeInMins:(int)timeInMins
                                            inDirection:(NSString *)direction
{
    
    
//    [self performSegueWithIdentifier:@"advancedTrainSelection" sender:self];
//    [self performSegueWithIdentifier:@"advancedTrainSelection"animated:YES];
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.4;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//    [self.navigationController pushViewController:advTrainVC animated:NO];//:advTrainVC animated:NO
    

    
    topMargin = self.view.frame.origin.y;
//    topMargin = 0;
    
    self.mapViewButton.enabled = NO;
    
    if(!self.tlc){
        originalWidth = self.view.frame.size.width;
        self.tlc = [self.storyboard instantiateViewControllerWithIdentifier:@"advancedStationSelection"];
        self.tlc.delegate = self;
        [self.tlc.view setFrame:CGRectMake(self.view.frame.size.width-44, topMargin, self.view.frame.size.width, self.view.frame.size.height)];
        [self addChildViewController:self.tlc];

        [self.view addSubview:self.tlc.view];
        self.tlc.view.hidden = YES;
        [self.tlc didMoveToParentViewController:self];
        [self.view layoutIfNeeded];
    }
    
//    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    
    [self.tlc
     setHeaderInfoWithTrainTowardsStationName:[NSString stringWithFormat:@"Trains towards %@", [self.appdelegate.journey findStationNameById:toStationId]]
     fromStationName:[self.appdelegate.journey findStationNameById:fromStationId]
     onLineName:[NSString stringWithFormat:@"%@ Line",[self.appdelegate.journey findLineNameById:lineId]]
     ];
    
    self.tlc.trainsList =  [self.appdelegate.journey listTrainsFromStationId:fromStationId
                                                                    toStationId:toStationId
                                                                       onLineId:lineId
                                                                   atTimeInMins:timeInMins
                                                                    inDirection:direction];
    int i=0;
    for(StationTrain *stationTrain in self.tlc.trainsList){
        if(stationTrain.isNextTrain){
            i++;
            break;
        }
        i++;
    }
    if(i == self.tlc.trainsList.count)
    {
        i = self.tlc.trainsList.count - 1;
    }
    [self.tlc.tableView reloadData];
    [self.tlc.tableView beginUpdates];
    [self.tlc.tableView endUpdates];
    [self.tlc.view layoutIfNeeded];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
    [self.tlc.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];

    int translateValue = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        if(phonemodel == Iphone5_5s || phonemodel == Iphone4_4s)
        {
            translateValue = -originalWidth+44;
        }else{
            translateValue = -originalWidth+44;
        }
        
    }else{
        translateValue = -originalWidth-224;//-originalWidth-154;
    }
//    int translateValue = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")?(-originalWidth+100):(-originalWidth-180);
    
//    [self hideActivityView];
    self.tlc.view.hidden = NO;
//    [self.tlc.navigationController setNavigationBarHidden:YES animated:YES];

    [UIView animateWithDuration:0.3 animations:^{
        [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(translateValue, 0)];
        [self.contentScrollView setTransform:CGAffineTransformMakeTranslation(translateValue, 0)];
        [self.timeSelectionView setTransform:CGAffineTransformMakeTranslation(translateValue, 0)];
        [self.viaSelectionView setTransform:CGAffineTransformMakeTranslation(translateValue, 0)];
        [self.fromToSwapView setTransform:CGAffineTransformMakeTranslation(translateValue, 0)];
        [self.tlc.view setTransform:CGAffineTransformMakeTranslation(-self.tlc.view.frame.size.width+44, 0)];
        
        self.statusBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self.appdelegate window].frame.size.width, 20)];
        self.statusBg.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:1.0];
        
        //Add the view behind the status bar
        [[self.appdelegate window].rootViewController.view addSubview:self.statusBg];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        

//        [self.contentScrollView setFrame:CGRectMake(-originalWidth+70, self.contentScrollView.frame.origin.y, self.view.frame.size.width, self.contentScrollView.frame.size.height)];
//        [self.timeSelectionView setFrame:CGRectMake(-originalWidth+70, self.timeSelectionView.frame.origin.y, self.view.frame.size.width, self.timeSelectionView.frame.size.height)];
//        [self.viaSelectionView setFrame:CGRectMake(-originalWidth+70, self.viaSelectionView.frame.origin.y, self.view.frame.size.width, self.viaSelectionView.frame.size.height)];
//        [self.fromToSwapView setFrame:CGRectMake(-originalWidth+70, self.fromToSwapView.frame.origin.y, self.view.frame.size.width, self.fromToSwapView.frame.size.height)];
//        [self.tlc.view setFrame:CGRectMake(0, self.tlc.view.frame.origin.y, self.tlc.view.frame.size.width, self.tlc.view.frame.size.height)];
    }];
    
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.tlc.view setFrame:CGRectMake(0, self.tlc.view.frame.origin.y, self.tlc.view.frame.size.width, self.tlc.view.frame.size.height)];
//    }];
    
}

-(void) hideTrainsListAndShowResultsScreen
{

    self.mapViewButton.enabled = YES;
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.contentScrollView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.timeSelectionView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.viaSelectionView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.fromToSwapView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.tlc.view setTransform:CGAffineTransformMakeTranslation(self.tlc.view.frame.size.width-44, 0)];
                         
                         for (UIView *subview in [[self.appdelegate window].rootViewController.view subviews])
                         {
                             if(subview == self.statusBg){
                                 [subview removeFromSuperview];
                                 break;
                             }
                         }
                         
                         [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
                         
//                         [self.contentScrollView setFrame:CGRectMake(0, self.contentScrollView.frame.origin.y, self.view.frame.size.width, self.contentScrollView.frame.size.height)];
//                         [self.timeSelectionView setFrame:CGRectMake(0, self.timeSelectionView.frame.origin.y, self.view.frame.size.width, self.timeSelectionView.frame.size.height)];
//                         [self.viaSelectionView setFrame:CGRectMake(0, self.viaSelectionView.frame.origin.y, self.view.frame.size.width, self.viaSelectionView.frame.size.height)];
//                         [self.fromToSwapView setFrame:CGRectMake(0, self.fromToSwapView.frame.origin.y, self.view.frame.size.width, self.fromToSwapView.frame.size.height)];
//                         [self.tlc.view setFrame:CGRectMake(self.view.frame.size.width-70, topMargin, self.view.frame.size.width, self.view.frame.size.height-topMargin)];
    }
                     completion:^(BOOL completion){
                         self.tlc.view.hidden = YES;
                     }
     ];
//    [UIView animateWithDuration:0.3
//                     animations:^{
//                         [self.tlc.view setFrame:CGRectMake(self.view.frame.size.width-70, topMargin, self.view.frame.size.width, self.view.frame.size.height-topMargin)];
//                     }];

}

-(void) updatePartOfJourneyAndSuccessorsAtTime:(int)startTime fromStationId:(int)stationId onLine:(int)lineId withTrainId:(int)trainId
{
    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    
    BOOL successFindingJourney = [self.appdelegate.journey findTrainsBetweenIntermediateStationId:stationId
                                                       andEndStation:self.appdelegate.journey.endStationId
                                                              atTime:startTime
                                                         withTrainId:trainId];
    if(!successFindingJourney)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot complete journey" message:@"The journey cannot be completed with the selected train. Please try another train or change via route." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else{
        for(UIView *eachView in [self.contentScrollView subviews])
        {
            [eachView removeFromSuperview];
        }
        
        [self populateJourneyContent];
        [self hideTrainsListAndShowResultsScreen];
    }
    [self hideActivityView];
}

#pragma mark - Activity Indicator methods

-(void)showActivityView
{
    UIWindow *window = self.appdelegate.window;
    self.activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [self.activityView addSubview:activityWheel];
    [window addSubview: self.activityView];
    
    [[[self.activityView subviews] objectAtIndex:0] startAnimating];
}

-(void)hideActivityView
{
    [[[self.activityView subviews] objectAtIndex:0] stopAnimating];
    [self.activityView removeFromSuperview];
    self.activityView = nil;
}

-(NSInteger) dayOfWeekFromDate:(NSDate *) date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    return [comps weekday];
}

-(int) minutesSinceMidnight:(NSDate *)date{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    int mins = 60 * [components hour] + [components minute];
    return mins;
}

- (IBAction)toggleViaRouteButtonTapped:(UIButton *)sender {
    int numberOfViaOptionsShownWithoutScroll = self.appdelegate.journey.viaRoutesList.count > 5? 5: self.appdelegate.journey.viaRoutesList.count;
    self.viaListTableViewHeightConstraint.constant = 60*numberOfViaOptionsShownWithoutScroll;
    if(self.viaListTableView.hidden)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.appdelegate.journey.selectedViaIndex inSection:0];
        [self.viaListTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    self.maskViewFullScreen.hidden = !self.maskViewFullScreen.hidden;
    self.viaListTableView.hidden = !self.viaListTableView.hidden;
    self.viaListContainerView.hidden = !self.viaListContainerView.hidden;
}

-(void)viaRouteSelected:(NSInteger)viaRouteIndex name:(NSString *)viaRouteName
{
    [self toggleViaRouteButtonTapped:nil];
    self.isReverseJourney = NO;
    [self recalculateRoute];
}
- (IBAction)hideViaRouteTableButtonTapped:(UIButton *)sender {
    self.maskViewFullScreen.hidden = YES;
    self.viaListTableView.hidden = YES;
    self.viaListContainerView.hidden = YES;
    self.datePickerBgView.hidden = YES;
}

- (IBAction)changeTimeButtonTapped:(UIButton *)sender {
    self.maskViewFullScreen.hidden = NO;
    self.datePickerBgView.hidden = NO;
}

- (IBAction)doneTimeChangeButtonTapped:(UIButton *)sender {
    self.maskViewFullScreen.hidden = YES;
    self.datePickerBgView.hidden = YES;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm, EEEE "];
    self.datePickerSelectedTimeLabel.text = [formatter stringFromDate:self.datePicker.date];
    
    self.isReverseJourney = NO;
    self.isDateChangedFromDatePicker = YES;
    [self recalculateRoute];
}

- (IBAction)nowButtonTapped:(UIButton *)sender {
    
    self.maskViewFullScreen.hidden = YES;
    self.datePickerBgView.hidden = YES;
    
    self.isReverseJourney = NO;
    self.isDateChangedFromDatePicker = NO;
    [self recalculateRoute];
    
}


-(BOOL) favouriteToggled
{
    SavedJourney *savedJourney = [SavedJourney new];
    
    savedJourney.fromStationName = self.appdelegate.journey.startStationName;
    savedJourney.toStationName = self.appdelegate.journey.endStationName;
    
    savedJourney.selectedViaText = [self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex];
    savedJourney.selectedViaIndex = self.appdelegate.journey.selectedViaIndex;
    
    savedJourney.fromStationId = self.appdelegate.journey.startStationId;
    savedJourney.toStationId = self.appdelegate.journey.endStationId;
    
    //TODO: Core Data (INSERT FAVOURITE)
    NSString *keyForSaving = [NSString stringWithFormat:@"Favourite_F%d_T%d_V%d",savedJourney.fromStationId,savedJourney.toStationId,savedJourney.selectedViaIndex];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([[[prefs dictionaryRepresentation] allKeys] containsObject:keyForSaving]){
        NSLog(@"Key already exists, returning");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not save" message:@"Favourite journey already exists." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//        return NO;
        
        [prefs removeObjectForKey:keyForSaving];
        [prefs synchronize];
        NSLog(@"Key deleted from Result screen");
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Removed from favourites" message:@"This route has been removed from your favourite journeys." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        self.extraView.favouriteIconImageView.image = [UIImage imageNamed:@"fev off"];
        self.extraView.favouriteTextLabel.text = @"Add to Favourites";
        self.extraView.isFavouriteAdd = YES;
        [self.extraView setNeedsLayout];
        
        return YES;
    }
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:savedJourney];
    [prefs setObject:myEncodedObject forKey:keyForSaving];
    [prefs synchronize];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved to favourites" message:@"This route has been added to your favourite journeys." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
//    NSLog(@"Saved!");
    
    self.extraView.favouriteIconImageView.image = [UIImage imageNamed:@"fev on"];
    self.extraView.favouriteTextLabel.text = @"Remove from Favourites";
    self.extraView.isFavouriteAdd = NO;
    [self.extraView setNeedsLayout];
    
    return YES;

}

-(void)shareJourneyTapped
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Share As:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Results Image",
                            @"SMS",
                            nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *lineValueDisplayed;
    switch (buttonIndex)
    {
        case 0:
            //Image Share
            [self shareAsImage];
            break;
        case 1:
            //Image Text
            [self shareAsText];
            break;
        default:
            break;
    }
}

-(void) shareAsText
{
    
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        NSString *msg = @"\n--sent via MRMApp";
        controller.body = [NSString stringWithFormat:@"%@%@ %@",self.routeSMSTitleStr,self.routeSMSStr,msg];//@"Directions from Station to Station. Text will appear here.";
        controller.recipients = nil;//[NSArray arrayWithObjects:@"", nil];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }

    
}

-(void) shareAsImage
{
    UIImage* fromToImage = nil;
    UIImage* viaImage = nil;
    UIView* timeImageView = self.timeSelectionView;
    
    
//    timeImageView.layer.borderWidth = 1.0f;
//    timeImageView.layer.borderColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5] CGColor];
    
    UIImage* timeImage = nil;
    UIImage* contentImage = nil;
//    UIImage* footerImage = nil;
    
    CGFloat height = 0;
    
    UIGraphicsBeginImageContext(self.fromToSwapView.frame.size);
    [self.fromToSwapView.layer renderInContext: UIGraphicsGetCurrentContext()];
    fromToImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    height+=self.fromToSwapView.frame.size.height;
    
    UIGraphicsBeginImageContext(self.viaSelectionView.frame.size);
    [self.viaSelectionView.layer renderInContext: UIGraphicsGetCurrentContext()];
    viaImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    height+=self.viaSelectionView.frame.size.height;
    
    UIGraphicsBeginImageContext(timeImageView.frame.size);
    [timeImageView.layer renderInContext: UIGraphicsGetCurrentContext()];
    
    timeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    height+=self.timeSelectionView.frame.size.height;
    
    
    CGRect contentFrame = CGRectMake(self.contentScrollView.frame.origin.x, self.contentScrollView.frame.origin.y-100, self.contentScrollView.contentSize.width, self.contentScrollView.contentSize.height);
    
    UIGraphicsBeginImageContext(contentFrame.size);
    {
        CGPoint savedContentOffset = self.contentScrollView.contentOffset;
//        savedContentOffset = CGPointMake(savedContentOffset.x, savedContentOffset.y);
        CGRect savedFrame = self.contentScrollView.frame;
//        savedFrame = CGRectMake(savedFrame.origin.x, savedFrame.origin.y-40, savedFrame.size.width, savedFrame.size.height);
        
        self.contentScrollView.contentOffset = CGPointZero;
        self.contentScrollView.frame = CGRectMake(0, 0, self.contentScrollView.contentSize.width, self.contentScrollView.contentSize.height);
        
        [self.contentScrollView.layer renderInContext: UIGraphicsGetCurrentContext()];
        contentImage = UIGraphicsGetImageFromCurrentImageContext();
        
        self.contentScrollView.contentOffset = savedContentOffset;
        self.contentScrollView.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    height+=contentFrame.size.height;
    
//    UIGraphicsBeginImageContext(self.timeSelectionView.frame.size);
//    [self.timeSelectionView.layer renderInContext: UIGraphicsGetCurrentContext()];
//    timeImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    height+=self.timeSelectionView.frame.size.height;
    
    
    CGSize newSize = CGSizeMake(self.view.frame.size.width, height-viaImage.size.height); //size of image view
    UIGraphicsBeginImageContext( newSize );
    
    // drawing 1st image
    [fromToImage drawInRect:CGRectMake(0,0,newSize.width,fromToImage.size.height)];
    [viaImage drawInRect:CGRectMake(0,fromToImage.size.height,newSize.width,viaImage.size.height)];
    [timeImage drawInRect:CGRectMake(0, fromToImage.size.height + viaImage.size.height , newSize.width ,timeImage.size.height)];
    [contentImage drawInRect:CGRectMake(0, fromToImage.size.height + viaImage.size.height, newSize.width, contentImage.size.height)];
    
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    
//    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
//    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
    
        UIImage     * iconImage = newImage;//[UIImage imageNamed:@"YOUR IMAGE"];
//        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Journey.jpg"];
        
        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
        
        _documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        //_documentInteractionController.UTI = @"net.whatsapp.image";
        _documentInteractionController.UTI = @"public.image";
        _documentInteractionController.delegate = self;
        
        [_documentInteractionController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
        
//        
//    } else {
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Message was cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MessageComposeResultSent:
            NSLog(@"Message was sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:             
            break;     
    } 
}

-(void)composeFeedbackEmail
{
    
    // Email Subject
    NSString *emailTitle = [NSString stringWithFormat:@"Feedback - MrmApp iOS v%@",[[[NSBundle mainBundle] infoDictionary]
                                                                               objectForKey:@"CFBundleShortVersionString"] ];
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:URLEMail];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        picker.mailComposeDelegate = self;
        [picker setSubject:emailTitle];
        [picker setMessageBody:nil isHTML:YES];
        [picker setToRecipients:toRecipents];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}



- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end






























