//
//  StationsList.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StationsList : NSObject

@property (strong, nonatomic) NSArray *stationArray;

- (NSArray *)getListOfStations:(NSString *)filteredWithLineName;
- (NSArray *)getListOfStations;

@end
