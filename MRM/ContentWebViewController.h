//
//  ContentWebViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 2/7/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentWebViewController : UIViewController <UIWebViewDelegate>

@property (strong,nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) NSString *htmlFileName;
@property (strong,nonatomic) NSString *title;

@end
