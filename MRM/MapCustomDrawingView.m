//
//  MapCustomDrawingView.m
//  MRMMapTest
//
//  Created by Shafeeq Rahiman on 6/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MapCustomDrawingView.h"
#import "UIImage+ImageWithAlpha.h"
#import "UIImage+animatedGIF.h"
#import "FMDatabase.h"
#import "MRMStationLink.h"
#import "MRMStationsLine.h"
#import "AppDelegate.h"
#import "RouteStation.h"
#import "SpriteData.h"

@interface MapCustomDrawingView ()

@property (strong,nonatomic) NSMutableArray *drawLinksArray;
@property (strong,nonatomic) AppDelegate *appdelegate;
@property bool boundsSet;
@end

@implementation MapCustomDrawingView

-(AppDelegate *)appdelegate{
    if(!_appdelegate){
        _appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appdelegate;
}


-(NSMutableArray *)drawLinksArray{
    if(!_drawLinksArray)
        _drawLinksArray = [[NSMutableArray alloc] init];
    return _drawLinksArray;

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.scrollZoomScale = 1;
    }
    [self commonInit];
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.scrollZoomScale = 1;
    }
    [self commonInit];
    return self;
}

-(void) toggleVisibility{
    self.hidden = !self.hidden;
}

-(void) commonInit {
    self.boundsSet = NO;
    
    [self findAllLinksFromJourneyInformation];
    [self setNeedsDisplay];
    
}

-(void)swapDrawingCall{
//    self.swapDrawing = !self.swapDrawing;
    [self setNeedsDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) findAllLinksFromJourneyInformation
{
    //allSegments is an NSArray MRMStationsLine
    //MRMStationsLine has an array of stations on a single line. and the line_id
//    return;
    [self.drawLinksArray removeAllObjects];
    
//    NSArray *firstLeg = [self.appdelegate.journey.partJournies firstObject];
//    NSArray *lastLeg = [self.appdelegate.journey.partJournies lastObject];
    
    for(NSArray *partJourney in self.appdelegate.journey.partJournies)
    {
        RouteStation *firstStation = [partJourney firstObject];
        RouteStation *lastStation = [partJourney lastObject];
        
        
        NSMutableArray *listOfStationIds = [[NSMutableArray alloc] init];
        int lineId = firstStation.line;
        
        for (RouteStation *station in partJourney)
        {
            if (station == lastStation) break;
            
            RouteStation *nextStation = [partJourney objectAtIndex:[partJourney indexOfObject:station]+1];
            
            [listOfStationIds addObject:station.station_id];
            
            NSArray *params = @[[NSNumber numberWithInt:firstStation.line],station.station_id,nextStation.station_id,nextStation.station_id,station.station_id];
            
            
            FMResultSet *rs = [self.appdelegate.db executeQuery:@"select _id,draw_script,stroke_color,stroke_width from line_draw where line_id = ? and ((from_stn_id = ? and to_stn_id= ?) or (from_stn_id = ? and to_stn_id= ?)) limit 1" withArgumentsInArray:params] ;
            
            MRMStationLink *stationLink = [[MRMStationLink alloc] init];
            
            while ([rs next]) {
                int linkId = [rs intForColumn:@"_id"];
                NSString *drawScript = [rs stringForColumn:@"draw_script"];
                float strokeWidth = [rs intForColumn:@"stroke_width"];
                NSString *strokeColor = [rs stringForColumn:@"stroke_color"];
                
                //            NSLog(stationName);
                stationLink.linkId = linkId;
                stationLink.drawScript = drawScript;
                stationLink.strokeColor = strokeColor;
                stationLink.strokeWidth = strokeWidth;
                stationLink.lineId = firstStation.line;
                [self.drawLinksArray addObject:stationLink];
            }
            
            [rs close];
            
            NSString *sprite_qry = [NSString stringWithFormat: @"SELECT STATION_ID, LINE_ID, SPRITE_TYPE, SPRITE_X, SPRITE_Y, MAP_X, MAP_Y, MAP_W, MAP_H, M_ACTIVE, M_IDLE FROM SPRITE_DATA T WHERE T.LINE_ID = ? AND T.STATION_ID = ? ORDER BY T.STATION_ID,T.LINE_ID"];
            
            rs = [self.appdelegate.db executeQuery:sprite_qry
                              withArgumentsInArray:@[@(lineId).stringValue, station.station_id]] ;
            
            while ([rs next]) {
                SpriteData *eachSpriteData = [[SpriteData alloc] init];
                eachSpriteData.spriteType = [rs stringForColumn:@"SPRITE_TYPE"];
                eachSpriteData.spriteX = [rs intForColumn:@"SPRITE_X"];
                eachSpriteData.spriteY = [rs intForColumn:@"SPRITE_Y"];
                eachSpriteData.mapX = [rs intForColumn:@"MAP_X"];
                eachSpriteData.mapY = [rs intForColumn:@"MAP_Y"];
                eachSpriteData.mapW = [rs intForColumn:@"MAP_W"];
                eachSpriteData.mapH = [rs intForColumn:@"MAP_H"];
                eachSpriteData.mActive = [rs intForColumn:@"M_ACTIVE"];
                eachSpriteData.mIdle = [rs intForColumn:@"M_IDLE"];
                
                eachSpriteData.isStop = [station.time isEqualToString:@""] ? NO:YES;
                
                [self.drawLinksArray addObject:eachSpriteData];
            }
            
            [rs close];
        }
        
        NSString *sprite_qry = [NSString stringWithFormat: @"SELECT STATION_ID, LINE_ID, SPRITE_TYPE, SPRITE_X, SPRITE_Y, MAP_X, MAP_Y, MAP_W, MAP_H, M_ACTIVE, M_IDLE FROM SPRITE_DATA T WHERE T.LINE_ID = ? AND T.STATION_ID = ? ORDER BY T.STATION_ID,T.LINE_ID"];
        
        FMResultSet *rs = [self.appdelegate.db executeQuery:sprite_qry withArgumentsInArray:@[@(lineId).stringValue, lastStation.station_id]] ;
        
        while ([rs next]) {
            SpriteData *eachSpriteData = [[SpriteData alloc] init];
            eachSpriteData.spriteType = [rs stringForColumn:@"SPRITE_TYPE"];
            eachSpriteData.spriteX = [rs intForColumn:@"SPRITE_X"];
            eachSpriteData.spriteY = [rs intForColumn:@"SPRITE_Y"];
            eachSpriteData.mapX = [rs intForColumn:@"MAP_X"];
            eachSpriteData.mapY = [rs intForColumn:@"MAP_Y"];
            eachSpriteData.mapW = [rs intForColumn:@"MAP_W"];
            eachSpriteData.mapH = [rs intForColumn:@"MAP_H"];
            eachSpriteData.mActive = [rs intForColumn:@"M_ACTIVE"];
            eachSpriteData.mIdle = [rs intForColumn:@"M_IDLE"];
            eachSpriteData.isStop = YES;
            
            [self.drawLinksArray addObject:eachSpriteData];
        }
        
        [rs close];
    }
}

// Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    
    
 /*   if(YES)
    {
//        //Kharbao - Bhiwandi
        UIColor* strokeColor = [UIColor colorWithRed: 0.701 green: 0.701 blue: 0.701 alpha: 1];
        UIBezierPath* path = [UIBezierPath bezierPath];
//        [path moveToPoint: CGPointMake(162,133)];
//        [path addCurveToPoint: CGPointMake(165,133) controlPoint1: CGPointMake(164,133) controlPoint2: CGPointMake(165,133)];
//        [path addCurveToPoint: CGPointMake(190,144) controlPoint1: CGPointMake(179,133) controlPoint2: CGPointMake(183,138)];
//        [path addLineToPoint: CGPointMake(199,153)];
//        [strokeColor setStroke];
        path.lineWidth = 2;
//        [path stroke];

        //Nerul - Seawoods
        //to Seawoods
        [path moveToPoint: CGPointMake(3869/16,5472/16)];
        [path addCurveToPoint: CGPointMake(3869/16,5496/16) controlPoint1: CGPointMake(3869/16,5485/16) controlPoint2: CGPointMake(3869/16,5494/16)];
//        [path moveToPoint: CGPointMake(3869/16,5496/16)];
        
        [path addCurveToPoint: CGPointMake(4047/16,5679/16)
                controlPoint1: CGPointMake(3869/16,5612/16)
                controlPoint2: CGPointMake(3948/16,5679/16)];
        
//        [path moveToPoint: CGPointMake(4047/16,5679/16)];
        [path addLineToPoint: CGPointMake(4085/16,5679/16)];
        
        [path fill];
//        [path stroke];

        return;
    }
    */
    if(!self.boundsSet)
    {
//        NSLog(@"%f,%f", self.superview.bounds.size.width, self.superview.bounds.size.height);
        CGRect frm = CGRectMake(0, 0, self.superview.bounds.size.width, self.superview.bounds.size.height );
        self.frame = frm;
        self.boundsSet = YES;

    }
    float scaleFactor = 4/self.scrollZoomScale;
    float fixedScaleFactor = 4;
    float markerScale = 0.5;
    
//    NSLog(@"drawRect scaleFactor = %f", scaleFactor);
    
    if(!self.drawLinksArray)
        return;
    
    
//  self.frame = CGRectOffset( self.frame, 0, 0 );
    
//    NSLog(@"Drawing Start time : %@",[[NSDate alloc] init]);
    
    UIColor* strokeColor;
    UIBezierPath* path;
    for (NSObject *obj in self.drawLinksArray){
        if([obj isKindOfClass:MRMStationLink.class])
        {
            MRMStationLink *eachStationLink = (MRMStationLink *) obj;
            
            strokeColor = [self.appdelegate getUIColorForLine:eachStationLink.lineId];
            [strokeColor setStroke];
            
            UIColor *fillColor = [self.appdelegate getUIColorForLine:eachStationLink.lineId];
            [fillColor setFill];
            
            
            path = [UIBezierPath bezierPath];
            
            path.lineWidth = eachStationLink.strokeWidth/scaleFactor;
            
            NSArray *components = [eachStationLink.drawScript componentsSeparatedByString:@"|"];
            float x1,y1,x2,y2,x3,y3;
            
            
            for (NSString *eachComponent in components)
            {
                if([eachComponent isEqualToString:@""])
                    break;
                char type = [eachComponent characterAtIndex:0];
                NSArray *coordinatesArray;
                switch(type){
                    case 'S':
                        coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                        x1 = ((NSString*)coordinatesArray[0]).floatValue/fixedScaleFactor;
                        y1 = ((NSString*)coordinatesArray[1]).floatValue/fixedScaleFactor;
                        [path moveToPoint:CGPointMake(x1,y1)];
                        break;
                    case 'L':
                        coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                        x1 = ((NSString*)coordinatesArray[0]).floatValue/fixedScaleFactor;
                        y1 = ((NSString*)coordinatesArray[1]).floatValue/fixedScaleFactor;
                        [path addLineToPoint:CGPointMake(x1,y1)];
                        break;
                    case 'C':
                        coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                        x1 = ((NSString*)coordinatesArray[0]).floatValue/fixedScaleFactor;
                        y1 = ((NSString*)coordinatesArray[1]).floatValue/fixedScaleFactor;
                        x2 = ((NSString*)coordinatesArray[2]).floatValue/fixedScaleFactor;
                        y2 = ((NSString*)coordinatesArray[3]).floatValue/fixedScaleFactor;
                        x3 = ((NSString*)coordinatesArray[4]).floatValue/fixedScaleFactor;
                        y3 = ((NSString*)coordinatesArray[5]).floatValue/fixedScaleFactor;
                        CGPoint p1 = CGPointMake(x1,y1);
                        CGPoint p2 = CGPointMake(x2,y2);
                        CGPoint p3 = CGPointMake(x3,y3);

                        [path addCurveToPoint:p1
                                controlPoint1:p2
                                controlPoint2:p3
                         ];
                        
                        break;
                    
                }
            }
            if(eachStationLink.strokeWidth == 4 ){
                [path fill];
            }
            [path stroke];
        }
    }
    
    //change scaleFactor = 2 for drawing station names and markers
    markerScale = 0.5;
    bool isfirstStation = YES;
    CGFloat minX = 0.0, minY = 0.0, maxX = 0.0, maxY = 0.0;
    
    
    for (NSObject *obj in self.drawLinksArray)
    {
        if([obj isKindOfClass:SpriteData.class])
        {
            SpriteData *eachSprite = (SpriteData *) obj;

            if ([eachSprite.spriteType isEqualToString:@"Marker"])
            {
                NSString *assetName = [NSString stringWithFormat:@"marker_%d", eachSprite.isStop?eachSprite.mActive:eachSprite.mIdle];
                UIImage *stationSprite = [UIImage imageNamed:assetName];
                CGPoint spriteCoordinates = CGPointMake(eachSprite.mapX*markerScale, eachSprite.mapY*markerScale);
                CGSize spriteSize = CGSizeMake(eachSprite.mapW*markerScale, eachSprite.mapH*markerScale);
                
                UIImage *eachStationMarker = [self extractImageAtPoint:spriteCoordinates
                                                                ofSize:spriteSize
                                                       FromSpriteSheet:stationSprite];
                
                CGRect mapFrame = CGRectMake(eachSprite.spriteX/fixedScaleFactor ,eachSprite.spriteY/fixedScaleFactor , (eachStationMarker.size.width/2)*self.scrollZoomScale, (eachStationMarker.size.height/2)*self.scrollZoomScale);
                [eachStationMarker drawInRect:mapFrame];
                
                if(isfirstStation)
                {
                    minX = mapFrame.origin.x;
                    minY = mapFrame.origin.y;
                    maxX = mapFrame.origin.x;
                    maxY = mapFrame.origin.y;
//                    NSLog(@"ORIGIN : (%f,%f) ",mapFrame.origin.x,mapFrame.origin.y);
                    isfirstStation = NO;
                }
                else
                {
//                    NSLog(@"comparing with (%f,%f) (%f,%f)", minX,minY,maxX,maxY);
                    
                    if(mapFrame.origin.x < minX){
//                        NSLog(@"found minX %f", mapFrame.origin.x);
                        minX = mapFrame.origin.x  ;
                    }else if(mapFrame.origin.x >= maxX){
//                        NSLog(@"found maxX %f", mapFrame.origin.x);
                        maxX = mapFrame.origin.x ;
                    }
                    
                    if(mapFrame.origin.y < minY ){
//                        NSLog(@"found minY %f", mapFrame.origin.y);
                        minY = mapFrame.origin.y  ;
                    } else if(mapFrame.origin.y >= maxY){
//                        NSLog(@"found maxY %f", mapFrame.origin.y);
                        maxY = mapFrame.origin.y ;
                    }
                    //
//                    NSLog(@"(%f,%f) to (%f, %f)",minX,minY,maxX,maxY);
                    //                        if(self.appdelegate.startStationMapPoint.y > mapFrame.origin.x){
                    //                        self.appdelegate.endStationMapPoint = mapFrame.origin;
                }
            }
            else
            {
                //Station Name Sprite
                markerScale = 0.5;
                if (eachSprite.isStop)
                {
                    NSString *assetName = [NSString stringWithFormat:@"txt_%d",eachSprite.mActive];
                    UIImage *namesprite = [UIImage imageNamed:assetName];
                    UIImage *eachStationNameImg;
                    CGPoint spriteCoordinates = CGPointMake(eachSprite.spriteX*markerScale, eachSprite.spriteY*markerScale);
                    CGSize spriteSize = CGSizeMake(eachSprite.mapW*markerScale, eachSprite.mapH*markerScale);
                    
                    eachStationNameImg = [self extractImageAtPoint:spriteCoordinates
                                                            ofSize:spriteSize
                                                   FromSpriteSheet:namesprite];
                    
                    CGRect mapFrame = CGRectMake(eachSprite.mapX/fixedScaleFactor ,eachSprite.mapY/fixedScaleFactor , (eachStationNameImg.size.width/2)*self.scrollZoomScale, (eachStationNameImg.size.height/2)*self.scrollZoomScale);
                    [eachStationNameImg drawInRect:mapFrame];
                    
                    
                    if(isfirstStation)
                    {
                        minX = mapFrame.origin.x;
                        minY = mapFrame.origin.y;
                        maxX = mapFrame.origin.x;
                        maxY = mapFrame.origin.y;
//                        NSLog(@"ORIGIN : (%f,%f) ",mapFrame.origin.x,mapFrame.origin.y);
                        isfirstStation = NO;
                    }
                    else
                    {
//                        NSLog(@"comparing with (%f,%f) (%f,%f)", minX,minY,maxX,maxY);

                        if(mapFrame.origin.x < minX){
//                            NSLog(@"found minX %f", mapFrame.origin.x);
                            minX = mapFrame.origin.x;
                        }else if(mapFrame.origin.x >= maxX){
//                            NSLog(@"found maxX %f", mapFrame.origin.x);
                            maxX = mapFrame.origin.x ;
                        }
                        
                        if(mapFrame.origin.y < minY ){
//                            NSLog(@"found minY %f", mapFrame.origin.y);
                            minY = mapFrame.origin.y  ;
                        } else if(mapFrame.origin.y >= maxY){
//                            NSLog(@"found maxY %f", mapFrame.origin.y);
                            maxY = mapFrame.origin.y ;
                        }
                        //
//                        NSLog(@"(%f,%f) to (%f, %f)",minX,minY,maxX,maxY);
                        //                        if(self.appdelegate.startStationMapPoint.y > mapFrame.origin.x){
                        //                        self.appdelegate.endStationMapPoint = mapFrame.origin;
                        
                    }
                    
                }
                
            }
            
            self.appdelegate.startStationMapPoint = CGPointMake(minX, minY);
            self.appdelegate.endStationMapPoint = CGPointMake(maxX, maxY);
            
        }
        
    }
//     NSLog(@"Drawing End time : %@",[[NSDate alloc] init]);
//    CGRect rectangle = CGRectMake(self.appdelegate.startStationMapPoint.x,
//                                  self.appdelegate.startStationMapPoint.y,
//                                  self.appdelegate.endStationMapPoint.x - self.appdelegate.startStationMapPoint.x,
//                                  self.appdelegate.endStationMapPoint.y - self.appdelegate.startStationMapPoint.y);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 0.0);   //this is the transparent color
//    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.5);
//    CGContextFillRect(context, rectangle);
//    CGContextStrokeRect(context, rectangle);
}


-(UIImage*)extractImageAtPoint:(CGPoint)pointExtractedImg ofSize:(CGSize)sizeExtractedImg FromSpriteSheet:(UIImage*)imgSpriteSheet
{
    UIImage *extractedImage;
    
    CGRect rectExtractedImage;
    
    rectExtractedImage=CGRectMake(pointExtractedImg.x,pointExtractedImg.y,sizeExtractedImg.width,sizeExtractedImg.height);
    
    CGImageRef imgRefSpriteSheet=imgSpriteSheet.CGImage;
    
    CGImageRef imgRefExtracted=CGImageCreateWithImageInRect(imgRefSpriteSheet,rectExtractedImage);
    
    extractedImage=[UIImage imageWithCGImage:imgRefExtracted];
    
    CGImageRelease(imgRefExtracted);
    
    return extractedImage;
    
    
}

@end
