//
//  LineRouteDetail.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LineRouteDetail : NSObject

@property int _id;
@property int lineId;
@property NSString *routeInfo;
@property int orderNo;
@property NSArray *routeInfoArray;

@end
