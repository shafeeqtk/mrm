//
//  RouteIntermediateActionsView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "RouteIntermediateActionsView.h"

@interface RouteIntermediateActionsView()
@property (strong, nonatomic) IBOutlet UIImageView *lineDotImageView;


@end

@implementation RouteIntermediateActionsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteIntermediateActionsView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{
                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteIntermediateActionsView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            [self commonInit];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteIntermediateActionsView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{
            self = [[[NSBundle mainBundle] loadNibNamed:@"RouteIntermediateActionsView"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
            }
            [self commonInit];
        }
    }
    return self;
}

- (void)commonInit
{
    self.lineDotImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"linedot.png"]];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,50);
    //any View setup action goes here
}



@end
