//
//  ViaSelectionTableView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViaSelectionTableViewCell: UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *viaImageView;
@property (strong, nonatomic) IBOutlet UILabel *viaRouteNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *viaNoOfInterchangesLabel;
@property (strong, nonatomic) IBOutlet UILabel *viaDurationLabel;
@end
