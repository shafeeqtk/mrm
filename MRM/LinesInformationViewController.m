//
//  LinesInformationViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 1/3/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import "LinesInformationViewController.h"

@interface LinesInformationViewController ()

@end

@implementation LinesInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(0, 0)];
    self.navigationController.navigationBar.topItem.title = @"Back";
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    self.navigationController.navigationBar.topItem.title = @"MRM";
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
