//
//  FavouritesAndRecentsTableDelegate.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/10/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "FavouritesAndRecentsTableDelegate.h"
#import "SavedSearchTableViewCell.h"
#import "CustomBottomAlignedTableViewHeaderCellTableViewCell.h"
#import "SavedJourney.h"
#import "AppDelegate.h"
#import "StatsDump.h"

@interface FavouritesAndRecentsTableDelegate()

@property (strong, nonatomic) NSUserDefaults *defaults;

@end

@implementation FavouritesAndRecentsTableDelegate


-(NSMutableArray *)recentList{
    if(!_recentList){
        _recentList = [[NSMutableArray alloc] init];
    }
    return _recentList;
}

-(NSMutableArray *)favList{
    if(!_favList){
        _favList = [[NSMutableArray alloc] init];
    }
    return _favList;
}


- (id) initWithTableView: (UITableView *) tableView {
    self = [super init];
    if (self) {
        //TODO: Core Data (SELECT)
        /*
        AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appdelegate managedObjectContext];
        
        NSManagedObject *newJourney;
//        newJourney = [NSEntityDescription insertNewObjectForEntityForName:@"StatsDump" inManagedObjectContext:context];
//        [newJourney setValue:  forKey:@"srcId"];
        
        
         NSEntityDescription *entityDesc =
         [NSEntityDescription entityForName:@"StatsDump"
                 inManagedObjectContext:context];
        
         NSFetchRequest *request = [[NSFetchRequest alloc] init];
         [request setEntity:entityDesc];
        
         NSPredicate *pred =
         [NSPredicate predicateWithFormat:@"(isFav = %b)",
                 true];
         [request setPredicate:pred];
//         NSManagedObject *matches = nil;
        
         NSError *error;
         NSArray *objects = [context executeFetchRequest:request
                                 error:&error];
        
        for ( NSManagedObject *matches in objects) {
            SavedJourney *eachObj = [[SavedJourney alloc] init];
            eachObj.fromStationId = [[[matches valueForKey:@"srcId"] description] intValue];
            [self.favList addObject:eachObj];
        }
        
        pred = [NSPredicate predicateWithFormat:@"(isFav = %b)",
         false];
        [request setPredicate:pred];
        objects = [context executeFetchRequest:request
                                                  error:&error];
        
        for ( NSManagedObject *matches in objects) {
            SavedJourney *eachObj = [[SavedJourney alloc] init];
            eachObj.fromStationId = [[[matches valueForKey:@"srcId"] description] intValue];
            [self.recentList addObject:eachObj];
        }
        
        */
        self.defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *defaultAsDic = [self.defaults dictionaryRepresentation];
        NSArray *keyArr = [defaultAsDic allKeys];
        for (NSString *key in keyArr)
        {
//            NSLog(@"key [%@] => Value [%@]",key,[defaultAsDic valueForKey:key]);

            //TODO: Core Data (SELECT)
           
            NSData *encodedObj = [defaultAsDic objectForKey:key];
            if (! ([[key componentsSeparatedByString:@"_"][0]
                    isEqualToString:@"Favourite"]
                   ||
                   [[key componentsSeparatedByString:@"_"][0]
                    isEqualToString:@"Recent"]))
                continue;
            
            SavedJourney *eachObj = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObj];
            
            
            if([eachObj isKindOfClass:SavedJourney.class])
            {
                if ([[key componentsSeparatedByString:@"_"][0] isEqualToString:@"Favourite"])
                {
                    [self.favList addObject:eachObj];
                    
                }
                else if ([[key componentsSeparatedByString:@"_"][0] isEqualToString:@"Recent"])
                {
                    [self.recentList addObject:eachObj];
                }
            }
        }
        
//        NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
        
        noOfFavourites = @(self.favList?self.favList.count:0).intValue;
        noOfRecents = @(self.favList?self.recentList.count:0).intValue;

        tableView.dataSource = self;
        tableView.delegate = self;
        [self recalculateIndexes];
    }
    return self;
}

int favHeaderRow, recentHeaderRow;
int noOfFavourites = 5;
int noOfRecents = 5;

-(void) recalculateIndexes{
    favHeaderRow = 0;
    recentHeaderRow = favHeaderRow + noOfFavourites+1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    cell.textLabel.text=[NSString stringWithFormat:@"%@",[array objectAtIndex:indexPath.section]];
    SavedSearchTableViewCell *cell;
    
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        CustomBottomAlignedTableViewHeaderCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        cell.backgroundColor = [UIColor clearColor];
        cell.headerLabel.text =  (indexPath.section == 0) ? self.favList.count>0?@"FAVOURITE JOURNEYS":@"No Favourites" : self.recentList.count>0?@"RECENT SEARCHES":@"No recent searches";
        cell.headerLabel.font = [UIFont systemFontOfSize:12];
        cell.headerLabel.textColor = [UIColor grayColor];
        
        
//        cell.textLabel.contentMode = UIViewContentModeLeft;
        
//        cell.detailTextLabel.text = @"Show All";
//        cell.detailTextLabel.font = [UIFont systemFontOfSize:8];
//        cell.detailTextLabel.textColor = [UIColor lightGrayColor];
        cell.contentView.clipsToBounds = YES;
        return cell;
    }
    else if(indexPath.section > recentHeaderRow){
        
        //RECENT CELLS
        cell = [tableView dequeueReusableCellWithIdentifier:@"recentCell"];
        cell.favouriteIconImage.image = [UIImage imageNamed:@"fev-off"];
        cell.leftViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.rightViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.mainBackgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.3f];
//        cell.layer.borderWidth = 1.0f;
//        cell.layer.borderColor = [[UIColor grayColor] CGColor];
        cell.layer.cornerRadius = 15.0f;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
//        NSLog(@"Section # %ld",indexPath.section);
        
        SavedJourney *jny = (SavedJourney *)[self.recentList objectAtIndex:(indexPath.section - noOfFavourites - 2 )];
        
        UniChar ucode = 0x2794;
        NSString *codeString = [NSString stringWithCharacters:&ucode length:1];
        
        cell.savedTripNameLabel.text = [NSString stringWithFormat:@"%@ %@ %@",jny.fromStationName, codeString, jny.toStationName];
        cell.savedTripDetailLabel.text = [NSString stringWithFormat:@"Via: %@",jny.selectedViaText];
        
    }
    else {
        
        //FAVOURITE CELL
        cell = [tableView dequeueReusableCellWithIdentifier:@"favouriteCell"];
        cell.layer.cornerRadius = 15.0f;
        cell.layer.masksToBounds = YES;
//        cell.layer.borderWidth = 0.5f;
//        cell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
//        NSLog(@"Section # %ld",indexPath.section);
        SavedJourney *jny = (SavedJourney *)[self.favList objectAtIndex:(indexPath.section - 1)];
        
        
        
        UniChar ucode = 0x2794;
        NSString *codeString = [NSString stringWithCharacters:&ucode length:1];
        
        cell.savedTripNameLabel.text = [NSString stringWithFormat:@"%@ %@ %@",jny.fromStationName, codeString, jny.toStationName];
        cell.savedTripDetailLabel.text = [NSString stringWithFormat:@"Via: %@",jny.selectedViaText];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        return 44;
    }
    if(indexPath.section > recentHeaderRow){
        return 74;
    }
    return 74;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(SavedSearchTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > recentHeaderRow){
        //        cell.backgroundColor = [UIColor clearColor];
        cell.favouriteIconImage.image = [UIImage imageNamed:@"MrmApp_ios_SpriteSheet_0084_home-fev-off"];
        cell.leftViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.rightViewBackgroundView.backgroundColor = [UIColor clearColor];
        cell.mainBackgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.3f];
        //        cell.layer.borderWidth = 1.0f;
        //        cell.layer.borderColor = [[UIColor grayColor] CGColor];
        cell.layer.cornerRadius = 15.0f;
        
        if(self.recentList.count>0)
        {
            SavedJourney *jny = (SavedJourney *)[self.recentList objectAtIndex:(indexPath.section - noOfFavourites - 2)];
            
            UniChar ucode = 0x2794;
            NSString *codeString = [NSString stringWithCharacters:&ucode length:1];
            
            cell.savedTripNameLabel.text = [NSString stringWithFormat:@"%@ %@ %@",jny.fromStationName, codeString, jny.toStationName];
            cell.savedTripDetailLabel.text = [NSString stringWithFormat:@"Via: %@",jny.selectedViaText];
        }
    }
    
}


// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
        return NO;
    }
    return YES;
}

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
//    if(indexPath.section > recentHeaderRow){
//        return nil;
//    }
//    return @"Edit";
}
-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath{
    return;
}

//TODO: Core Data (DELETE)
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
/*
        AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appdelegate managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"StatsDump" inManagedObjectContext:context]];
*/
        if ([[tableView cellForRowAtIndexPath:indexPath].reuseIdentifier isEqualToString:@"favouriteCell"]){
            SavedJourney *savedJourney = [self.favList objectAtIndex:(indexPath.section - 1)];
/*
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"srcId == %d AND destId == %d AND jxIdList == %@", savedJourney.fromStationId,savedJourney.toStationId,savedJourney.selectedViaText]];
            NSError* error = nil;
            NSArray* results = [context executeFetchRequest:fetchRequest error:&error];
            
            StatsDump *stat = results[0];
            stat.isFav = false;
            [context save:&error];
*/
            
            
            [self removePrefsWithKey:[NSString stringWithFormat:@"Favourite_F%d_T%d_V%d",savedJourney.fromStationId,savedJourney.toStationId,savedJourney.selectedViaIndex]];
             
            [self.favList removeObjectAtIndex:(indexPath.section - 1)];
            noOfFavourites--;
        } else if([[tableView cellForRowAtIndexPath:indexPath].reuseIdentifier isEqualToString:@"recentCell"]){
            SavedJourney *savedJourney = [self.recentList objectAtIndex:(indexPath.section - noOfFavourites - 2)];
            [self removePrefsWithKey:[NSString stringWithFormat:@"Recent_F%d_T%d_V%d",savedJourney.fromStationId,savedJourney.toStationId,savedJourney.selectedViaIndex]];
            
            [self.recentList removeObjectAtIndex:(indexPath.section - noOfFavourites - 2)];
            noOfRecents--;
        }
        [self recalculateIndexes];
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(editingStyle == UITableViewCellEditingStyleNone) {
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return noOfFavourites+noOfRecents+2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == favHeaderRow || indexPath.section == recentHeaderRow){
            //return
        return;
    }
    else if(indexPath.section > recentHeaderRow){
        //recent cell selected
        SavedJourney *jny = (SavedJourney *)[self.recentList objectAtIndex:(indexPath.section - noOfFavourites - 2)];
        [self.delegate showResultsPageWithSavedSearchObject:jny];
        return;
    }
    else {
        //favourite cell selected
        SavedJourney *jny = (SavedJourney *)[self.favList objectAtIndex:(indexPath.section - 1)];
        [self.delegate showResultsPageWithSavedSearchObject:jny];
        return;
    }

    
}

-(void) removePrefsWithKey:(NSString *) prefKey
{
    [self.defaults removeObjectForKey:prefKey];
    [self.defaults synchronize];
    NSLog(@"Key deleted");
}

             
             

@end
