//
//  SlideMenuViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "MenuItemTableViewCell.h"

@interface SlideMenuViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *triangleBottomBorderView;

@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIImage *img = [UIImage imageNamed:@"triangle 255.png"];
    self.triangleBottomBorderView.backgroundColor = [UIColor colorWithPatternImage:img];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuItemCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            cell.menuItemIcon.image = [UIImage imageNamed:@"MM home icon"];
            cell.menuItemLabel.text = @"MRM Home";
            break;
        case 1:
            cell.menuItemIcon.image = [UIImage imageNamed:@"MM map icon"];
            cell.menuItemLabel.text = @"Browse Map";
            break;
        case 2:
            cell.menuItemIcon.image = [UIImage imageNamed:@"MM railway icon"];
            cell.menuItemLabel.text = @"Mumbai Suburban Railways";
            break;
        case 3:
            cell.menuItemIcon.image = [UIImage imageNamed:@"MM ticketing icon"];
            cell.menuItemLabel.text = @"Rail Ticket System";
            break;
        case 4:
            cell.menuItemIcon.image = nil;
            cell.menuItemLabel.text = @"About MRM Project";
            break;
        case 5:
            cell.menuItemIcon.image = nil;
            cell.menuItemLabel.text = @"Feedback";
            break;
        case 6:
            cell.menuItemIcon.image = nil;
            cell.menuItemLabel.text = @"Terms Of Use";
            break;
        case 7:
            cell.menuItemIcon.image = nil;
            cell.menuArrowImageView.hidden = YES;
            cell.menuItemLabel.text = [NSString stringWithFormat:@"Version: MRM v%@ ",[[[NSBundle mainBundle] infoDictionary]
                                                                                     objectForKey:@"CFBundleShortVersionString"]];
//            cell.menuItemLabel.textAlignment = NSTextAlignmentRight;
            cell.menuItemLabel.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            break;
        default:
            break;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = [UIColor clearColor];//    ;[UIColor colorWithRed:255/255.0  green:255/255.0 blue:255/255.0 alpha:1.0];
    bgColorView.backgroundColor = [UIColor colorWithRed:255/255.0  green:255/255.0 blue:255/255.0 alpha:0.4];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate selectedMenuItemAtIndex:indexPath.row];
    return;
}

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.delegate selectedMenuItemAtIndex:indexPath.row];
}


#define URLMrmAppWebsite @"http://www.mrmapp.in"

- (IBAction)openMrmAppWebsiteTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLMrmAppWebsite]];
    
}

@end
