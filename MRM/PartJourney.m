//
//  PartJourney.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "PartJourney.h"

@implementation PartJourney

-(NSMutableArray *)stationList{
    if(!_stationList)
        _stationList = [[NSMutableArray alloc] init];
    return _stationList;
}

@end
