//
//  MRMRouteStations.m
//  MRM
//
//  Created by Shafeeq Rahiman on 7/12/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "RouteStations.h"

@implementation RouteStations


-(NSMutableArray *)stationArray{
    if(!_stationArray)
        _stationArray = [[NSMutableArray alloc] init];
    return _stationArray;
}

-(void) addIntermediateStation:(RouteStation *)station{
    [self.stationArray addObject:station];
}
@end
