//
//  ResultsMapViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/3/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapCustomDrawingView.h"

@interface ResultsMapViewController : UIViewController <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet MapCustomDrawingView *mapDrawView;

-(void) setupMap;
@end

