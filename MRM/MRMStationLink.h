//
//  MRMStationLink.h
//  MRM
//
//  Created by Shafeeq Rahiman on 6/26/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MRMStationLink : NSObject

@property int linkId;
@property (strong,nonatomic) NSString *strokeColor;
@property (nonatomic) float strokeWidth;
@property (strong,nonatomic) NSString *drawScript;
@property (nonatomic) int fromStationId;
@property (nonatomic) int toStationId;
@property (nonatomic) int lineId;

@end
