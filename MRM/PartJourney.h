//
//  PartJourney.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PartJourney : NSObject

@property int lineId;
//@property int startStationId;
//@property int endStationId;

@property (strong, nonatomic) NSMutableArray *stationList;

@end
