//
//  ResultsExtrasView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ResultsExtrasView.h"

#define URLMrmAppWebsite @"http://www.mrmapp.in"

@interface ResultsExtrasView()

@property (strong, nonatomic) IBOutlet UIImageView *topBorderImageView;
@property (strong, nonatomic) IBOutlet UIView *addFavView;
@property (strong, nonatomic) IBOutlet UIView *journeyView;
@property (strong, nonatomic) IBOutlet UIView *shareJourneyView;
@property (strong, nonatomic) IBOutlet UIImageView *bottomBorderImageView;
@property (strong, nonatomic) IBOutlet UIView *feedbackButtonView;

@end

@implementation ResultsExtrasView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

NSString *nibName = @"ResultsExtrasView";

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{
                self = [[[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            [self commonInit];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{
                self = [[[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            [self commonInit];
        }
    }
    return self;
}

- (void)commonInit
{
    self.topBorderImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangle 212.png"]];
    self.bottomBorderImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangle down 212.png"]];
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,50);
    //any View setup action goes here
    
    CGFloat cornerRadius = 15.0f;
    self.addFavView.layer.cornerRadius = cornerRadius;
    self.journeyView.layer.cornerRadius = cornerRadius;
    self.shareJourneyView.layer.cornerRadius = cornerRadius;
    self.feedbackButtonView.layer.cornerRadius = cornerRadius;
    
}

- (IBAction)favouriteButtonTapped:(UIButton *)sender
{
    BOOL savedSuccess = [self.delegate favouriteToggled];
//    if(savedSuccess)
//    {
//        self.favouriteIconImageView.image = [UIImage imageNamed:@"fev off"];
//        self.favouriteTextLabel.text = @"Remove from favourites";
//        self.isFavouriteAdd = NO;
//    }
}

- (IBAction)shareButtonTapped:(UIButton *)sender
{
    [self.delegate shareJourneyTapped];
    
}

- (IBAction)feedbackButtonTapped:(UIButton *)sender
{
//    NSString *url = [URLEMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    
    [self.delegate composeFeedbackEmail];

}

- (IBAction)openMrmAppWebsiteTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLMrmAppWebsite]];

}

@end
