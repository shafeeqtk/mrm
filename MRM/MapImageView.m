//
//  Draw2D.m
//  MRMMapTest
//
//  Created by Shafeeq Rahiman on 6/18/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MapImageView.h"
#import "UIImage+ImageWithAlpha.h"
#import "MRMStationLink.h"
#import "FMDatabase.h"


@interface MapImageView()
@property (strong,nonatomic) FMDatabase *db;
@property (strong,nonatomic) NSMutableArray *tempArray;
@end

@implementation MapImageView


-(NSMutableArray *)tempArray{
    if(!_tempArray)
        _tempArray = [[NSMutableArray alloc] init];
    return _tempArray;
    
}
-(void)toggleOpacity{
    if(self.alpha<1){
        self.alpha = 1.0;
    }
    else
    {
        self.alpha = 0.5;
    }
}

-(void) toggleVisibility{
    self.hidden = !self.hidden;
}

//-(id)initWithCoder:(NSCoder*)coder{
//    
//    self = [super initWithCoder:coder];
//    if (self) {
//        // Initialization code
//        [self setNeedsDisplay];
//
//    }
//    
//    return self;
//}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    UIImage *map = [UIImage imageNamed:@"mapall.png"];
//    UIImageView *imview = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,self.superview.frame.size.width, self.superview.frame.size.height)];

//    CGRect frm = CGRectMake(0, 0, 1680, 2380);
    
    NSLog(@"Image Frame size: %f , %f " ,self.frame.size.width, self.frame.size.height);
    
    CGRect frm = CGRectMake(0,0,self.frame.size.width, self.frame.size.height);
    
//    float offsetX = 90, offsetY = 90;
//    
//    CGRect frm = CGRectMake(offsetX, offsetY, self.frame.size.width-2*offsetX, self.frame.size.height - 2*offsetY);
    
    
    UIImageView *imview = [[UIImageView alloc] initWithFrame:frm];
    [imview setImage: map];
    imview.alpha = 1.0;
//    self.contentMode = UIViewContentModeScaleAspectFit;
    
//    CGAffineTransform s =  CGAffineTransformMakeScale(1,1);
//    CGAffineTransform t = CGAffineTransformMakeTranslation(0, 0);
//    CGAffineTransform ts = CGAffineTransformConcat(t,s);
//    self.transform = ts;
    
    self.clipsToBounds = NO;
    [self addSubview:imview];
    /*
    
    [self.tempArray removeAllObjects];

    if (!_db) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"mrm_v2" ofType:@"db3"];
        _db = [FMDatabase databaseWithPath:path];
    }
    if (![_db open]){
        [_db open];
    }
    
    FMResultSet *rs = [self.db executeQuery:@"select drawScript,strokeColor,lineWidth from maplinks"] ;
    
    while ([rs next]) {
        NSString *drawScript = [rs stringForColumn:@"drawScript"];
        float strokeWidth = [rs intForColumn:@"lineWidth"];
        NSString *strokeColor = [rs stringForColumn:@"strokeColor"];
        
        //            NSLog(stationName);
        MRMStationLink *stationLink = [[MRMStationLink alloc] init];
        stationLink.drawScript = drawScript;
        stationLink.strokeColor = strokeColor;
        stationLink.strokeWidth = strokeWidth;
        [self.tempArray addObject:stationLink];
    }
    
    [rs close];
    [_db close];
    
    /////////
    if(!self.tempArray)
        return;
    
    self.frame = CGRectOffset( self.frame, 0.5, 0 );
    
    UIColor* strokeColor;
    UIBezierPath* path;
    for (MRMStationLink *eachStationLink in self.tempArray){
        //         eachStationLink.drawScript
        if ([eachStationLink.strokeColor isEqualToString:@"0xFF65FF99"])
        {
            eachStationLink.strokeColor = @"0xFF3CB371";
            //            eachStationLink.strokeColor = @"0xFF000000";
        }else if([eachStationLink.strokeColor isEqualToString:@"0xFFFF19E5"])
        {
            eachStationLink.strokeColor = @"0xFFDF1B76";
        }else if([eachStationLink.strokeColor isEqualToString:@"0xFFFF7F00"])
        {
            eachStationLink.strokeColor = @"0xFFEA8013";
        }
        //        223/255,27/255,118/255
        
        
        unsigned int outVal;
        NSScanner* scanner = [NSScanner scannerWithString:eachStationLink.strokeColor];
        [scanner scanHexInt:&outVal];
        strokeColor = UIColorFromRGB(outVal);
        path = [UIBezierPath bezierPath];
        
        NSArray *components = [eachStationLink.drawScript componentsSeparatedByString:@"|"];
        int p1,p2,p3,p4,p5,p6;
        
        for (NSString *eachComponent in components){
            char type = [eachComponent characterAtIndex:0];
            NSArray *coordinatesArray;
            switch(type){
                case 'S':
                    coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                    p1 = ((NSString*)coordinatesArray[0]).intValue;
                    p2 = ((NSString*)coordinatesArray[1]).intValue;
                    [path moveToPoint: CGPointMake(p1,p2)];
                    break;
                case 'C':
                    coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                    p1 = ((NSString*)coordinatesArray[0]).intValue;
                    p2 = ((NSString*)coordinatesArray[1]).intValue;
                    p3 = ((NSString*)coordinatesArray[2]).intValue;
                    p4 = ((NSString*)coordinatesArray[3]).intValue;
                    p5 = ((NSString*)coordinatesArray[4]).intValue;
                    p6 = ((NSString*)coordinatesArray[5]).intValue;
                    [path addCurveToPoint: CGPointMake(p1,p2) controlPoint1: CGPointMake(p3,p4) controlPoint2: CGPointMake(p5,p6)];
                    break;
                case 'L':
                    coordinatesArray = [[[eachComponent componentsSeparatedByString:@"("][1] componentsSeparatedByString:@")"][0] componentsSeparatedByString:@","];
                    p1 = ((NSString*)coordinatesArray[0]).intValue;
                    p2 = ((NSString*)coordinatesArray[1]).intValue;
                    [path addLineToPoint: CGPointMake(p1,p2)];
                    break;
            }
        }
        [strokeColor setStroke];
        path.lineWidth = eachStationLink.strokeWidth;
        [path stroke];
    }

    /////////
    */
    
}


//https://developer.apple.com/library/mac/documentation/graphicsimaging/reference/CoreImageFilterReference/Reference/reference.html#//apple_ref/doc/filter/ci/CIBoxBlur


/*
- (void)drawRect:(CGRect)rect {

//    
////  draw image with alpha and overlay with image part
    UIImage *myImage = [UIImage imageNamed:@"map.png"];
//    myImage = [myImage imageByApplyingAlpha:0.5];
//    CGRect imageRect = [[UIScreen mainScreen] bounds];
    CGRect imageRect = CGRectMake(0, 0, myImage.size.width, myImage.size.height);
    [myImage drawInRect:imageRect];
////
//
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGMutablePathRef path = CGPathCreateMutable();
//    //or for e.g. CGPathAddRect(path, NULL, CGRectInset([self bounds], 10, 20));
//    CGRect rectangle = CGRectMake(70,90,110,230);
//    CGPathRef roundedRectPath = [Draw2D newPathForRoundedRect:rectangle radius:20];
//    CGContextAddPath(context, roundedRectPath);
//    
//    CGContextSaveGState(context);
//        
//    CGContextClip(context);
//    CGPathRelease(path);
//    [[UIImage imageNamed:@"map.png"] drawInRect:imageRect];
    
////    [@"Vangaon" drawInRect: CGRectMake(87, 90, 200, 24) withFont:[UIFont systemFontOfSize:16]];
    
//  draw image with sepia filter
//    UIImage *myimage = [UIImage imageNamed:@"map.png"];
//    CIImage *cimage = [[CIImage alloc] initWithImage:myimage];
//    
//    CIFilter *sepiaFilter = [CIFilter filterWithName:@"CISepiaTone"];
//    [sepiaFilter setDefaults];
//    [sepiaFilter setValue:cimage forKey:@"inputImage"];
//    [sepiaFilter setValue:[NSNumber numberWithFloat:0.8f]
//                   forKey:@"inputIntensity"];
//    CIImage *image = [sepiaFilter outputImage];
//    CIContext *context = [CIContext contextWithOptions: nil];
//    CGImageRef cgImage = [context createCGImage:image fromRect: image.extent];
//    UIImage *resultUIImage = [UIImage imageWithCGImage: cgImage];
//    CGRect imageRect =[[UIScreen mainScreen] bounds]; [resultUIImage drawInRect:imageRect];
    
    
//
////  draw in image with scaling (fit to window)
//    UIImage *myImage = [UIImage imageNamed:@"map.png"];
//    myImage = [myImage imageByApplyingAlpha:0.5];
//    CGRect imageRect =[[UIScreen mainScreen] bounds];
//    [myImage drawInRect:imageRect];
    
////  draw in image without scaling
//    UIImage *myImage = [UIImage imageNamed:@"map.png"];
//    CGPoint imagePoint = CGPointMake(0, 0);
//    [myImage drawAtPoint:imagePoint];
    
    
////  radial gradient sphere like
//    CGContextRef context = UIGraphicsGetCurrentContext(); CGGradientRef gradient;
//    CGColorSpaceRef colorspace;
//    CGFloat locations[2] = { 0.0, 1.0};
//    NSArray *colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blueColor].CGColor];
//    colorspace = CGColorSpaceCreateDeviceRGB();
//    gradient = CGGradientCreateWithColors(colorspace, (CFArrayRef)colors, locations);
//    CGPoint startPoint, endPoint;
//    CGFloat startRadius, endRadius;
//    startPoint.x = 180;
//    startPoint.y = 180;
//    endPoint.x = 200;
//    endPoint.y = 200;
//    startRadius = 0;
//    endRadius = 75;
//    CGContextDrawRadialGradient (context, gradient, startPoint, startRadius, endPoint, endRadius,
//                                 0);
//// radial gradient
//    CGContextRef context = UIGraphicsGetCurrentContext(); CGGradientRef gradient;
//    CGColorSpaceRef colorspace;
//    CGFloat locations[3] = { 0.0, 0.5, 1.0 };
//    NSArray *colors = @[(id)[UIColor redColor].CGColor, (id)[UIColor greenColor].CGColor, (id)[UIColor cyanColor].CGColor];
//    colorspace = CGColorSpaceCreateDeviceRGB();
//    gradient = CGGradientCreateWithColors(colorspace, (CFArrayRef)colors, locations);
//    CGPoint startPoint, endPoint;
//    CGFloat startRadius, endRadius;
//    startPoint.x = 100;
//    startPoint.y = 100;
//    endPoint.x = 200;
//    endPoint.y = 200;
//    startRadius = 10;
//    endRadius = 75;
//    CGContextDrawRadialGradient(context, gradient, startPoint, startRadius, endPoint, endRadius, 0);

//// linear gradient
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGGradientRef gradient;
//    CGColorSpaceRef colorspace;
//    CGFloat locations[4] = { 0.0, 0.25, 0.5, 0.75 };
//    NSArray *colors = @[(id)[UIColor redColor].CGColor, (id)[UIColor greenColor].CGColor,
//                        (id)[UIColor blueColor].CGColor, (id)[UIColor yellowColor].CGColor];
//    colorspace = CGColorSpaceCreateDeviceRGB();
//    gradient = CGGradientCreateWithColors(colorspace, (CFArrayRef)colors, locations);
//    CGPoint startPoint, endPoint;
//    startPoint.x = 0.0;
//    startPoint.y = 0.0;
//    endPoint.x = 500;
//    endPoint.y = 500;
//    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);

//// ellipse with shadow
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGSize myShadowOffset = CGSizeMake (-10, 15);
//    CGContextSaveGState(context);
//    CGContextSetShadow (context, myShadowOffset, 5);
//    CGContextSetLineWidth(context, 4.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor blueColor].CGColor);
//    CGRect rectangle = CGRectMake(60,170,200,80);
//    CGContextAddEllipseInRect(context, rectangle);
//    CGContextStrokePath(context);
//    CGContextRestoreGState(context);
    
//
////dashed line
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 20.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor blueColor].CGColor);
//    CGFloat dashArray[] = {2,6,4,2};
//    CGContextSetLineDash(context, 3, dashArray, 4);
//    CGContextMoveToPoint(context, 10, 200);
//    CGContextAddQuadCurveToPoint(context, 150, 10, 300, 200);
//    CGContextStrokePath(context);

////  arc
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 4.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor blueColor].CGColor);
//    CGContextMoveToPoint(context, 100, 100);
//    CGContextAddArcToPoint(context, 100,200, 300,200, 100);
//    CGContextStrokePath(context);

//
//
//// quadratic curve
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 4.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor blueColor].CGColor);
//    CGContextMoveToPoint(context, 10, 200);
//    CGContextAddQuadCurveToPoint(context, 150, 10, 300, 200);
//    CGContextStrokePath(context);
//    
////  cubic curve
////    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 4.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor blueColor].CGColor);
//    CGContextMoveToPoint(context, 10, 10);
//    CGContextAddCurveToPoint(context, 0, 50, 300, 250, 300, 400);
//    CGContextStrokePath(context);
//    
////      ellipse
    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetLineWidth(context, 4.0);
//    CGContextSetStrokeColorWithColor(context,[UIColor colorWithRed:0.93 green:0.22 blue:0.541 alpha:1.0].CGColor);
//    CGRect rectangle = CGRectMake(157,52,11,11);
////    CGPathAddRoundedRect(NULL, NULL, rectangle, 0.5, 1); cant get this working
//    CGContextAddEllipseInRect(context, rectangle);
//    CGContextStrokePath(context);
    
    CGRect rectangle = CGRectMake(153,48,19,14);
    CGContextSetLineWidth(context, 7.0);
	CGPathRef roundedRectPath = [self newPathForRoundedRect:rectangle radius:7];
	[[UIColor colorWithRed:0.93 green:0.22 blue:0.541 alpha:1.0] set];
	CGContextAddPath(context, roundedRectPath);
    //	CGContextFillPath(ctx);
    CGContextStrokePath(context);
    CGPathRelease(roundedRectPath);
    
//  line
//    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 10.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = {0.93, 0.22, 0.541, 1.0};
    
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    CGContextMoveToPoint(context, 163, 63);
    CGContextAddLineToPoint(context, 163, 93);
    
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
//end line
    

//new ellipse
    rectangle = CGRectMake(153,93,19,14);
    CGContextSetLineWidth(context, 7.0);
	roundedRectPath = [self newPathForRoundedRect:rectangle radius:7];
	[[UIColor colorWithRed:0.93 green:0.22 blue:0.541 alpha:1.0] set];
	CGContextAddPath(context, roundedRectPath);
    //	CGContextFillPath(ctx);
    CGContextStrokePath(context);
    CGPathRelease(roundedRectPath);
    
//end new ellipse
    CGContextSetLineWidth(context, 10.0);
    
     colorspace = CGColorSpaceCreateDeviceRGB();
    
//    CGFloat components[] = {0.93, 0.22, 0.541, 1.0};
    
    color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    CGContextMoveToPoint(context, 163, 105);
    CGContextAddLineToPoint(context, 163, 120);
    
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
////playground
//// two rounded rectangles
//    CGContextRef ctx = UIGraphicsGetCurrentContext();
//    
//	CGRect frame = self.bounds;
//    
//    CGRect rectangle = CGRectMake(20,170,100,40);
//	CGPathRef roundedRectPath = [self newPathForRoundedRect:rectangle radius:20];
//    
//	[[UIColor blueColor] set];
//	CGContextAddPath(ctx, roundedRectPath);
//    CGContextStrokePath(ctx);
//    
//    
//    rectangle = CGRectMake(200,170,100,40);
//	roundedRectPath = [self newPathForRoundedRect:rectangle radius:20];
//	[[UIColor blueColor] set];
//	CGContextAddPath(ctx, roundedRectPath);
//    //	CGContextFillPath(ctx);
//    CGContextStrokePath(ctx);
//	CGPathRelease(roundedRectPath);
}


- (CGPathRef) newPathForRoundedRect:(CGRect)rect radius:(CGFloat)radius
{
	CGMutablePathRef retPath = CGPathCreateMutable();
    
	CGRect innerRect = CGRectInset(rect, radius, radius);
    
	CGFloat inside_right = innerRect.origin.x + innerRect.size.width;
	CGFloat outside_right = rect.origin.x + rect.size.width;
	CGFloat inside_bottom = innerRect.origin.y + innerRect.size.height;
	CGFloat outside_bottom = rect.origin.y + rect.size.height;
    
	CGFloat inside_top = innerRect.origin.y;
	CGFloat outside_top = rect.origin.y;
	CGFloat outside_left = rect.origin.x;
    
	CGPathMoveToPoint(retPath, NULL, innerRect.origin.x, outside_top);
    
	CGPathAddLineToPoint(retPath, NULL, inside_right, outside_top);
	CGPathAddArcToPoint(retPath, NULL, outside_right, outside_top, outside_right, inside_top, radius);
	CGPathAddLineToPoint(retPath, NULL, outside_right, inside_bottom);
	CGPathAddArcToPoint(retPath, NULL,  outside_right, outside_bottom, inside_right, outside_bottom, radius);
    
	CGPathAddLineToPoint(retPath, NULL, innerRect.origin.x, outside_bottom);
	CGPathAddArcToPoint(retPath, NULL,  outside_left, outside_bottom, outside_left, inside_bottom, radius);
	CGPathAddLineToPoint(retPath, NULL, outside_left, inside_top);
	CGPathAddArcToPoint(retPath, NULL,  outside_left, outside_top, innerRect.origin.x, outside_top, radius);
    
	CGPathCloseSubpath(retPath);
    
	return retPath;
}
 */

// This method creates an image by changing individual pixels of an image. Color of pixel has been taken from an array of colours('avgRGBsOfPixel')
//-(UIImage *)createTexture{
//    
//    UIImage *sampleImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"map.png"]];
//    CGRect imageRect = CGRectMake(0, 0, sampleImage.size.width, sampleImage.size.height);
//    
//    UIGraphicsBeginImageContext(sampleImage.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    //Save current status of graphics context
//    CGContextSaveGState(context);
//    CGContextDrawImage(context, imageRect, sampleImage.CGImage);
//    //    And then just draw a point on it wherever you want like this:
//    
//    //    CGContextFillRect(context, CGRectMake(x,y,1,1));
//    //Fix error according to @gsempe's comment
//    int pixelCount =0;
//    for(int i = 0 ; i<sampleImage.size.width ; i++){
//        for(int j = 0 ; j<sampleImage.size.height ; j++){
//            pixelCount++;
//            int index = pixelCount/pixelCountForCalculatingAvgColor;
//            //            NSLog(@"Index : %d", index);
//            if(index >= [avgRGBsOfPixel count]){
//                index = [avgRGBsOfPixel count] -1;
//                NSLog(@"Bad Index");
//            }
//            //            if(pixelCount >9999){
//            //                pixelCount = 9999;
//            //                NSLog(@"Bad Index");
//            //            }
//            UIColor *color = [avgRGBsOfPixel objectAtIndex:index];
//            float red ,green, blue ,alpha ;
//            [color getRed:&red green:&green blue:&blue alpha:&alpha];
//            CGContextSetRGBFillColor(context, red , green, blue , 1);
//            CGContextFillRect(context, CGRectMake(i,j,1,1));
//        }
//    }
//    
//    //    Then just save it to UIImage again:
//    
//    CGContextRestoreGState(context);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    return sampleImage;
//}


@end
