//
//  ResultsMapViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/3/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "ResultsMapViewController.h"
#import "AppDelegate.h"
#import "Station.h"
#import "MRMStationsLine.h"
#import "FMDatabase.h"
#import "UIImageView+GeometryConversion.h"

@interface ResultsMapViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *mapImageView;
@property (strong, nonatomic) IBOutlet UIView *combinedMapView;
@property (strong, nonatomic) AppDelegate *appdelegate;
@property (strong, nonatomic) IBOutlet UIImageView *testImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImage *mapImage;
@property (strong, nonatomic) UIView *fillerView;

@property (nonatomic,retain) UIView *activityView;


@end

@implementation ResultsMapViewController

-(AppDelegate *)appdelegate
{
    if(!_appdelegate)
        _appdelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appdelegate;
}

CGRect newFrame;

-(void) viewDidLoad
{
    [super viewDidLoad];
    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];

}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self setupMap];
}

CGFloat ratio;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    }

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil ];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    
//    CGRect frame = CGRectMake(self.appdelegate.startStationMapPoint.x,
//                              self.fillerView.frame.size.height + self.appdelegate.startStationMapPoint.y,
//                              self.appdelegate.endStationMapPoint.x,
//                              self.appdelegate.endStationMapPoint.y);
//    
//    [self zoomToRectInImage:frame];

//    NSLog(@"Center of RECT = %f,%f", CGRectGetMidX(frame), CGRectGetMidY(frame));
    
    
}

//- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//    UIView *vw = [[UIView alloc] initWithFrame:self.testImageView.frame];
//    [vw addSubview:self.mapDrawView];
//    return vw;
//}
/*
- (void)scrollViewDidZoom:(UIScrollView *)aScrollView
{
    return;
    for (UIView *drawView in self.mapImageView.subviews) {
        if([drawView isKindOfClass:MapCustomDrawingView.class]){
            CGRect oldFrame = drawView.frame;
            // 0.5 means the anchor is centered on the x axis. 1 means the anchor is at the bottom of the view. If you comment out this line, the pin's center will stay where it is regardless of how much you zoom. I have it so that the bottom of the pin stays fixed. This should help user RomeoF.
//            [self.mapDrawView.layer setAnchorPoint:CGPointMake(0.5, 1)];
            self.mapDrawView.frame = oldFrame;
            // When you zoom in on scrollView, it gets a larger zoom scale value.
            // You transform the pin by scaling it by the inverse of this value.
            self.mapDrawView.transform = CGAffineTransformMakeScale(1.0/self.scrollView.zoomScale, 1.0/self.scrollView.zoomScale);
        }
    }
}
*/

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = self.mapImageView;
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
    
    self.mapDrawView.scrollZoomScale = scrollView.zoomScale;
    
//    NSLog(@"Zooming now - zoomScale = %f", scrollView.zoomScale);
    [self.mapImageView setNeedsDisplay];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.mapImageView;
}

-(IBAction)backTapped:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Activity Indicator methods

-(void)showActivityView
{
    UIWindow *window = self.appdelegate.window;
    self.activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [self.activityView addSubview:activityWheel];
    [window addSubview: self.activityView];
    
    [[[self.activityView subviews] objectAtIndex:0] startAnimating];
}

-(void)hideActivityView
{
    [[[self.activityView subviews] objectAtIndex:0] stopAnimating];
    [self.activityView removeFromSuperview];
    self.activityView = nil;
}


-(void)zoomToRectInImage:(CGRect)rect
{
    /*
    CGRect r        = [self.mapImageView convertRectFromView:rect];
    CGRect scrollViewRect = [self.mapImageView convertRect:r fromView:self.mapImageView];
    
    [self.scrollView zoomToRect:scrollViewRect animated:YES];
    
    */
    
    
    CGRect frame = [self.mapImageView convertRectFromView:rect];
    frame = [self.mapImageView convertRect:frame fromView:self.mapImageView];
    
    CGFloat centerX = CGRectGetMidX(frame);
    CGFloat centerY = CGRectGetMidY(frame);
    CGFloat curZoom = self.scrollView.zoomScale;
    [self.scrollView setContentOffset:CGPointMake((centerX*curZoom),
                                                  (centerY*curZoom) - ([self.navigationController navigationBar].frame.size.height+[UIApplication sharedApplication].statusBarFrame.size.height))
                             animated:NO];
    

}

- (void)zoomToPoint:(CGPoint)zoomPoint withScale: (CGFloat)scale animated: (BOOL)animated
{
    //Normalize current content size back to content scale of 1.0f
    CGSize contentSize;
    contentSize.width = (self.scrollView.contentSize.width / self.scrollView.zoomScale);
    contentSize.height = (self.scrollView.contentSize.height / self.scrollView.zoomScale);
    
    //translate the zoom point to relative to the content rect
    zoomPoint.x = (zoomPoint.x / self.scrollView.bounds.size.width) * contentSize.width;
    zoomPoint.y = (zoomPoint.y / self.scrollView.bounds.size.height) * contentSize.height;
    
    //derive the size of the region to zoom to
    CGSize zoomSize;
    zoomSize.width = self.scrollView.bounds.size.width / scale;
    zoomSize.height = self.scrollView.bounds.size.height / scale;
    
    //offset the zoom rect so the actual zoom point is in the middle of the rectangle
    CGRect zoomRect;
    zoomRect.origin.x = zoomPoint.x - zoomSize.width / 2.0f;
    zoomRect.origin.y = zoomPoint.y - zoomSize.height / 2.0f;
    zoomRect.size.width = zoomSize.width;
    zoomRect.size.height = zoomSize.height;
    
    //apply the resize
    [self.scrollView zoomToRect: zoomRect animated: animated];
}

-(void)reDrawMapDrawing
{
    self.mapDrawView.scrollZoomScale = ratio*self.scrollView.zoomScale;
    [self.mapImageView setNeedsDisplay];


}

-(void) setupMap
{
    CGRect viewRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, [self.navigationController navigationBar].frame.size.height+[UIApplication sharedApplication].statusBarFrame.size.height );
    
    //to cover our navigation bar, add a filler view.
    self.fillerView = [[UIView alloc] initWithFrame:viewRect];
    
    [self.view addSubview:self.fillerView];
    
    viewRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+self.fillerView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.fillerView.frame.size.height);
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:viewRect];
    
    self.scrollView.delegate = self;
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    self.scrollView.clipsToBounds = YES; // default is NO, we want to restrict drawing within our scrollview
    
    self.scrollView.bounces = NO;
    self.scrollView.bouncesZoom = NO;
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 4;
    
    
    NSString *imageFile = [NSString stringWithFormat:@"%@/mapall.png", [[NSBundle mainBundle] resourcePath]];
    
    self.mapImage = [UIImage imageWithContentsOfFile:imageFile];
    
    self.mapImageView = [[UIImageView alloc] initWithImage:self.mapImage];
    self.mapImageView.userInteractionEnabled = YES;
    
    UIView *blankView = [[UIView alloc] initWithFrame:self.mapImageView.frame];
    blankView.backgroundColor = [UIColor whiteColor];
    blankView.alpha = 0.7;
    [self.mapImageView addSubview:blankView];
    
    self.mapDrawView = [[MapCustomDrawingView alloc] initWithFrame:self.mapImageView.frame];
    self.mapDrawView.backgroundColor = [UIColor clearColor];
    [self.mapImageView addSubview:self.mapDrawView];
    
    viewRect = CGRectMake(0,self.fillerView.frame.size.height,self.mapImageView.image.size.width,self.mapImageView.image.size.height);
    
    [self.scrollView addSubview:self.mapImageView];
    
    ratio = CGRectGetWidth(self.scrollView.bounds) / self.mapImage.size.width;
    self.mapImageView.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), self.mapImage.size.height * ratio);
    self.mapImageView.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    self.mapImageView.image = self.mapImage;
    
    
    [self.scrollView setContentSize:CGSizeMake(viewRect.size.width, viewRect.size.height)];
    
    [self.view addSubview:self.scrollView];
    
    NSLog(@"Ratio = %f", ratio);
    NSLog(@"ZOOM SCALE: %f",self.scrollView.zoomScale);
    self.mapDrawView.scrollZoomScale = self.scrollView.zoomScale;
    
    //    [self initZoom];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(reDrawMapDrawing)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    [self hideActivityView];
}
@end

