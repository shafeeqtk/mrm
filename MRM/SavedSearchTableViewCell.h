//
//  SavedSearchView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/1/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SavedSearchTableViewCellDelegate;

@interface SavedSearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *favouriteIconImage;
@property (strong, nonatomic) IBOutlet UILabel *savedTripNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *savedTripDetailLabel;
@property (strong, nonatomic) IBOutlet UIView *leftViewBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *rightViewBackgroundView;
@property (strong, nonatomic) IBOutlet UIView *mainBackgroundView;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *recentLeftStarViewWidth;

@property (nonatomic, retain) id<SavedSearchTableViewCellDelegate> delegate;
@end

@protocol SavedSearchTableViewCell <NSObject>

@optional
//any methods

@end
