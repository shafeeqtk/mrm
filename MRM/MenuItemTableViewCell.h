//
//  MenuItemTableViewCell.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *menuItemIcon;
@property (strong, nonatomic) IBOutlet UILabel *menuItemLabel;
@property (strong, nonatomic) IBOutlet UIImageView *menuArrowImageView;
@end
