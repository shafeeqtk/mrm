//
//  MRMRouteStations.h
//  MRM
//
//  Created by Shafeeq Rahiman on 7/12/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RouteStation.h"

@interface RouteStations : NSObject

@property (strong, nonatomic) NSMutableArray *stationArray;

-(void) addIntermediateStation:(RouteStation *)station;
@end
