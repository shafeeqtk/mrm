//
//  ExpandingTableViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 1/2/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import "ExpandingTableViewController.h"

@interface ExpandingTableViewController ()

@end

@implementation ExpandingTableViewController

@synthesize selectedIndexPath;

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    
    UITableViewCell *cell;
    
    NSIndexPath* indexPathSelected = self.selectedIndexPath;
    
    if ( nil == indexPathSelected || [indexPathSelected compare: indexPath] != NSOrderedSame )
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] ;
        }
        
        cell.textLabel.text = [NSString stringWithFormat: @"cell %ld", (long)indexPath.row];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier2];
        }
        
        cell.textLabel.text = [NSString stringWithFormat: @"cell %ld", (long)indexPath.row];
        cell.detailTextLabel.text = [NSString stringWithFormat: @"%ld (expanded!)", (long)indexPath.row];
    }
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( self.selectedIndexPath != nil && [self.selectedIndexPath compare: indexPath] == NSOrderedSame )
    {
//        NSLog(@"%f",tableView.rowHeight * 2);
        return 88;//tableView.rowHeight * 2;
    }
//    NSLog(@"%f",tableView.rowHeight);
    return 44;//tableView.rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* toReload = [NSArray arrayWithObjects: indexPath, self.selectedIndexPath, nil];
    self.selectedIndexPath = indexPath;
    [tableView beginUpdates];
    [tableView endUpdates];
    //[tableView reloadRowsAtIndexPaths: toReload withRowAnimation: UITableViewRowAnimationMiddle];
}


@end
