//
//  RoutePlannerViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/30/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "RoutePlannerViewController.h"
#import "StationSelectionViewController.h"
#import "FullJourney.h"
#import "AppDelegate.h"
#import "FavouritesAndRecentsTableDelegate.h"
#import "ResultsListViewController.h"
#import "RouteStation.h"
#import "SavedJourney.h"
#import "SlideMenuViewController.h"
#import "BrowseMapViewController.h"
#import "ContentWebViewController.h"
#import "PageContentViewController.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface RoutePlannerViewController () <StationSelectionDelegate>

@property (strong, nonatomic) IBOutlet UIView *viaRoutesView;
@property (strong, nonatomic) IBOutlet UITableView *viaTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viaViewHeightConstraint;
@property (strong, nonatomic) AppDelegate *appdelegate;
@property (strong, nonatomic) FullJourney *journey;
@property (strong, nonatomic) IBOutlet UITableView *favRecentTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *favTableHeight;
@property (strong, nonatomic) IBOutlet UIView *triangleBorderView;
@property (strong, nonatomic) IBOutlet UIView *triangleBottomBorderView;


@property (strong, nonatomic) FavouritesAndRecentsTableDelegate *favDelegate;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchJourneyLeftMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchJourneyRightMarginConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchJourneyTopMarginConstraint;

@property (strong, nonatomic) IBOutlet UIView *viaBgView;
@property (nonatomic,retain) UIView *activityView;
@property (strong, nonatomic) IBOutlet UIButton *viaDropDownButton;
@property (strong, nonatomic) IBOutlet UILabel *viaRoutesHeaderLabel;

@property (strong, nonatomic) SavedJourney *savedJourneyChosen;
@property (strong, nonatomic) SlideMenuViewController *slideMenuVC;
@property BOOL isMenuOpen;
@property (strong, nonatomic) IBOutlet UIView *viaMaskView;
@property (strong, nonatomic) NSString *contentHtmlFileName;
@property (strong, nonatomic) NSString *contentTitle;

@property CGFloat translateValue;

@end

@implementation RoutePlannerViewController


enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;

#pragma mark - Find Phone Model


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}

#pragma mark - Initializer methods

-(AppDelegate *)appdelegate{
    if(!_appdelegate)
        _appdelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appdelegate;
}

-(NSArray *)viaOptionsList{
    if(_viaOptionsList)
        return _viaOptionsList;
    else
        return [[NSArray alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
/*
 UNCOMMENT THIS TO RE-INSERT HELP TUTORIAL -QUICK TOUR
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"tutorialWatched"]) {
        [self performSegueWithIdentifier:@"showTutorial" sender:self];
    }
  */
    
    [self findPhoneType];
    // Do any additional setup after loading the view.
    
    self.isMenuOpen = NO;
    self.viaOptionsList = [[NSArray alloc] init];
    self.selectedViaOptionIndex = 0;
    
    self.viaRoutesView.hidden = YES;
    
     self.favDelegate= [[FavouritesAndRecentsTableDelegate alloc] initWithTableView:self.favRecentTableView];
    self.favDelegate.delegate = self;
//    [self.favRecentTableView setNeedsDisplay];

    [self.favRecentTableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];

    
    self.triangleBorderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangle 212.png"]];
    self.triangleBottomBorderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangle down 212.png"]];


    if(phonemodel == Iphone6 || phonemodel == Iphone6plus ){
        self.searchJourneyLeftMarginConstraint.constant = 40;
        self.searchJourneyRightMarginConstraint.constant = 40;
        self.searchJourneyTopMarginConstraint.constant = 40;
    }
    
    self.viaDropDownButton.enabled = NO;
    self.viaDropdownArrowImage.alpha = 0.1;
    
    self.searchJourneyBgView.layer.borderWidth = 1.0f;
    self.searchJourneyBgView.layer.borderColor = [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1] CGColor];
    
    self.viaBgView.layer.borderWidth = 1.0f;
    self.viaBgView.layer.borderColor = [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1] CGColor];
    self.viaBgView.layer.masksToBounds = YES;
    
    [self.viaTableView setSeparatorColor:[UIColor grayColor]];

    self.viaTableView.layer.borderWidth = 1.0f;
    self.viaTableView.layer.borderColor = [[[UIColor blackColor] colorWithAlphaComponent:0.3] CGColor];
    self.viaTableView.bounces = NO;
    
    self.fromStationLabel.layer.cornerRadius = 5;
    self.fromStationLabel.layer.masksToBounds = YES;
    self.toStationLabel.layer.cornerRadius = 5;
    self.toStationLabel.layer.masksToBounds = YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];


    self.favDelegate = nil;
    self.favDelegate = [[FavouritesAndRecentsTableDelegate alloc] initWithTableView:self.favRecentTableView];
    self.favDelegate.delegate = self;
//        [self.favRecentTableView setNeedsDisplay];
    [self.favRecentTableView reloadData];
    
    [self.favRecentTableView addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    
    
    if(self.fromStationId && self.toStationId)
    {
        
        self.appdelegate.journey.startStationName = self.fromStationLabel.text;
        self.appdelegate.journey.startStationId = self.fromStationId;
        
        self.appdelegate.journey.endStationName = self.toStationLabel.text;
        self.appdelegate.journey.endStationId = self.toStationId;
        
        [self.appdelegate.journey generateViaList];
    }
    [self.favRecentTableView reloadData];
    

    
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"%f",self.favRecentTableView.contentSize.height);
     self.favTableHeight.constant = self.favRecentTableView.contentSize.height;
//    CGRect frame = self.favRecentTableView.frame;
//    frame.size = self.favRecentTableView.contentSize;
//    self.favRecentTableView.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"Memory warning from RoutePlannerViewController");
}

- (void)dealloc
{
    self.favRecentTableView.delegate = nil;
    self.favRecentTableView.dataSource = nil;
    self.viaTableView.delegate = nil;
    self.viaTableView.dataSource = nil;
    self.searchPageScrollView .delegate = nil;
}


#pragma mark - Navigation


- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if([identifier isEqualToString:@"journeyResults"]){
        
        if(self.fromStationId == 0
           || self.toStationId == 0 )
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route selected" message:@"No route was selected. Please select from/to stations to see results." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
        if(self.appdelegate.journey.viaRoutesList.count == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return NO;
        }

        self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
        self.appdelegate.journey.departureDay = [self dayOfWeekFromDate:[NSDate date]];
//        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        //Start an activity indicator here
        
        [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
        
        [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
        
        [self hideActivityView];
        
        
        NSArray *selectedViaTextArray = [[self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex] componentsSeparatedByString:@"-"];
        int totalNoOfJourneyLegs = self.appdelegate.journey.partJournies.count;
        
        if ([selectedViaTextArray.firstObject isEqualToString:@"Direct Connectivity"]){
            if(totalNoOfJourneyLegs == 0 )
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return NO;
            }else {
                return YES;
            }
        }
        
        if(totalNoOfJourneyLegs == 0 )
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
        if(! selectedViaTextArray.count+1 == totalNoOfJourneyLegs)  //+1 to account for the source and destination switches
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return NO;
        }
        
    }else if([identifier isEqualToString:@"savedSearchResults"]){
        
        
    }
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //fromStationSelection
    //fromStationSelection
    //showResults
    NSLog(@"%@",[segue identifier]);
    
    if([segue.identifier isEqualToString:@"fromStationSelection"]){
        StationSelectionViewController *ssvc = nil;
        
        if([[segue destinationViewController] isKindOfClass:StationSelectionViewController.class]){
            ssvc = [segue destinationViewController];
        }else{
            return;
        }
        ssvc.delegate = self;
        ssvc.selectionType = @"source";
        ssvc.headerText = @"Select Source Station";
        
    }else if([segue.identifier isEqualToString:@"toStationSelection"]){
        StationSelectionViewController *ssvc = nil;
        
        if([[segue destinationViewController] isKindOfClass:StationSelectionViewController.class]){
            ssvc = [segue destinationViewController];
        }else{
            return;
        }
        ssvc.delegate = self;
        ssvc.selectionType = @"destination";
        ssvc.headerText = @"Select Destination Station";
        
    }else if([segue.identifier isEqualToString:@"journeyResults"]){
        //moving this code to shouldPerformSegueWithIdentifier.
        //It is needed first for verification whether route exists
        //We can use the same result objects here.
        
        //Use self.appdelegate.journey, which is the Full Journey object to populate the PartJourney object
        //Part Journeys will be based on the selected viaOption in FullJourney object.
//        self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
//        [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
        
        if([[segue destinationViewController] isKindOfClass:ResultsListViewController.class]){
            ResultsListViewController *rmvc = [segue destinationViewController];

                for(SavedJourney *jny in self.favDelegate.favList)
                {
                    if(jny.fromStationId == self.appdelegate.journey.startStationId &&
                       jny.toStationId == self.appdelegate.journey.endStationId &&
                       jny.selectedViaIndex == self.appdelegate.journey.selectedViaIndex)
                    {
                        rmvc.isFavourite = YES;
                        break;
                    }
                    rmvc.isFavourite = NO;
                }
            
            
            [self saveSearchToRecents];
            
            NSArray *firstPartJourney = [self.appdelegate.journey.partJournies firstObject];
            NSArray *lastPartJourney = [self.appdelegate.journey.partJournies lastObject];
            
            //START - On Results page, set header source and destination station names.
            RouteStation *homeStation = [firstPartJourney firstObject];
            RouteStation *ultimateStation = [lastPartJourney lastObject];
            
            rmvc.fromStationText = homeStation.stationName;
            rmvc.toStationText = ultimateStation.stationName;
            rmvc.selectedViaRouteNameText = [self.viaOptionsList objectAtIndex:self.appdelegate.journey.selectedViaIndex];
            rmvc.journey = self.appdelegate.journey;
            int noOfInterchanges = self.appdelegate.journey.partJournies.count - 1;
            
                        rmvc.selectedViaRouteInterchangesText = [NSString stringWithFormat:@"%@ %@",noOfInterchanges == 0 ?@"No":@(noOfInterchanges).stringValue, noOfInterchanges > 1?@"interchanges":@"interchange"];
            
            NSLog(@"%d",ultimateStation.timeInMins-homeStation.timeInMins);
            
            int hrs = self.appdelegate.journey.departureTimeInMinutes/60;
            int mins = self.appdelegate.journey.departureTimeInMinutes%60;
            rmvc.selectedTimeText = [NSString stringWithFormat:@"%@:%@",@(hrs>12?hrs%12:hrs).stringValue, @(mins).stringValue];
            
            rmvc.selectedTimeText = @"Now";
            
            if(ultimateStation.timeInMins-homeStation.timeInMins > 0)
            {
                hrs = (ultimateStation.timeInMins-homeStation.timeInMins)/60;
                mins = (ultimateStation.timeInMins-homeStation.timeInMins)%60;
            }
            else
            {
                hrs = (ultimateStation.timeInMins+1440-homeStation.timeInMins)/60;
                mins = (ultimateStation.timeInMins+1440-homeStation.timeInMins)%60;
            }
            if(hrs >= 1)
            {
                rmvc.selectedViaRouteEstimatedTimeText = [NSString stringWithFormat:@"%@ %@ %@ Mins",@(hrs).stringValue,hrs == 1 ?@"Hr":@"Hrs", @(mins).stringValue];
            }else {
                rmvc.selectedViaRouteEstimatedTimeText = [NSString stringWithFormat:@"%@ Mins", @(mins).stringValue];
            }
            
            rmvc.selectedDayText = @"";
            
            //END - On Results page, set header source and destination station names.
            /*
            NSMutableString *mutStr = [[NSMutableString alloc] init];
            bool routeExists = NO;
            [mutStr appendString:@"\n===== START JOURNEY =====\n"];
            for(NSArray *partJourney in self.appdelegate.journey.partJournies){
                for(RouteStation *routeStation in partJourney){
                    if([routeStation.time isEqualToString:@""] && [routeStation.stationName isEqualToString:@""]){
//                        continue;
                    }
                    routeExists = YES;
                    [mutStr appendString:[NSString stringWithFormat:@"%@ at %@ ",routeStation.stationName, routeStation.time]];
                    [mutStr appendString:@"\n"];
                }
                if(partJourney != self.appdelegate.journey.partJournies.lastObject){
                  [mutStr appendString:@"\n===== Change Train =====\n"];
                }
            }
            [mutStr appendString:@"\n===== END JOURNEY ====="];

            if(!routeExists){
                mutStr = [NSMutableString stringWithString:@"No trains to destination at this time. Try alternate Via route."];
            }
            
            rmvc.debugText = mutStr;*/
        }
        else{
            return;
        }
        
    }
    else if([segue.identifier isEqualToString:@"savedSearchResults"]){
        //moving this code to shouldPerformSegueWithIdentifier.
        //It is needed first for verification whether route exists
        //We can use the same result objects here.
        
        //Use self.appdelegate.journey, which is the Full Journey object to populate the PartJourney object
        //Part Journeys will be based on the selected viaOption in FullJourney object.
        //        self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
        //        [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
        
//        
//        if(! selectedViaTextArray.count+1 == totalNoOfJourneyLegs)  //+1 to account for the source and destination switches
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//            [self didSelectStationWithName:nil andStationId:nil selectedAs:nil];
//            return;
//        }

        
        
        if([[segue destinationViewController] isKindOfClass:ResultsListViewController.class]){
            ResultsListViewController *rmvc = [segue destinationViewController];
            
            
            for(SavedJourney *jny in self.favDelegate.favList)
            {
                if(jny.fromStationId == self.appdelegate.journey.startStationId &&
                   jny.toStationId == self.appdelegate.journey.endStationId &&
                   jny.selectedViaIndex == self.appdelegate.journey.selectedViaIndex)
                {
                    rmvc.isFavourite = YES;
                    break;
                }
                rmvc.isFavourite = NO;
            }
            
            
            NSArray *firstPartJourney = [self.appdelegate.journey.partJournies firstObject];
            NSArray *lastPartJourney = [self.appdelegate.journey.partJournies lastObject];
            
            //START - On Results page, set header source and destination station names.
            RouteStation *homeStation = [firstPartJourney firstObject];
            RouteStation *ultimateStation = [lastPartJourney lastObject];
            
            rmvc.fromStationText = homeStation.stationName;
            rmvc.toStationText = ultimateStation.stationName;
            rmvc.selectedViaRouteNameText = [self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex];
            rmvc.journey = self.appdelegate.journey;
            int noOfInterchanges = @(self.appdelegate.journey.partJournies.count).intValue - 1;
            
            rmvc.selectedViaRouteInterchangesText = [NSString stringWithFormat:@"%@ %@",noOfInterchanges == 0 ?@"No":@(noOfInterchanges).stringValue, noOfInterchanges > 1?@"interchanges":@"interchange"];
            
            NSLog(@"%d",ultimateStation.timeInMins-homeStation.timeInMins);
            
            int hrs = self.appdelegate.journey.departureTimeInMinutes/60;
            int mins = self.appdelegate.journey.departureTimeInMinutes%60;
            rmvc.selectedTimeText = [NSString stringWithFormat:@"%@:%@",@(hrs>12?hrs%12:hrs).stringValue, @(mins).stringValue];
            
            rmvc.selectedTimeText = @"Now";
            
            if(ultimateStation.timeInMins-homeStation.timeInMins > 0)
            {
                hrs = (ultimateStation.timeInMins-homeStation.timeInMins)/60;
                mins = (ultimateStation.timeInMins-homeStation.timeInMins)%60;
            }
            else
            {
                hrs = (ultimateStation.timeInMins+1440-homeStation.timeInMins)/60;
                mins = (ultimateStation.timeInMins+1440-homeStation.timeInMins)%60;
            };
            
            if(hrs >= 1)
            {
                rmvc.selectedViaRouteEstimatedTimeText = [NSString stringWithFormat:@"%@ %@ %@ Mins",@(hrs).stringValue,hrs == 1 ?@"Hr":@"Hrs", @(mins).stringValue];
            }else {
                rmvc.selectedViaRouteEstimatedTimeText = [NSString stringWithFormat:@"%@ Mins", @(mins).stringValue];
            }
            
            rmvc.selectedDayText = @"";
            
            //END - On Results page, set header source and destination station names.
            /*
             NSMutableString *mutStr = [[NSMutableString alloc] init];
             bool routeExists = NO;
             [mutStr appendString:@"\n===== START JOURNEY =====\n"];
             for(NSArray *partJourney in self.appdelegate.journey.partJournies){
             for(RouteStation *routeStation in partJourney){
             if([routeStation.time isEqualToString:@""] && [routeStation.stationName isEqualToString:@""]){
             //                        continue;
             }
             routeExists = YES;
             [mutStr appendString:[NSString stringWithFormat:@"%@ at %@ ",routeStation.stationName, routeStation.time]];
             [mutStr appendString:@"\n"];
             }
             if(partJourney != self.appdelegate.journey.partJournies.lastObject){
             [mutStr appendString:@"\n===== Change Train =====\n"];
             }
             }
             [mutStr appendString:@"\n===== END JOURNEY ====="];
             
             if(!routeExists){
             mutStr = [NSMutableString stringWithString:@"No trains to destination at this time. Try alternate Via route."];
             }
             
             rmvc.debugText = mutStr;*/
        }
        else{
            return;
        }
        
    }
    else if([segue.identifier isEqualToString:@"browseTheMap"]){
        BrowseMapViewController *bmvc = segue.destinationViewController;
        //nothing to process
//        [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:bmvc withObject:nil];
        
    }
    else if([segue.identifier isEqualToString:@"showContentWebView"]){
        ContentWebViewController *cwvc = segue.destinationViewController;
        cwvc.htmlFileName = self.contentHtmlFileName;
        cwvc.title = self.contentTitle;
    }
}

-(void) saveSearchToRecents
{
    SavedJourney *savedJourney = [SavedJourney new];
    
    savedJourney.fromStationName = self.fromStationLabel.text;
    savedJourney.toStationName = self.toStationLabel.text;
    
    savedJourney.selectedViaText = [self.viaOptionsList objectAtIndex:self.selectedViaOptionIndex];
    savedJourney.selectedViaIndex = self.selectedViaOptionIndex;
    
    savedJourney.fromStationId = self.fromStationId;
    savedJourney.toStationId = self.toStationId;
    
    NSDate *date = [NSDate date];
    
    savedJourney.savedDate = date;
    
    //TODO: Core Data (INSERT RECENT)
    
    NSString *keyForSaving = [NSString stringWithFormat:@"Recent_F%d_T%d_V%d",savedJourney.fromStationId,savedJourney.toStationId,savedJourney.selectedViaIndex];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    //no exist comparison needed for recent search, just show last 5
    /*if([[[prefs dictionaryRepresentation] allKeys] containsObject:keyForSaving]){
        NSLog(@"Key already exists, returning");
        return;
    }*/
    
    int recentCount = 0;
    NSDictionary *defaultAsDic = [prefs dictionaryRepresentation];
    NSArray *keyArr = [defaultAsDic allKeys];
    for (NSString *key in keyArr)
    {
        NSLog(@"key [%@] => Value [%@]",key,[defaultAsDic valueForKey:key]);
        
        
        NSData *encodedObj = [defaultAsDic objectForKey:key];
        if (! ([[key componentsSeparatedByString:@"_"][0]
                isEqualToString:@"Recent"]))
            continue;
        
        SavedJourney *eachObj = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObj];
        
        
        if([eachObj isKindOfClass:SavedJourney.class])
        {
            if ([[key componentsSeparatedByString:@"_"][0] isEqualToString:@"Recent"])
            {
                if(recentCount>4){
                    [prefs removeObjectForKey:key];
                }
                recentCount++;
            }
        }
    }
    
    NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:savedJourney];
    [prefs setObject:myEncodedObject forKey:keyForSaving];
    [prefs synchronize];
    NSLog(@"Saved!");
}


#pragma mark - button tapped code

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

bool swapped=YES;
- (IBAction)swapRouteButtonTapped:(UIButton *)sender {
    
    if(!self.fromStationId || !self.toStationId)
    {
        return;
    }
    
    if(sender)
    {
        //do animation only if the button was pressed from Home screen.
        //There is a possibility this was invoked as a delegate call from Results screen.
        [self rotateImage:self.swapButtonImage duration:0.2 curve:UIViewAnimationCurveEaseIn degrees:swapped?180:360];
        swapped = !swapped;
    }
    //Direct swap labels
//    NSString *tmp = self.fromStationLabel.text;
//    self.fromStationLabel.text = self.toStationLabel.text;
//    self.toStationLabel.text = tmp;
    
    //Swap labels with animation
    
    CGRect view1Frame = self.fromStationLabel.frame;
    CGRect view2Frame = self.toStationLabel.frame;
    
    if (!CGRectIntersectsRect(self.fromStationLabel.frame, self.toStationLabel.frame)) {
        
        self.fromStationLabel.frame = view2Frame;
        self.toStationLabel.frame = view1Frame;
        NSString *tmp;
        tmp = self.fromStationLabel.text;
        self.fromStationLabel.text = self.toStationLabel.text;
        self.toStationLabel.text = tmp;
        
        view1Frame = self.fromStationLabel.frame;
        view2Frame = self.toStationLabel.frame;
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
            self.fromStationLabel.frame = view2Frame;
            self.toStationLabel.frame = view1Frame;
        } completion:nil];
    }

    
    self.appdelegate.journey.startStationName = self.fromStationLabel.text;
    self.appdelegate.journey.startStationId = self.toStationId;
    
    self.appdelegate.journey.endStationName = self.toStationLabel.text;
    self.appdelegate.journey.endStationId = self.fromStationId;
    
    self.fromStationId = self.appdelegate.journey.startStationId;
    self.toStationId = self.appdelegate.journey.endStationId;
    
        if(self.fromStationId && self.toStationId)
        {
            [self loadViaStations];
        }
    

}

-(void) loadViaStations{
    self.viaDropDownButton.enabled = YES;
    self.viaDropdownArrowImage.alpha = 1.0f;
    
    self.viaOptionsList = [self.appdelegate.journey generateViaList];
    if(self.viaOptionsList.count == 0){
        //no via routes possible for the combination? return control.
        return;
    }
    self.viaRoutesHeaderLabel.text = [NSString stringWithFormat:@"Via: %@", [self.appdelegate.journey.viaRoutesNameList objectAtIndex:0]];
    int numberOfViaOptionsShownWithoutScroll = self.viaOptionsList.count> 3? 3:self.viaOptionsList.count;
    
    
    int heightChange = self.viaViewHeightConstraint.constant;
    
    if(self.viaViewHeightConstraint.constant != 44*numberOfViaOptionsShownWithoutScroll){
        heightChange = (44*numberOfViaOptionsShownWithoutScroll);
    }
    
    if(self.viaViewHeightConstraint.constant == 0){
        [UIView animateWithDuration:0.5 animations:^{
            [self.viaTableView reloadData];
            [self.viaTableView beginUpdates];
            [self.viaTableView endUpdates];
            [self.view layoutIfNeeded];
        }];
        return;
    }
    
    if(self.viaViewHeightConstraint.constant != 44*numberOfViaOptionsShownWithoutScroll){
        heightChange = (44*numberOfViaOptionsShownWithoutScroll);
    }
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.viaViewHeightConstraint.constant = heightChange;
        [self.viaTableView reloadData];
        [self.viaTableView beginUpdates];
        [self.viaTableView endUpdates];
        [self.view layoutIfNeeded];
    }];
    
    
//    if(self.appdelegate.journey.selectedViaIndex >= self.viaOptionsList.count-1)
//    {
////        NSIndexPath *iP = [NSIndexPath indexPathWithIndex:0];
//        [self.viaTableView selectRowAtIndexPath:iP animated:NO scrollPosition:UITableViewScrollPositionNone];
//        self.selectedViaOptionIndex = 0;
//        self.appdelegate.journey.selectedViaIndex = 0;
//    }

}

- (IBAction)toggleViaButtonTapped:(UIButton *)sender {
    int numberOfViaOptionsShownWithoutScroll = self.viaOptionsList.count> 3? 3:self.viaOptionsList.count;
    
    int heightChange = self.viaViewHeightConstraint.constant;
    if(self.viaViewHeightConstraint.constant<40){
        heightChange += 44*numberOfViaOptionsShownWithoutScroll;
        
        [self rotateImage:self.viaDropdownArrowImage duration:0.2 curve:UIViewAnimationCurveEaseIn degrees:180];
        

        self.viaRoutesHeaderLabel.textColor = [UIColor blackColor];
    }else{
        heightChange -= 44*numberOfViaOptionsShownWithoutScroll;
        
        [self rotateImage:self.viaDropdownArrowImage duration:0.2 curve:UIViewAnimationCurveEaseIn degrees:360];
        self.viaRoutesHeaderLabel.textColor = [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1];
    }
    
    [self.view layoutIfNeeded];
//    [UIView animateWithDuration:0.5 animations:^{
//        self.viaViewHeightConstraint.constant = heightChange;
//        [self.view layoutIfNeeded];
//    }];
    self.viaMaskView.hidden = NO;
    self.viaTableView.hidden = NO;
    
}

#pragma mark - Image rotation code

// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

- (void)rotateImage:(UIImageView *)image duration:(NSTimeInterval)duration
              curve:(int)curve degrees:(CGFloat)degrees
{
    // Setup the animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // The transform matrix
    CGAffineTransform transform =
    CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    image.transform = transform;
    
    // Commit the changes
    [UIView commitAnimations];
}

#pragma mark - TableView methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viaOptionsList.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.appdelegate.journey.startStationId   = self.fromStationId;
    self.appdelegate.journey.startStationName = self.fromStationLabel.text;
    
    self.appdelegate.journey.endStationId   = self.toStationId;
    self.appdelegate.journey.endStationName = self.toStationLabel.text;
    
    self.appdelegate.journey.selectedViaIndex = indexPath.row;
    
    NSLog(@"Via Options for the Journey: %@", self.appdelegate.journey.viaRoutesNameList);
    NSLog(@"Selected Via Index: %d",self.appdelegate.journey.selectedViaIndex);
    
    NSLog(@"Current mins since 0000HRS : %d", [self minutesSinceMidnight:[NSDate date]]);
    
    self.viaRoutesHeaderLabel.text = [NSString stringWithFormat:@"Via: %@", [self.appdelegate.journey.viaRoutesNameList objectAtIndex:indexPath.row]];
    self.viaMaskView.hidden = YES;
    self.viaTableView.hidden = YES;
    
//    return;
}

-(NSInteger) dayOfWeekFromDate:(NSDate *) date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
    return [comps weekday];
}


-(int) minutesSinceMidnight:(NSDate *)date{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    int mins = 60 * [components hour] + [components minute];
    return 60 * [components hour] + [components minute];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"viaOptionCell"];
    cell.textLabel.text = [self.viaOptionsList objectAtIndex:indexPath.row];
    
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.minimumScaleFactor = 0.5;
    
//    cell.layer.borderColor = [[UIColor grayColor] CGColor];
//    cell.layer.borderWidth = 0.5;

    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = [UIColor colorWithRed:(255/255.0) green:(192/255.0) blue:(0/255.0) alpha:0.5];
    cell.selectedBackgroundView = selectionColor;
    
    
    NSLog(@"%@", cell.textLabel.text);
    if(indexPath.row == 0 || self.appdelegate.journey.selectedViaIndex >= self.viaOptionsList.count-1){
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        self.selectedViaOptionIndex = 0;
        self.appdelegate.journey.selectedViaIndex = 0;
    }
    return cell;
}

#pragma mark - custom delegate methods

-(void)didSelectStationWithName:(NSString *)stationName andStationId:(int)stationId selectedAs:(NSString *)type{
    if([type isEqualToString:@"source"]){
        self.fromStationLabel.text = stationName;
//        self.fromStationLabel.textColor = [UIColor greenColor];
        self.fromStationId =  stationId;
    } else if([type isEqualToString:@"destination"]){
        self.toStationLabel.text = stationName;
//        self.toStationLabel.textColor = [UIColor greenColor];
        self.toStationId =  stationId;
    }
    
    if(self.fromStationId && self.toStationId){
//        self.viaRoutesView.hidden = NO;
//        self.viaTableView.hidden = NO;
        //load the via selection list
        if(!self.appdelegate.journey)
            self.appdelegate.journey = [[FullJourney alloc] init];
        
        self.appdelegate.journey.startStationName = self.fromStationLabel.text;
        self.appdelegate.journey.startStationId = self.fromStationId;
        
        self.appdelegate.journey.endStationName = self.toStationLabel.text;
        self.appdelegate.journey.endStationId = self.toStationId;

        [self loadViaStations]; //self.viaRoutesList is populated after this. Default selection route#0.
    }else{
        self.viaRoutesView.hidden = YES;
    }
    return;
}

-(void)swappedStationsFromResultsView
{
    [self swapRouteButtonTapped:nil];
}

#pragma mark - Activity Indicator methods

-(void)showActivityView
{
    UIWindow *window = self.appdelegate.window;
    self.activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [self.activityView addSubview:activityWheel];
    [window addSubview: self.activityView];
    
    [[[self.activityView subviews] objectAtIndex:0] startAnimating];
}

-(void)hideActivityView
{
    [[[self.activityView subviews] objectAtIndex:0] stopAnimating];
    [self.activityView removeFromSuperview];
    self.activityView = nil;
}

-(void) showResultsPageWithSavedSearchObject:(SavedJourney *)savedJourney
{
    if(!self.appdelegate.journey)
        self.appdelegate.journey = [[FullJourney alloc] init];
    
    self.savedJourneyChosen = savedJourney;
    
    self.appdelegate.journey.startStationId = self.savedJourneyChosen.fromStationId;
    self.appdelegate.journey.endStationId = self.savedJourneyChosen.toStationId;
    self.appdelegate.journey.startStationName = self.savedJourneyChosen.fromStationName;
    self.appdelegate.journey.endStationName = self.savedJourneyChosen.toStationName;
    [self.appdelegate.journey generateViaList];
    self.appdelegate.journey.selectedViaIndex = self.savedJourneyChosen.selectedViaIndex;
    
    
    if(self.appdelegate.journey.startStationId == 0
       || self.appdelegate.journey.endStationId == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route selected" message:@"No route was selected. Please select from/to stations to see results." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self didSelectStationWithName:nil andStationId:nil selectedAs:nil];
        return;
    }
    
    if(self.appdelegate.journey.viaRoutesList.count == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self didSelectStationWithName:nil andStationId:nil selectedAs:nil];
        return;
    }
    
    self.appdelegate.journey.departureTimeInMinutes = [self minutesSinceMidnight:[NSDate date]];
    //        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //Start an activity indicator here
    
    [NSThread detachNewThreadSelector:@selector(showActivityView) toTarget:self withObject:nil];
    
    [self.appdelegate.journey findTrainsBetweenStartAndEndStation:self.appdelegate.journey.departureTimeInMinutes];
    
    [self hideActivityView];
    
    
    NSArray *selectedViaTextArray = [[self.appdelegate.journey.viaRoutesNameList objectAtIndex:self.appdelegate.journey.selectedViaIndex] componentsSeparatedByString:@"-"];
    int totalNoOfJourneyLegs = @(self.appdelegate.journey.partJournies.count).intValue;
    
    if ([selectedViaTextArray.firstObject isEqualToString:@"Direct Connectivity"]){
        if(totalNoOfJourneyLegs == 0 )
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [self didSelectStationWithName:nil andStationId:nil selectedAs:nil];
            return;
        }
    }
    
    if(totalNoOfJourneyLegs == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No route found" message:@"No route could be found for the via route selected. Please try selecting an alternate route" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self didSelectStationWithName:nil andStationId:nil selectedAs:nil];
        return;
    }
    
    [self performSegueWithIdentifier:@"savedSearchResults" sender:self];
    
}

#pragma mark - Menu Methods


- (IBAction)menuButtonTapped:(UIBarButtonItem *)sender {
    CGFloat originalWidth = self.view.bounds.size.width;
    self.translateValue = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        CGFloat translateValue = 0;
        if(phonemodel == Iphone5_5s || phonemodel == Iphone4_4s)
        {
            self.translateValue = originalWidth+0;
        }else{
            self.translateValue = originalWidth+0;
        }
        
    }else{
        //ios 7, don't translate! doesn't work well
        self.translateValue = originalWidth-320;
    }
    
    if(!self.slideMenuVC){
        self.slideMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"slideMenuVC"];
        self.slideMenuVC.delegate = self;
        [self.slideMenuVC.view setFrame:CGRectMake(-self.view.frame.size.width, -1, self.view.frame.size.width, self.view.frame.size.height)];
        [self addChildViewController:self.slideMenuVC];
        [self.view addSubview:self.slideMenuVC.view];
        [self.slideMenuVC didMoveToParentViewController:self];
        self.slideMenuVC.view.userInteractionEnabled = YES;
        [self.view layoutIfNeeded];
    }
    
    
    
    
    if(self.isMenuOpen){
        //HIDE MENU
        self.navigationController.navigationBar.topItem.title = @"MRM";
        [UIView animateWithDuration:0.2 animations:^{
            
            [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(0, 0)];
            [self.searchPageScrollView setTransform:CGAffineTransformMakeTranslation(0, 0)];
            [self.slideMenuVC.view setTransform:CGAffineTransformMakeTranslation(-self.slideMenuVC.view.frame.size.width, 0)];
        }];
    }else{
        //SHOW MENU
        [UIView animateWithDuration:0.2 animations:^{
            [self.searchPageScrollView setTransform:CGAffineTransformMakeTranslation(self.translateValue, 0)];
            [self.slideMenuVC.view setTransform:CGAffineTransformMakeTranslation(self.slideMenuVC.view.frame.size.width, 0)];
            [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(self.navigationController.navigationBar.frame.size.width, 0)];
            
        }];
    }
    
    self.isMenuOpen = !self.isMenuOpen;

}

     
-(void) closeMenuScreen
{
    [self menuButtonTapped:nil];
}

-(void) selectedMenuItemAtIndex:(NSInteger) itemIndex
{
    switch(itemIndex)
    {
        case 0:
        //close menu and slide back to home screen
        self.isMenuOpen = YES;
        [self menuButtonTapped:nil];
            break;
        case 1:
             [self performSegueWithIdentifier:@"browseTheMap" sender:self];
            break;
        case 2:
            self.contentHtmlFileName = @"line";
            self.contentTitle = @"Lines";
            [self performSegueWithIdentifier:@"showContentWebView" sender:self];
            break;
        case 3:
            self.contentHtmlFileName = @"ticket";
            self.contentTitle = @"Ticketing";
            [self performSegueWithIdentifier:@"showContentWebView" sender:self];
            break;
        case 4:
            self.contentHtmlFileName = @"about";
            self.contentTitle = @"About MRM";
            [self performSegueWithIdentifier:@"showContentWebView" sender:self];
            break;
        case 5:
            [self sendGenericFeedbackEmail];
            break;
        case 6:
            self.contentHtmlFileName = @"tos";
            self.contentTitle = @"Terms Of Use";
            [self performSegueWithIdentifier:@"showContentWebView" sender:self];
            break;
        default:
            break;
    }
}

- (IBAction)hideViaListButtonTapped:(UIButton *)sender {
    self.viaMaskView.hidden = YES;
    self.viaTableView.hidden = YES;
}


#define URLMrmAppWebsite @"http://www.mrmapp.in"

- (IBAction)openMrmAppWebsiteTapped:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLMrmAppWebsite]];
    
}

#define URLEMail @"contact@mrmapp.in"

-(void)sendGenericFeedbackEmail
{
    
    // Email Subject
    NSString *emailTitle = [NSString stringWithFormat:@"Feedback - MrmApp iOS v%@",[[[NSBundle mainBundle] infoDictionary]
                                                                                    objectForKey:@"CFBundleShortVersionString"] ];
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:URLEMail];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        picker.mailComposeDelegate = self;
        [picker setSubject:emailTitle];
        [picker setMessageBody:nil isHTML:YES];
        [picker setToRecipients:toRecipents];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}



- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end





















