//
//  IntermediateStationsListView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "IntermediateStationsListView.h"

@implementation IntermediateStationsListView


-(UIColor *)lineColor{
    if(!_lineColor)
        _lineColor = [UIColor new];
    return _lineColor;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            self = [[[NSBundle mainBundle] loadNibNamed:@"IntermediateStationsListView"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
            [self commonInit];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            
            self = [[[NSBundle mainBundle] loadNibNamed:@"IntermediateStationsListView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            [self commonInit];
        }
    }
    return self;
}

- (void)commonInit
{
    
    
//    [self setLineColor];
    // self.intermediateStationsView.frame = CGRectMake(0, 50, 320, 200);
}



-(void) setLineColorForLine:(UIColor *)lineColor
{
    self.lineColor = lineColor;
//    if(!self.lineColor)
//        self.lineColor = [UIColor colorWithRed:255/255.0 green:65/255.0 blue:129/255.0 alpha:1];
    //iterate through all the components which show the line color and set the color
    UIImage *img;
    for ( UIImageView *iv in self.stationMarkerImageViews){
        img = [iv.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iv.tintColor = lineColor;
        iv.image = img;
    }
    
}

@end
