//
//  UISegementedControl+Retouch.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/10/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "UISegementedControl+Retouch.h"

@implementation UISegementedControl_Retouch

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// this sends a value changed event even if we reselect the currently selected segment
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSInteger current = self.selectedSegmentIndex;
    [super touchesBegan:touches withEvent:event];
    if (current == self.selectedSegmentIndex) {
        [self setSelectedSegmentIndex:UISegmentedControlNoSegment];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}


@end
