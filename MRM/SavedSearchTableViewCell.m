//
//  SavedSearchView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/1/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "SavedSearchTableViewCell.h"

@implementation SavedSearchTableViewCell


enum PhoneModel{
    Iphone4_4s,
    Iphone5_5s,
    Iphone6,
    Iphone6plus
};

enum PhoneModel phonemodel;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)findPhoneType{
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        
        if( screenHeight > 480 && screenHeight < 667 ){
            phonemodel = Iphone5_5s;
            NSLog(@"iPhone 5/5s");
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            phonemodel = Iphone6;
            NSLog(@"iPhone 6");
            //            heightAdjustment = 200;
        } else if ( screenHeight > 480 ){
            phonemodel = Iphone6plus;
            NSLog(@"iPhone 6 Plus");
        } else {
            NSLog(@"iPhone 4/4s");
            phonemodel = Iphone4_4s;
            
        }
    }
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            self = [[[NSBundle mainBundle] loadNibNamed:@"SavedSearch"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
            [self commonInit];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            
            self = [[[NSBundle mainBundle] loadNibNamed:@"SavedSearch"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
            [self commonInit];
        }
    }
    return self;
}

- (void)commonInit
{
    if(phonemodel == Iphone6){
        for(NSLayoutConstraint* constraint in self.recentLeftStarViewWidth){
            constraint.constant = 50;
        }
//        self.recentLeftStarViewWidth.constant = 50;
    } else if(phonemodel == Iphone6plus){
        for(NSLayoutConstraint* constraint in self.recentLeftStarViewWidth){
            constraint.constant = 70;
        }
    }
    
//    [self setLineColor];
    // self.intermediateStationsView.frame = CGRectMake(0, 50, 320, 200);
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (void)layoutSubviews
{
    [super layoutSubviews];
    for (UIView *subview in self.subviews) {
//        NSLog(NSStringFromClass([subview class]));
        if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"]) {
            ((UIView*)subview).layer.cornerRadius=7.0f;
            ((UIView*)subview).layer.masksToBounds = YES;
        }
        if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellContentView"]) {
            ((UIView*)subview).layer.cornerRadius=7.0f;
            ((UIView*)subview).layer.masksToBounds = YES;
        }
    }
}

- (IBAction)addToFavouriteButtonTapped:(UIButton *)sender {
    
}
@end
