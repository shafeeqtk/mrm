//
//  Line.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Line : NSObject

@property (strong, nonatomic) NSString *lineId;
@property (strong, nonatomic) NSString *lineCode;
@property (strong, nonatomic) NSString *lineName;
@property (strong, nonatomic) NSString *lineColor;
@end
