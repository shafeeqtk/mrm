//
//  MRMStationsLine.m
//  MRM
//
//  Created by Shafeeq Rahiman on 6/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MRMStationsLine.h"

@implementation MRMStationsLine

-(NSArray *)stationsListWithinLine{
    if(!_stationsListWithinLine)
        _stationsListWithinLine = [[NSArray alloc] init];
    return _stationsListWithinLine;
}

@end
