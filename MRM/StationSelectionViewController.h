//
//  StationSelectionViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/8/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol StationSelectionDelegate;


@interface StationSelectionViewController : UIViewController <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UITextFieldDelegate,UISearchBarDelegate, UIScrollViewDelegate>

@property (nonatomic, assign) id<StationSelectionDelegate> delegate;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSMutableArray *stationsList;
@property (strong, nonatomic) NSMutableArray *resultTempArray;
@property (strong, nonatomic) NSArray *allStationsArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSString *selectionType;
@property (strong, nonatomic) IBOutlet UILabel *navBarHeaderLabel;
@property (strong, nonatomic) IBOutlet UIView *topNavView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topNavViewHeightConstraint;
@property (strong, nonatomic) NSString *headerText;

@end



@protocol StationSelectionDelegate <NSObject>

@optional
-(void) didSelectStationWithName:(NSString *)stationName andStationId:(int)stationId selectedAs:(NSString *)type;

@end