//
//  StationTrain.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StationTrain : NSObject


@property (strong, nonatomic) NSString *platf_no;
@property (strong, nonatomic) NSString *platf_side;
@property (strong, nonatomic) NSString *speed;
@property int train_time;
@property int noOfCar;
@property int lineId;
@property int trainId;
@property (strong, nonatomic) NSString *trainTimeStr;
@property (strong, nonatomic) NSString *direction;
@property (strong, nonatomic) NSString *start_stn_name;
@property (strong, nonatomic) NSString *end_stn_name;
@property (strong, nonatomic) NSString *end_stn_code;
@property (strong, nonatomic) NSString *special_info;
@property BOOL isFutureTrain;
@property BOOL isNextTrain;
@property int currentStationId;

@end
