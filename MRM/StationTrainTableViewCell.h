//
//  StationTrainTableViewCell.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationTrainTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *trainLastStopAndSpeedLabel;
@property (strong, nonatomic) IBOutlet UILabel *platformNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *trainSpecialInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *carInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *trainComingFromLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *towardsStationCodeLabel;

@end
