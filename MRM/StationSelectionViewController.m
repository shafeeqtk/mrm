//
//  StationSelectionViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/8/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "StationSelectionViewController.h"
#import "AppDelegate.h"
#import "UIImage-JTColor.h"
#import "UISegementedControl+Retouch.h"
#import "Station.h"

@interface StationSelectionViewController ()
@property (strong, nonatomic) IBOutlet UISegementedControl_Retouch *segmentedController;
@property (strong, nonatomic) IBOutlet UIView *lineSelectionView;
@property (strong, nonatomic) IBOutlet UIButton *currentLocationButton;

@property (strong, nonatomic) AppDelegate *appdelegate;

@property (strong, nonatomic) FMDatabase *db;
@property (strong, nonatomic) NSArray *titles;
@property (strong, nonatomic) NSArray *icons;
@property (nonatomic,retain) NSMutableDictionary *sections;
@property (strong, nonatomic) Station *currStationObj;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIImageView *backButtonImageView;

@property (strong, nonatomic) NSTimer *locationTimer;

@property CGFloat originalTopNavHeight;
@property (strong, nonatomic) IBOutlet UILabel *headerSmall;


@end

@implementation StationSelectionViewController

-(AppDelegate *)appdelegate{
    if(!_appdelegate)
        _appdelegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appdelegate;
}

-(NSMutableArray *)stationsList{
    if(!_stationsList)
        _stationsList = [[NSMutableArray alloc] init];
    return _stationsList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerSmall.hidden = YES;
    self.topNavView.clipsToBounds = YES;
    
    self.headerSmall.text = self.headerText;
    self.navBarHeaderLabel.text = self.headerText;
    self.originalTopNavHeight = self.topNavViewHeightConstraint.constant;
    // items to be used for each segment (same as UISegmentControl) for all instances
    self.titles = [NSArray arrayWithObjects:[@"All" uppercaseString], [@"Local" uppercaseString], [@"Metro" uppercaseString],[@"Mono" uppercaseString], nil];
    
    
    if ([self.stationsList count] == 0 ) {
        self.stationsList = [self loadStationsList];
    }
    
    [self createDictionaryOfStations];
    self.allStationsArray = self.stationsList;
    
    self.currentLocationButton.titleLabel.tintColor = [UIColor darkGrayColor];
//    self.currentLocationButton.titleLabel.font = [UIFont fontWithName:@"MyriadPro-BoldSemiCn" size:17];
    
//    [self currentLocationFetch:nil];
    
    
    UITextField *textField = [self.searchBar valueForKey:@"_searchField"];
    if(textField)
        textField.clearButtonMode = UITextFieldViewModeAlways;
    

//    self.tableView.contentOffset = CGPointMake(0, self.searchBar.frame.size.height);
    /*
     //Scrolls down to the first section of tableView automatically
     //Had this when hiding the search was desired.
    [UIView animateWithDuration:1 animations:^{
        self.tableView.contentOffset = CGPointMake(0, self.searchBar.frame.size.height);
    } completion:nil];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    /*if ([[sender identifier] isEqualToString:@"showResultsMain"]){
        
        MRMStation* source, *destination;
        
        for(int i=0;i<self.stationsList.count;i++){
            for(MRMStation *station in [self.stationsList objectAtIndex:i]){
                if([station.name isEqualToString:@"Andheri"]){
                    source = station;
                }else if([station.name isEqualToString:@"Ambernath"]){
                    destination = station;
                }
            }
            
            if(!source && !destination){
                break;
            }
        }
        
    }*/
    
}

- (IBAction)popToMainScreen:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Database query handlers

- (NSMutableArray *) loadStationsList
{
    NSLog(@"Loading stations from App Delegate");
    
    return [self.appdelegate.stationsMaster mutableCopy];

    
}



-(void)createDictionaryOfStations{
    self.sections = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the stations and create our keys
    for (Station *eachstation in self.stationsList)
    {
        NSString *c = [eachstation.stationName substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [self.sections allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [self.sections setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the stations into their respective keys
    for (Station *eachstation in self.stationsList)
    {
        [[self.sections objectForKey:[eachstation.stationName substringToIndex:1]] addObject:eachstation];
    }
    
}

#pragma mark - Segmented Control methods

- (IBAction)localSelected:(UIButton *)sender {
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select Line:" delegate:self cancelButtonTitle:@"All Local Lines" destructiveButtonTitle:nil otherButtonTitles:
                            @"Western (WR)",
                            @"Central (CR)",
                            @"Harbour (HR)",
                            @"Trans Harbour (TH)",
                            @"Diva-Vasai (DV)",
                            nil];
    popup.tag = 1;
//    popup.
//    popup.cancelButtonIndex = [popup addButtonWithTitle:@"All Local Lines"];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(IBAction) handleLineSelection: (UISegmentedControl *)segmentedControl {
    if(segmentedControl.selectedSegmentIndex<0)
        return;
//    NSString *selectedLineValue = @"All";
    if(segmentedControl.selectedSegmentIndex == 1){
                [self localSelected:nil];
                return;
    }else{
        //based on line selection (all,mono, metro - filter the results)
        NSString *line = @"All";
        switch (segmentedControl.selectedSegmentIndex)
        {
            case 2:
                line = @"8";
                break;
            case 3:
                line = @"7";
                break;
            default:
                break;
        }
        [self filterTableWithLineName:line];
    }
    
}

#pragma mark - Table View Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"stationnamecell"];
//    return cell;
    Station *station;
    UITableViewCell *cell;
    
    station = [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                  reuseIdentifier:@"stationnamecell"];
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:17.0]];
    
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.text = station.stationName;

    [cell.detailTextLabel setFont:[UIFont systemFontOfSize:12.0]];
    cell.detailTextLabel.textColor = [UIColor grayColor];

    cell.detailTextLabel.text = station.lineNames;
    
    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    NSMutableArray *indexList =
    [NSMutableArray arrayWithCapacity:[self.sections count]+1];
    [indexList addObject:UITableViewIndexSearch];
    for (NSString *item in [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)])
        [indexList addObject:[item substringToIndex:1]];
    return indexList;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *titleStr = [NSString stringWithFormat:@" %@",[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]];
    
    return titleStr;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.sections allKeys] count];
}

- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title
                atIndex:(NSInteger)index
{
    if (index == 0)
    {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    return index - 1; // due magnifying glass icon
}

bool fromSelected, toSelected;


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {

    NSInteger sumSections = 0;
    int currentRow = 0;
    for (int i = 0; i < indexPath.section; i++) {
        int rowsInSection = [tableView numberOfRowsInSection:i];
        sumSections += rowsInSection;
    }
    currentRow = sumSections + indexPath.row;
    
    if([[self.stationsList objectAtIndex:currentRow] isKindOfClass:Station.class]){
        
        Station *station = (Station *)[self.stationsList objectAtIndex:currentRow];
        NSLog(@"--> %d %@ %@ %d",indexPath.row, self.selectionType, station.stationName, station.stationId);
        [self.delegate didSelectStationWithName:station.stationName andStationId:station.stationId selectedAs:self.selectionType];

        [self dismissViewControllerAnimated:YES completion:nil];

    }
    return;
}

#pragma mark - Search methods


-(void)filterTableWithLineName:(NSString *)string
{
    NSString *searchString = string;
    NSArray *resultTempArray = self.allStationsArray;
    NSMutableArray *resultArray;
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lines CONTAINS[c] %@", searchString];
    if([string isEqualToString:@"All"]){
        filter = [NSPredicate predicateWithFormat:@"SELF.stationId!=nil"];
    }else if([string isEqualToString:@"Local-All"]){
        filter = [NSPredicate predicateWithFormat:@"NOT(SELF.lineNames CONTAINS[c] %@) AND NOT(SELF.lineNames CONTAINS[c] %@)",@"Metro", @"Mono"];
    }
    resultArray = [[resultTempArray filteredArrayUsingPredicate:filter] mutableCopy];
    
    if (!searchString || searchString.length == 0) {
        
        self.stationsList = [resultTempArray mutableCopy];
    } else {
        if (resultArray.count == 0) {
            NSLog(@"No data From Search");
        }
        else {
            self.stationsList = resultArray;
        }
    }
    [self createDictionaryOfStations];
    [self reloadTableView];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    if ([searchBar.text isEqualToString:@""]){
        [searchBar resignFirstResponder];
        return;
    }
    searchBar.text = @"";
    
    [self handleLineSelection:self.segmentedController];
    
//    self.stationsList = [self.allStationsArray mutableCopy];
//    [self createDictionaryOfStations];
//    [self reloadTableView];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}
// hide search bar's cancel button when not editing
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = NO;
    
    return YES;
    
}



-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        self.headerSmall.hidden = NO;
                         self.navBarHeaderLabel.hidden = YES;
                         [self toggleBackButtonVisibility];
                        self.topNavViewHeightConstraint.constant = 40;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil
     ];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.headerSmall.hidden = YES;
                        self.navBarHeaderLabel.hidden = NO;
                         [self toggleBackButtonVisibility];
                         self.topNavViewHeightConstraint.constant = self.originalTopNavHeight;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil
     ];
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString *searchString;
    NSArray *resultTempArray = self.allStationsArray;
    NSMutableArray *resultArray;
    
    if (text.length > 0) {
        searchString = [NSString stringWithFormat:@"%@%@",searchBar.text, text];
    } else {
        searchString = [searchBar.text substringToIndex:[searchBar.text length] - 1];
    }
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.stationName CONTAINS[c] %@", searchString];
    resultArray = [[resultTempArray filteredArrayUsingPredicate:filter] mutableCopy];
    
    if (!searchString || searchString.length == 0) {
        self.stationsList = [resultTempArray mutableCopy];
    } else {
        if (resultArray.count == 0) {
            NSLog(@"No data From Search");
            self.stationsList = [[[NSArray alloc] init] mutableCopy];
        }
        else {
            self.stationsList = resultArray;
        }
    }
    [self createDictionaryOfStations];
    [self reloadTableView];
    return YES;
}

-(void) reloadTableView {
    dispatch_async(dispatch_get_main_queue(), ^{ [self.tableView reloadData]; });
}

#pragma mark - Action Sheet methods


-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *val = @"";
    NSString *lineValueDisplayed;
    switch (buttonIndex)
    {
        case 0:
            val = @"WR";
            lineValueDisplayed = @"1";
            break;
        case 1:
            val = @"CR";
            lineValueDisplayed = @"2";
            break;
        case 2:
            val = @"HR";
            lineValueDisplayed = @"3";
            break;
        case 3:
            val = @"TH";
            lineValueDisplayed = @"4";
            break;
        case 4:
            val = @"DV";
            lineValueDisplayed = @"5";
            break;
        case 5:
            val = @"All";
            lineValueDisplayed = @"Local-All";
            break;
        default:
            lineValueDisplayed = @"Local-All";
            break;
    }
    
    val = [NSString stringWithFormat:@"Local - %@", val];

    [self.segmentedController setTitle:val forSegmentAtIndex:self.segmentedController.selectedSegmentIndex];
    [self filterTableWithLineName:lineValueDisplayed];
//    [self initializeLineSelectionComponent];
//    [self.control setSelectedSegmentIndex:1];
}

#pragma mark - page button actions


#pragma mark - Location Fetch methods


- (IBAction)currentLocationFetch:(id)sender {
    if (!_locationManager)
    {
        self.locationManager = [CLLocationManager new];
    }
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    CLAuthorizationStatus locationAuthStatus = [CLLocationManager authorizationStatus];
    if(locationAuthStatus == kCLAuthorizationStatusDenied || locationAuthStatus == kCLAuthorizationStatusNotDetermined || locationAuthStatus == kCLAuthorizationStatusRestricted){
        NSLog(@"Not Authorized for Location services");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Authorized for Location Services"
                                                        message:@"MRM could not find your current location. Please select station from the list."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else{
        NSLog(@"Authorized for Location services");
        [self.locationManager startUpdatingLocation];
        self.locationTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopUpdatingLocations) userInfo:nil repeats:NO];
    }
    
}

- (void)stopUpdatingLocations {
    [self.locationManager stopUpdatingLocation];
    [self.locationTimer invalidate];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to find location"
                                                    message:@"MRM could not find your current location. \nPlease select station from the list."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self.locationTimer invalidate];
    //    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    
    [self.locationManager stopUpdatingLocation];
    double latitude, longitude;
    
    CLLocation * currentLocation = (CLLocation *)[locations lastObject];
    NSLog(@"Location: %@", currentLocation);
    
    NSLog(@"::Current Location:: \nLAT:%f \nLONG:%f", latitude, longitude);
    
    NSLog(@"Loading Station Lat/Long from database");
    
    FMResultSet *rs = [self.appdelegate.db executeQuery:@"SELECT _id,name,lat,lng FROM STATION_MASTER ORDER BY NAME ASC"];
    //    NSSort
    float lowestDistance = 3;
    float closestX = 0.0, closestY = 0.0;
    NSString *closestStationName = @"";
    int closestStationId = -1;
    
    self.currStationObj = nil;
    
    while ([rs next]) {
        NSString *stationid = [NSString stringWithFormat:@"%d",[rs intForColumn:@"_id"]];
        NSString *name = [rs stringForColumn:@"name"];
        double lat = [rs doubleForColumn:@"lat"];
        double lng = [rs doubleForColumn:@"lng"];
        int x,y;
        
        CLLocation *station = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
        
        float radiusInKm = [station distanceFromLocation:currentLocation]/1000;
        
        if ( radiusInKm < 3 ){
            if(radiusInKm < lowestDistance)
            {
                lowestDistance = radiusInKm;
                closestStationName = name;
                closestStationId = stationid.intValue;
                
                self.currStationObj = [self.appdelegate getStationFromStationId:stationid.intValue];
                
//                self.fromTextField.text = name;
                

                closestX = x;
                closestY = y;
            }
        }
        
        //        self.scrollView.zoomScale = 3.0;
        //        self.scrollView.center = CGPointMake(x/4.0, y/4.0);
    }
    
    
    NSLog(@"%@",closestStationName);
//    [UIView animateWithDuration:1.0
//                          delay:0
//                        options:UIViewAnimationOptionBeginFromCurrentState
//                     animations:^{
//                         [self.scrollView setZoomScale:3.0f animated:NO];
//                         CGRect frame = CGRectMake(((closestX-100)/4.0), ((closestY-150)/4.0), 50, 50);
//                         [self.scrollView zoomToRect:frame animated:NO];
//                         
//                         //                             CGFloat newContentOffsetX = (self.contentSize.width/2) - (self.bounds.size.width/2);
//                         //
//                         //                             self.scrollView.center = CGPointMake(x/4.0, y/4.0);
//                         
//                     }
//                     completion:nil];
    
    [rs close];
    
    if(self.currStationObj){
        [self.delegate didSelectStationWithName:self.currStationObj.stationName andStationId:self.currStationObj.stationId selectedAs:self.selectionType];
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }else{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location not supported"
                                                    message:@"MRM could not find your location. Please select a station from the list."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    }
    
}

- (IBAction)backButtonTapped:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

-(void)toggleBackButtonVisibility{
    self.backButton.hidden = !self.backButton.hidden;
    self.backButtonImageView.hidden = !self.backButtonImageView.hidden;
}
@end
