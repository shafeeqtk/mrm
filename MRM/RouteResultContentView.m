//
//  RouteResultContentView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/23/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "RouteResultContentView.h"
#import "IntermediateStationsListView.h"
#import "RouteStation.h"

@interface RouteResultContentView ()

@property bool isShowStationsToggled;
@property int noOfIntStations;
@end

@implementation RouteResultContentView

-(UIColor *)lineColor{
    if(!_lineColor)
        _lineColor = [UIColor new];
    return _lineColor;
}

static NSString *CellIdentifier = @"cell";

-(NSMutableArray *)stationsList{
    if(!_stationsList)
        _stationsList = [[NSMutableArray alloc] init];
    return _stationsList;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//-(id)initWithCoder:(NSCoder *)aDecoder
//{
//    if ((self = [super initWithCoder:aDecoder])) {
//        self = [[[NSBundle mainBundle] loadNibNamed:@"RouteResultContentView"
//                                              owner:nil
//                                            options:nil] objectAtIndex:0];
//    }
//    return self;
//}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.subviews.count == 0) {
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteResultContentView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{

            self = [[[NSBundle mainBundle] loadNibNamed:@"RouteResultContentView"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
            }
            [self commonInit];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            if( screenHeight < screenWidth ){
                screenHeight = screenWidth;
            }
            
            if( screenHeight > 480 && screenHeight < 667 ){
                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteResultContentView"
                                                      owner:self
                                                    options:nil] objectAtIndex:0];
            }
            else{

                self = [[[NSBundle mainBundle] loadNibNamed:@"RouteResultContentView"
                                              owner:self
                                            options:nil] objectAtIndex:0];
            }
        [self commonInit];
        }
    }
    return self;
}

- (void)commonInit
{
    _isShowStationsToggled = YES;   //important, this is for ensuring the show-hide full routes works properly
//    self.stationsList = [[NSArray alloc] initWithObjects:@"Vile Parle",@"Mahim",@"Bandra",nil];
//    [self addIntermediateStations:self.stationsList];
    
//    [self setLineColor];
    
// self.intermediateStationsView.frame = CGRectMake(0, 50, 320, 200);
    
    
    self.trainInfoView.layer.shadowOpacity = 0.4;
    self.trainInfoView.layer.shadowRadius = 1;
    self.trainInfoView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.trainInfoView.layer.shadowOffset = CGSizeMake(0, 0);
}

-(void) setLineColorForLine:(UIColor *)lineColor
{
    self.lineColor = lineColor;
//    self.lineColor = [UIColor colorWithRed:255/255.0 green:65/255.0 blue:129/255.0 alpha:1];
    //iterate through all the components which show the line color and set the color
    UIImage *img;
    for ( UIImageView *iv in self.stationMarkerImageViews){
        img = [iv.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iv.tintColor = lineColor;
        iv.image = img;
    }
    
}


float eachStationHeight = 28;

-(void)addStationToList:(NSString *) stationName atTime:(NSString *)time{
    RouteStation *intermediateStation = [[RouteStation alloc] init];
    intermediateStation.stationName = stationName;
    intermediateStation.time = time;
    [self.stationsList addObject:intermediateStation];
    
}

-(void)addIntermediateStations{
//    if(stationObjectList){
    if(self.stationsList){
        _noOfIntStations = self.stationsList.count;
//        noOfIntStations = 1;
        int stationsAdded = 0;
        for (RouteStation *station in self.stationsList)
        {
            IntermediateStationsListView *stn1 =[[IntermediateStationsListView alloc] init];
            stn1.arrivalStationNameLabel.font = [UIFont boldSystemFontOfSize:12];
            
            if ([station.time isEqualToString:@""]){
                [stn1 setLineColorForLine:[UIColor clearColor]];
                stn1.arrivalStationNameLabel.alpha = 0.5;
            }else{
                [stn1 setLineColorForLine:self.lineColor];
                stn1.arrivalStationNameLabel.textColor = [UIColor darkGrayColor];
            }
            stn1.arrivalStationNameLabel.text = station.stationName;
            stn1.arrivalTimeLabel.text = station.time;
            stn1.frame = CGRectMake(0,(stationsAdded++)*eachStationHeight,self.intermediateStationsView.frame.size.width, stationsAdded*eachStationHeight);
            //stn1.hidden = YES; //hide it initially
            [self.intermediateStationsView addSubview:stn1];
        }
        
//        
//        stn1 =[[IntermediateStationsListView alloc]init];
//        stn1.frame = CGRectMake(0,1*eachStationHeight,self.intermediateStationsView.frame.size.width, stationsAdded*eachStationHeight);
//        [self.intermediateStationsView addSubview:stn1];
//        
        
        //self.intermediateStationsListHeightConstraint.constant = noOfIntStations*eachStationHeight;
        self.intermediateStationsListHeightConstraint.constant = 0;
        self.intermediateStationsView.alpha = 0.0;
//        self.intermediateStationsView.frame = CGRectMake(self.intermediateStationsView.frame.origin.x,self.intermediateStationsView.frame.origin.y,self.intermediateStationsView.frame.size.width,0);
        /*self.intermediateStationsView.frame = CGRectMake(self.intermediateStationsView.frame.origin.x,self.intermediateStationsView.frame.origin.y,self.intermediateStationsView.frame.size.width,0);
         */
//        stationsAdded++;
    }
    
    
}

float sizeToExpand = 0;
-(void)expandIntermediateStationsListWithToggleFlag:(bool)toggleFlag
{
//    noOfIntStations = 1;
//    NSLog(@"Height of ISV: %f",self.intermediateStationsListHeightConstraint.constant);
    
    [self.superview layoutIfNeeded];
    [self layoutIfNeeded];
    if(self.intermediateStationsListHeightConstraint.constant == 0) {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.intermediateStationsListHeightConstraint.constant = _noOfIntStations*eachStationHeight;
                             [self.superview layoutIfNeeded];
                             [self layoutIfNeeded];
                             self.intermediateStationsView.alpha = self.intermediateStationsView.alpha == 0.0? 1.0:0.0 ;
                        }
                         completion: nil
         ];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.intermediateStationsListHeightConstraint.constant = 0 ;
                             [self.superview layoutIfNeeded];
                             [self layoutIfNeeded];
                             self.intermediateStationsView.alpha = self.intermediateStationsView.alpha == 0.0? 1.0:0.0 ;
                         } completion: nil
         ];
    }


//    NSLog(@"INDEX IN SUPERVIEW : %lu",(unsigned long)[self.superview.subviews indexOfObject:self]);
    
    if(toggleFlag){
        [self rotateImage:self.trainInfoToggleRouteDetailsImageView duration:0.2 curve:UIViewAnimationCurveEaseIn degrees:_isShowStationsToggled?180:360];
        _isShowStationsToggled = !_isShowStationsToggled;
    }

    

}

- (IBAction)expandIntermediateStationsList:(UIButton *)sender {
    [self expandIntermediateStationsListWithToggleFlag:YES];
}



// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

- (void)rotateImage:(UIImageView *)image duration:(NSTimeInterval)duration
              curve:(int)curve degrees:(CGFloat)degrees
{
    // Setup the animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    // The transform matrix
    CGAffineTransform transform =
    CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    image.transform = transform;
    
    // Commit the changes
    [UIView commitAnimations];
}


- (IBAction)allTrainsListButtonPressed:(UIButton *)sender {
    
    NSLog(@"Advanced station list: %@ to %@ direction: %d list index:%lu",self.startStationLabel.text,self.endStationLabel.text, self.direction, (unsigned long)[[self.superview subviews] indexOfObject:self]);
    
//    self.startStationLabel.text
//    self.endStationLabel.text

    [self.delegate loadAdvancedTrainSelectionTableViewFromStationId:self.startStationId toStationId:self.endStationId onLineId:self.lineId atTimeInMins:self.timeInMins inDirection:self.direction];
    return;
}


@end
