//
//  MRMStation.h
//  MRM
//
//  Created by Shafeeq Rahiman on 6/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RouteStation : NSObject

@property (strong,nonatomic) NSString *stationName;
@property (strong,nonatomic) NSString *station_id;
@property (strong,nonatomic) NSString *speed;
@property (strong,nonatomic) NSString *time;
@property (strong,nonatomic) NSString *platform_no;
@property (strong,nonatomic) NSString *platform_side;
@property (nonatomic) int line;
@property (nonatomic) int no_of_car;
@property (strong,nonatomic) NSString *all_lines;
@property (strong,nonatomic) NSString *all_line_ids;
@property (nonatomic) int timeInMins;

@property (strong,nonatomic) NSString *trainStartedFromStationName;
@property (strong,nonatomic) NSString *trainGoingTowardsStationName;
@property (strong,nonatomic) NSString *trainGoingTowardsStationCode;
@property (strong,nonatomic) NSString *specialInfo;
@property (strong,nonatomic) NSString *trainDirection;

-(NSString *) stationDescription ;
-(void) initializeStationWithstationName:(NSString *)name stationId:(int)stationId line:(int)line;

@end
