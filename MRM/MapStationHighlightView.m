//
//  MapStationHighlightView.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/29/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MapStationHighlightView.h"
#import "SpriteData.h"

@interface MapStationHighlightView()

@property bool boundsSet;

@end
@implementation MapStationHighlightView


-(NSMutableArray *)drawStationsArray{
    if(!_drawStationsArray)
        _drawStationsArray = [[NSMutableArray alloc] init];
    return _drawStationsArray;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.scrollZoomScale = 1;
    }
    [self commonInit];
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.scrollZoomScale = 1;
    }
    [self commonInit];
    return self;
}

-(void) toggleVisibility{
    self.hidden = !self.hidden;
}

-(void) commonInit {
    self.boundsSet = NO;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    if(!self.boundsSet)
    {
        //        NSLog(@"%f,%f", self.superview.bounds.size.width, self.superview.bounds.size.height);
        CGRect frm = CGRectMake(0, 0, self.superview.bounds.size.width, self.superview.bounds.size.height );
        self.frame = frm;
        self.boundsSet = YES;
        
    }
    float scaleFactor = 4/self.scrollZoomScale;
    float fixedScale = 4;
    float markerScale = 0.5;
    
    //    NSLog(@"drawRect scaleFactor = %f", scaleFactor);
    
    if(!self.drawStationsArray)
        return;
    
    
    //  self.frame = CGRectOffset( self.frame, 0, 0 );
    
    //    NSLog(@"Drawing Start time : %@",[[NSDate alloc] init]);
    
    
    //change scaleFactor = 2 for drawing station names and markers
    markerScale = 0.5;
    
    NSLog(@"DRAWRECT SCALE= %f %f ",self.scrollZoomScale, scaleFactor);
    
    for (NSObject *obj in self.drawStationsArray)
    {
        if([obj isKindOfClass:SpriteData.class])
        {
            SpriteData *eachSprite = (SpriteData *) obj;
            
            if ([eachSprite.spriteType isEqualToString:@"Marker"])
            {
                NSString *assetName = [NSString stringWithFormat:@"marker_%d", eachSprite.mActive];
                UIImage *stationSprite = [UIImage imageNamed:assetName];
                CGPoint spriteCoordinates = CGPointMake(eachSprite.mapX*markerScale, eachSprite.mapY*markerScale);
                CGSize spriteSize = CGSizeMake(eachSprite.mapW*markerScale, eachSprite.mapH*markerScale);
                
                UIImage *eachStationMarker = [self extractImageAtPoint:spriteCoordinates
                                                                ofSize:spriteSize
                                                       FromSpriteSheet:stationSprite];
                
                CGRect mapFrame = CGRectMake(eachSprite.spriteX/fixedScale ,eachSprite.spriteY/fixedScale , (eachStationMarker.size.width/2)*self.baseImageRatio*self.scrollZoomScale, (eachStationMarker.size.height/2)*self.baseImageRatio*self.scrollZoomScale);
                NSLog(@"Station Marker frame (%f , %f) size = %f %f", mapFrame.origin.x,mapFrame.origin.y, mapFrame.size.width,mapFrame.size.height);
                [eachStationMarker drawInRect:mapFrame];
                
            }
            else
            {
                //Station Name Sprite
                NSString *assetName = [NSString stringWithFormat:@"txt_%d",eachSprite.mActive];
                UIImage *namesprite = [UIImage imageNamed:assetName];
                UIImage *eachStationNameImg;
                CGPoint spriteCoordinates = CGPointMake(eachSprite.spriteX*markerScale, eachSprite.spriteY*markerScale);
                CGSize spriteSize = CGSizeMake(eachSprite.mapW*markerScale, eachSprite.mapH*markerScale);
                
                eachStationNameImg = [self extractImageAtPoint:spriteCoordinates
                                                        ofSize:spriteSize
                                               FromSpriteSheet:namesprite];
                
                CGRect mapFrame = CGRectMake(eachSprite.mapX/fixedScale ,eachSprite.mapY/fixedScale , (eachStationNameImg.size.width/2)*self.scrollZoomScale, (eachStationNameImg.size.height/2)*self.scrollZoomScale);
                NSLog(@"Station Name frame (%f , %f) size = %f %f", mapFrame.origin.x,mapFrame.origin.y, mapFrame.size.width,mapFrame.size.height);
                [eachStationNameImg drawInRect:mapFrame];
                
                
            }
            
        }
        
    }
}


-(UIImage*)extractImageAtPoint:(CGPoint)pointExtractedImg ofSize:(CGSize)sizeExtractedImg FromSpriteSheet:(UIImage*)imgSpriteSheet
{
    UIImage *extractedImage;
    
    CGRect rectExtractedImage;
    
    rectExtractedImage=CGRectMake(pointExtractedImg.x,pointExtractedImg.y,sizeExtractedImg.width,sizeExtractedImg.height);
    
    CGImageRef imgRefSpriteSheet=imgSpriteSheet.CGImage;
    
    CGImageRef imgRefExtracted=CGImageCreateWithImageInRect(imgRefSpriteSheet,rectExtractedImage);
    
    extractedImage=[UIImage imageWithCGImage:imgRefExtracted];
    
    CGImageRelease(imgRefExtracted);
    
    return extractedImage;
    
    
}



@end
