//
//  HomeScreenViewController.h
//  MRM
//
//  Created by Shafeeq Rahiman on 10/19/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end
