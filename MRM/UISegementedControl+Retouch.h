//
//  UISegementedControl+Retouch.h
//  MRM
//
//  Created by Shafeeq Rahiman on 11/10/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegementedControl_Retouch : UISegmentedControl

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

@end
