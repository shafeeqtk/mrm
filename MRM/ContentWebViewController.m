//
//  ContentWebViewController.m
//  MRM
//
//  Created by Shafeeq Rahiman on 2/7/15.
//  Copyright (c) 2015 Glass Box Software. All rights reserved.
//

#import "ContentWebViewController.h"

@interface ContentWebViewController ()

@end

@implementation ContentWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [[NSBundle mainBundle] URLForResource:self.htmlFileName withExtension:@"html"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(0, 0)];
    self.navigationController.navigationBar.topItem.title = @"Back";
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    self.navigationController.navigationBar.topItem.title = self.title;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil ];
    
    
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
