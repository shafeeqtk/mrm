//
//  MRMStationLink.m
//  MRM
//
//  Created by Shafeeq Rahiman on 6/26/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "MRMStationLink.h"

@implementation MRMStationLink

-(NSString *)strokeColor{
    if(!_strokeColor)
        _strokeColor = @"";
    return _strokeColor;
}

-(NSString *)drawScript{
    if(!_drawScript)
        _drawScript = @"";
    return _drawScript;
}

@end
