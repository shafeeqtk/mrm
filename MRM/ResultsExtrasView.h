//
//  ResultsExtrasView.h
//  MRM
//
//  Created by Shafeeq Rahiman on 12/27/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ResultsExtrasViewDelegate;

@interface ResultsExtrasView : UIView

@property (nonatomic, assign) id<ResultsExtrasViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIImageView *favouriteIconImageView;
@property (strong, nonatomic) IBOutlet UILabel *favouriteTextLabel;
@property BOOL isFavouriteAdd;

@end




@protocol ResultsExtrasViewDelegate <NSObject>

@optional

-(BOOL) favouriteToggled;
-(void) shareJourneyTapped;
-(void) composeFeedbackEmail;

@end

