//
//  FullJourney.m
//  MRM
//
//  Created by Shafeeq Rahiman on 12/4/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "FullJourney.h"
#import "AppDelegate.h"
#import "FMDatabase.h"
#import "Station.h"
#import "RouteStation.h"
#import "RouteStations.h"
#import "StationTrain.h"
#import "Line.h"

@interface FullJourney()

@property (strong, nonatomic) AppDelegate *appdelegate;

@end


@implementation FullJourney

-(AppDelegate *)appdelegate{
    if(!_appdelegate){
        _appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appdelegate;
}

-(NSMutableArray *)partJournies{
    if(!_partJournies)
        _partJournies = [[NSMutableArray alloc] init];
    return _partJournies;
}

-(NSArray *)  generateViaList
{
    
    NSMutableArray *routeArray = [[NSMutableArray alloc] init];
    NSMutableArray *timeArray = [[NSMutableArray alloc] init];
    NSMutableArray *tmpRoutesNameList = [[NSMutableArray alloc] init];
    
    //self.viaRoutesList = [[NSMutableArray alloc] init];
    //FMResultSet *rs_viaStationIds = [self.db executeQuery:@"SELECT distinct route from station_route_mapping where (source_station_id = ? and dest_station_id = ?) OR (source_station_id = ? and dest_station_id = ?) order by priority asc" withArgumentsInArray:@[[@(self.startStationId) stringValue],[@(self.endStationId) stringValue], [@(self.endStationId) stringValue],[@(self.startStationId) stringValue]] ];
    
    FMResultSet *rs_viaStationIds = [self.appdelegate.db executeQuery:@"SELECT distinct route, time from station_route_mapping where (source_station_id = ? and dest_station_id = ?) order by priority asc" withArgumentsInArray:@[[@(self.startStationId) stringValue],[@(self.endStationId) stringValue]] ];
    while ([rs_viaStationIds next]) {
        
        NSString *route = [rs_viaStationIds stringForColumn:@"ROUTE"];
        NSString *travelTimeBetweenStations = [rs_viaStationIds stringForColumn:@"TIME"];
        
        NSLog(@"%@ - %@", route, travelTimeBetweenStations);
        
        NSMutableString *routeDescription = [[NSMutableString alloc]initWithString:@""];
        NSArray *routeSplit;
        if ([route length] == 1 && [route isEqualToString:@"0"]){
            [routeDescription appendString:@"Direct Connectivity"];
            routeSplit = @[];
            [routeArray addObject:routeSplit];
        }
        else{
            routeSplit = [route componentsSeparatedByString:@"|"];
            [routeArray addObject:routeSplit];
            for (NSString *stId in routeSplit)
            {
                NSString *junctionStationName = [self findStationNameById:[stId intValue]];
                
                [routeDescription appendString:junctionStationName];
                if (! [routeSplit.lastObject isEqualToString:stId]){
                    [routeDescription appendString:@"-"];
                }
            }
        }
        [tmpRoutesNameList addObject:routeDescription.description];
        
        int time = 0;
        
        NSArray *travelTimeSplit = [travelTimeBetweenStations componentsSeparatedByString:@"|"];
        for (NSString *eachDuration in travelTimeSplit)
        {
            time += eachDuration.intValue;
        }

        int hrs = (time)/60;
        int mins = (time)%60;
        NSString *timeStr = @"";
        
        if(hrs >= 1)
        {
            timeStr = [NSString stringWithFormat:@"%@ %@ %@ Mins",@(hrs).stringValue,hrs == 1 ?@"Hr":@"Hrs", @(mins).stringValue];
        }else {
            timeStr = [NSString stringWithFormat:@"%@ Mins", @(mins).stringValue];
        }
        [timeArray addObject: timeStr ];

    }
    
    [rs_viaStationIds close];
    
    self.viaRoutesList = [NSArray arrayWithArray:routeArray];
    self.viaRoutesNameList = [NSArray arrayWithArray:tmpRoutesNameList];
    self.viaRoutesTimes = [NSArray arrayWithArray:timeArray];

    for( NSString *str in self.viaRoutesList){
        NSLog(@"viaRoutesList: %@",str);
    }
    

    return self.viaRoutesNameList;
}

-(void)generateFullJourney
{
    //startStationId, endStationId, viaRoutesList and selectedViaIndex are present in this class already.
    //This method will populate the PartJournies required to complete the full journey
    
    
}

-(NSString *) findStationNameById:(int)stationId
{
    NSString *returnStr = @"";
    for (Station *station in self.appdelegate.stationsMaster){
        if(station.stationId == stationId){
            returnStr = station.stationName;
            break;
        }
    }
    return returnStr;
}

-(NSString *) findStationCodeById:(int)stationId
{
    NSString *returnStr = @"";
    for (Station *station in self.appdelegate.stationsMaster){
        if(station.stationId == stationId){
            returnStr = station.stationCode;
            break;
        }
    }
    return returnStr;
}

-(NSString *) findLineNameById:(int)lineId
{
    NSString *returnStr = @"";
    for (Line *line in self.appdelegate.linesMaster){
        if(line.lineId.intValue == lineId){
            returnStr = line.lineName;
            break;
        }
    }
    return returnStr;
}


-(int) findStationIdByName:(NSString *)stationName
{
    int stationId = 0;
    for (Station *station in self.appdelegate.stationsMaster){
        if([station.stationName isEqualToString:stationName]){
            stationId = station.stationId;
            break;
        }
    }
    return stationId;
}

-(void) findTrainsBetweenStartAndEndStation: (int) afterTimeInMins
{
    BOOL journeyStart = YES;
    
    [self.partJournies removeAllObjects];
    
    NSMutableArray *viaStationIds = [[NSMutableArray alloc] init];
    [viaStationIds addObject:@(self.startStationId).stringValue];
    
    if([[self.viaRoutesList objectAtIndex:self.selectedViaIndex] isKindOfClass:NSArray.class]){
        if([[self.viaRoutesList objectAtIndex:self.selectedViaIndex] count] >0){
            NSArray *selectedViaIds = [self.viaRoutesList objectAtIndex:self.selectedViaIndex];
            [viaStationIds addObjectsFromArray:selectedViaIds];
        }
    }else{
        if(![[self.viaRoutesList objectAtIndex:self.selectedViaIndex] isEqualToString:@"Direct Connectivity"]){
            
            int stationId = [self findStationIdByName:[self.viaRoutesList objectAtIndex:self.selectedViaIndex]];
            [viaStationIds addObject:@(stationId).stringValue];
        }
    }
    
    [viaStationIds addObject:@(self.endStationId).stringValue];
    
    NSString *qry_train = @"select tas1.train_id as train_id, tas1.no_of_car as no_of_car, tas1.speed as speed, tas1.direction as direction, tas1.line_id as line_id, tas1.station_id as src_station_id, tas1.time_in_minutes as src_time_in_mins, tas1.platform_no as src_platform_no, tas1.platform_side as src_platform_side, tas2.station_id as dest_station_id, tas2.time_in_minutes as dest_time_in_mins, tas2.platform_no as dest_platform_no, tas2.platform_side as dest_platform_side, tas1.start_stn_id as start_stn_id, tas1.end_stn_id as end_stn_id, tas1.special_info as special_info from %@ tas1 INNER JOIN %@ tas2 ON tas1.train_id = tas2.train_id and tas1.station_id = ? and tas2.station_id = ? and tas1.time_in_minutes < tas2.time_in_minutes and (tas2.time_in_minutes-tas1.time_in_minutes) < 180 and tas1.time_in_minutes > ?  and tas1.direction = tas2.direction and %@ order by tas1.time_in_minutes asc limit 1";
    //removed this condition:
    //and (tas2.time_in_minutes-tas1.time_in_minutes) < 180
    //including new columns
    //tas1.start_stn_name as start_stn_name, tas1.end_stn_name as end_stn_name, tas1.end_stn_code as end_stn_code
    
    
    NSString *qry_allStations = @"select * from %@ WHERE TRAIN_ID = ? AND TIME_IN_MINUTES >= ? AND TIME_IN_MINUTES <= ? ORDER BY TIME_IN_MINUTES";
    
    for (int i=0; i < viaStationIds.count-1; i++)
    {
        
        NSString *fromStationIdStr = [viaStationIds objectAtIndex:i];
        NSString *toStationIdStr = [viaStationIds objectAtIndex:(i+1)];
        NSString *lineStr = @([self.appdelegate getCommonLineIdBetweenStation:fromStationIdStr.intValue secondStationId:toStationIdStr.intValue]).stringValue;
        
        //hold a list of RouteStations for each leg of the journey.
        //All stations are included, regardless of stoppage or not.
        NSMutableArray *partJourney = [[self.appdelegate listOfStationsBetweenStation:fromStationIdStr.intValue andStation:toStationIdStr.intValue onLine:lineStr.intValue] mutableCopy];
        
        int journeyStartTime = afterTimeInMins;
        
        
        //for intermediate journeys, add 2 mins (based on First leg Flag)
        if(!journeyStart)
            journeyStartTime = afterTimeInMins+2;
        
        //After setting start time for first leg, set a Flag.
        if(journeyStart)
            journeyStart = !journeyStart;
        
        NSString *train_id = @"";
        
        
        
        NSArray *paramArray = @[ fromStationIdStr, toStationIdStr, [NSString stringWithFormat:@"%d", afterTimeInMins]];
        
//        FMResultSet *rs = [self.db executeQuery:qry_train_count withArgumentsInArray:paramArray];
//        if([rs next]){
//            if([rs intForColumn:@"cnt"] == 0)
//            {
//                    [self.partJournies removeAllObjects];
//                    return;
//                }
//        }
//        [rs close];
        
        NSString *tableName = @"";
        NSString *sundayOnlyCondition = self.appdelegate.journey.departureDay == 1 ? @" tas1.NOT_ON_SUNDAY = 0 " : @" tas1.SUNDAY_ONLY = 0 ";
        
        if([lineStr isEqualToString:@"5"] || [lineStr isEqualToString:@"6"]){
            tableName = @"TSD_L56";
        }
        else if([lineStr isEqualToString:@"3"] || [lineStr isEqualToString:@"4"]){
            tableName = @"TSD_L34";
        }
        else
        {
            tableName = [NSString stringWithFormat:@"TSD_L%@",lineStr];
        }
        
        NSString *optimizedTrainQuery = [NSString stringWithFormat:qry_train,tableName,tableName, sundayOnlyCondition];
        NSString *optimizedAllTrainsQuery = [NSString stringWithFormat:qry_allStations,tableName];
        
        FMResultSet *rs = [self.appdelegate.db executeQuery:optimizedTrainQuery withArgumentsInArray:paramArray];
//        PartJourney *partJourney = [[PartJourney alloc] init];
        
        bool queryReturnedRows = NO;

        while ([rs next]) {
            queryReturnedRows = YES;
            train_id = [rs stringForColumn:@"train_id"];
            journeyStartTime = [rs intForColumn:@"src_time_in_mins"];
            afterTimeInMins = [rs intForColumn:@"dest_time_in_mins"];
            
            
            FMResultSet *rs2 = [self.appdelegate.db executeQuery:optimizedAllTrainsQuery withArgumentsInArray:@[ train_id, [NSString stringWithFormat:@"%d", journeyStartTime] ,[NSString stringWithFormat:@"%d", afterTimeInMins]]];
            
            while ([rs2 next]) {
                NSString *station_id = [rs2 stringForColumn:@"station_id"];
                NSString *stationName = [self findStationNameById:station_id.intValue];
                
                NSString *speed = [rs2 stringForColumn:@"speed"];
                NSString *direction = [rs2 stringForColumn:@"direction"];
                NSString *platf_no = [rs2 stringForColumn:@"platform_no"];
                NSString *platf_side = [rs2 stringForColumn:@"platform_side"];
                int line = [rs2 intForColumn:@"line_id"];
                int no_of_car = [rs2 intForColumn:@"no_of_car"];
                if(no_of_car == -1 )
                    no_of_car = 12;
                
                int train_time = [rs2 intForColumn:@"time_in_minutes"];
                NSString *start_stn_name = [self findStationNameById:([rs2 stringForColumn:@"start_stn_id"]).intValue];
                NSString *end_stn_name = [self findStationNameById:([rs2 stringForColumn:@"end_stn_id"]).intValue];
                NSString *end_stn_code = [self findStationCodeById:([rs2 stringForColumn:@"end_stn_id"]).intValue];
                NSString *special_info = [rs2 stringForColumn:@"special_info"];
                
                
                NSMutableString* trainTimeStr = [NSMutableString stringWithString:[self minutesToHrsMins:train_time asFormat:@"time"]];
                
                
                for(RouteStation *station in partJourney){
                    if(station.station_id.intValue == station_id.intValue){
                        station.stationName = stationName;
                        station.timeInMins = train_time;
                        station.time = trainTimeStr;
                        station.station_id = station_id;
                        station.speed = speed;
                        station.platform_no = platf_no;
                        station.platform_side = platf_side;
                        station.line = line;
                        station.no_of_car = no_of_car;
                        station.trainStartedFromStationName = start_stn_name;
                        station.trainGoingTowardsStationName = end_stn_name;
                        station.trainGoingTowardsStationCode = end_stn_code;
                        station.trainDirection = direction;
                        station.specialInfo = [special_info isEqualToString:@"LS"]?@"Ladies Special":@"";
                        NSLog(@"Station Name : %@" ,station.stationName);
                        NSLog(@"Station time: %@",station.time);
                        break;
                    }else{
                        //no station match, find its name
                        station.stationName = [self findStationNameById:station.station_id.intValue];
                    }
                }
            }
            [rs2 close];
        }
        
        [rs close];
        
        if(!queryReturnedRows){
            //adding logic to check next day trains is any part of the journey couldn't be completed
//            [self.partJournies removeAllObjects];
            if(afterTimeInMins == 300)
                [self.partJournies removeAllObjects];
            else{
                i--;
                afterTimeInMins = 300;
                continue;
            }
        }
            [self.partJournies addObject:partJourney];
    }
    
    
}


-(BOOL) findTrainsBetweenIntermediateStationId:(int)startStationId andEndStation:(int)endStationId atTime:(int)startimeInMins withTrainId:(int)trainId
{
    bool journeyStart = YES;
    NSMutableArray *tempPartJournies = [NSMutableArray new];
    
    NSMutableArray *viaStationIds = [[NSMutableArray alloc] init];
    [viaStationIds addObject:@(self.appdelegate.journey.startStationId).stringValue];
    
    if([[self.viaRoutesList objectAtIndex:self.selectedViaIndex] isKindOfClass:NSArray.class]){
        if([[self.viaRoutesList objectAtIndex:self.selectedViaIndex] count] >0){
            NSArray *selectedViaIds = [self.viaRoutesList objectAtIndex:self.selectedViaIndex];
            [viaStationIds addObjectsFromArray:selectedViaIds];
        }
    }else{
        if(![[self.viaRoutesList objectAtIndex:self.selectedViaIndex] isEqualToString:@"Direct Connectivity"]){
            
            int stationId = [self findStationIdByName:[self.viaRoutesList objectAtIndex:self.selectedViaIndex]];
            [viaStationIds addObject:@(stationId).stringValue];
        }
    }
    
    [viaStationIds addObject:@(self.appdelegate.journey.endStationId).stringValue];
    
    //don't recompute part-journeys that are completed already
    for (int i=0; i < self.partJournies.count; i++)
    {
//        RouteStation *firstStation = partJourney.firstObject;
        
        NSString *firstStationIdInViaList = [viaStationIds objectAtIndex:0];
        NSString *firstStationIdInThisLeg = @(startStationId).stringValue;
        
        if( !  ([firstStationIdInThisLeg isEqualToString:firstStationIdInViaList]) )
        {
            [viaStationIds removeObjectAtIndex:0];
        }else{
            break;
        }
        
    }
    
    NSString *qry_train_fixed_time = @"select tas1.train_id as train_id, tas1.no_of_car as no_of_car, tas1.speed as speed, tas1.direction as direction, tas1.line_id as line_id, tas1.station_id as src_station_id, tas1.time_in_minutes as src_time_in_mins, tas1.platform_no as src_platform_no, tas1.platform_side as src_platform_side, tas2.station_id as dest_station_id, tas2.time_in_minutes as dest_time_in_mins, tas2.platform_no as dest_platform_no, tas2.platform_side as dest_platform_side, tas1.start_stn_id as start_stn_id, tas1.end_stn_id as end_stn_id, tas1.special_info as special_info from %@ tas1 INNER JOIN %@ tas2 ON tas1.train_id = tas2.train_id and tas1.station_id = ? and tas2.station_id = ? and tas1.time_in_minutes < tas2.time_in_minutes  and (tas2.time_in_minutes-tas1.time_in_minutes) < 180 and tas1.time_in_minutes = ?  and tas1.direction = tas2.direction and tas1.train_id = ? and %@ order by tas1.time_in_minutes asc limit 1";
    
    NSString *qry_train = @"select tas1.train_id as train_id, tas1.no_of_car as no_of_car, tas1.speed as speed, tas1.direction as direction, tas1.line_id as line_id, tas1.station_id as src_station_id, tas1.time_in_minutes as src_time_in_mins, tas1.platform_no as src_platform_no, tas1.platform_side as src_platform_side, tas2.station_id as dest_station_id, tas2.time_in_minutes as dest_time_in_mins, tas2.platform_no as dest_platform_no, tas2.platform_side as dest_platform_side, tas1.start_stn_id as start_stn_id, tas1.end_stn_id as end_stn_id, tas1.special_info as special_info from %@ tas1 INNER JOIN %@ tas2 ON tas1.train_id = tas2.train_id and tas1.station_id = ? and tas2.station_id = ? and tas1.time_in_minutes < tas2.time_in_minutes  and (tas2.time_in_minutes-tas1.time_in_minutes) < 180 and tas1.time_in_minutes > ?  and tas1.direction = tas2.direction and %@ order by tas1.time_in_minutes asc limit 1";
    //removed this condition:
    //and (tas2.time_in_minutes-tas1.time_in_minutes) < 180
    //including new columns
    //tas1.start_stn_name as start_stn_name, tas1.end_stn_name as end_stn_name, tas1.end_stn_code as end_stn_code
    
    
    NSString *qry_allStations = @"select * from %@ WHERE TRAIN_ID = ? AND TIME_IN_MINUTES >= ? AND TIME_IN_MINUTES <= ? ORDER BY TIME_IN_MINUTES";
    
    for (int i=0; i < viaStationIds.count-1; i++){
        
        
        NSString *fromStationIdStr = [viaStationIds objectAtIndex:i];
        NSString *toStationIdStr = [viaStationIds objectAtIndex:(i+1)];
        NSString *lineStr = @([self.appdelegate getCommonLineIdBetweenStation:fromStationIdStr.intValue secondStationId:toStationIdStr.intValue]).stringValue;
        
        //hold a list of RouteStations for each leg of the journey.
        //All stations are included, regardless of stoppage or not.
        NSMutableArray *partJourney = [[self.appdelegate listOfStationsBetweenStation:fromStationIdStr.intValue andStation:toStationIdStr.intValue onLine:lineStr.intValue] mutableCopy];
        
        int journeyStartTime = startimeInMins;
        
        //for intermediate journeys, add 2 mins (based on First leg Flag)
        if(!journeyStart)
            journeyStartTime = startimeInMins+2;
        
        //After setting start time for first leg, set a Flag.
        if(journeyStart)
            journeyStart = !journeyStart;
        
        NSString *train_id = @"";
        
        
        
        NSArray *paramArray;
        
        if(i==0)
        {
            paramArray = @[ fromStationIdStr, toStationIdStr, [NSString stringWithFormat:@"%d", startimeInMins], @(trainId).stringValue];
        }
        else
        {
            paramArray = @[ fromStationIdStr, toStationIdStr, [NSString stringWithFormat:@"%d", startimeInMins]];
        }
        
        //        FMResultSet *rs = [self.db executeQuery:qry_train_count withArgumentsInArray:paramArray];
        //        if([rs next]){
        //            if([rs intForColumn:@"cnt"] == 0)
        //            {
        //                    [self.partJournies removeAllObjects];
        //                    return;
        //                }
        //        }
        //        [rs close];
        
        NSString *tableName = @"";
        NSString *sundayOnlyCondition = self.appdelegate.journey.departureDay == 1 ? @" tas1.NOT_ON_SUNDAY = 0 " : @" tas1.SUNDAY_ONLY = 0 ";
        
        if([lineStr isEqualToString:@"5"] || [lineStr isEqualToString:@"6"]){
            tableName = @"TSD_L56";
        }
        else if([lineStr isEqualToString:@"3"] || [lineStr isEqualToString:@"4"]){
            tableName = @"TSD_L34";
        }
        else
        {
            tableName = [NSString stringWithFormat:@"TSD_L%@",lineStr];
        }
        
        NSString *optimizedTrainQuery = [NSString stringWithFormat:qry_train,tableName,tableName, sundayOnlyCondition];
        
        //for first train, you need time EQUAL TO whatever time is specified.
        //if you dont do this, time will be >, causing you to get the next train!
        if(i==0)
        {
            optimizedTrainQuery = [NSString stringWithFormat:qry_train_fixed_time,tableName,tableName, sundayOnlyCondition];
        }
        NSString *optimizedAllTrainsQuery = [NSString stringWithFormat:qry_allStations,tableName];
        
        FMResultSet *rs = [self.appdelegate.db executeQuery:optimizedTrainQuery withArgumentsInArray:paramArray];
        //        PartJourney *partJourney = [[PartJourney alloc] init];
        
        bool queryReturnedRows = NO;
        
        while ([rs next]) {
            queryReturnedRows = YES;
            train_id = [rs stringForColumn:@"train_id"];
            journeyStartTime = [rs intForColumn:@"src_time_in_mins"];
            startimeInMins = [rs intForColumn:@"dest_time_in_mins"];
            
            
            FMResultSet *rs2 = [self.appdelegate.db executeQuery:optimizedAllTrainsQuery withArgumentsInArray:@[ train_id, [NSString stringWithFormat:@"%d", journeyStartTime] ,[NSString stringWithFormat:@"%d", startimeInMins]]];
            
            while ([rs2 next]) {
                NSString *station_id = [rs2 stringForColumn:@"station_id"];
                NSString *stationName = [self findStationNameById:station_id.intValue];
                
                NSString *speed = [rs2 stringForColumn:@"speed"];
                NSString *direction = [rs2 stringForColumn:@"direction"];
                NSString *platf_no = [rs2 stringForColumn:@"platform_no"];
                NSString *platf_side = [rs2 stringForColumn:@"platform_side"];
                int line = [rs2 intForColumn:@"line_id"];
                int no_of_car = [rs2 intForColumn:@"no_of_car"];
                if(no_of_car == -1 )
                    no_of_car = 12;
                int train_time = [rs2 intForColumn:@"time_in_minutes"];
                NSString *start_stn_name = [self findStationNameById:([rs2 stringForColumn:@"start_stn_id"]).intValue];
                NSString *end_stn_name = [self findStationNameById:([rs2 stringForColumn:@"end_stn_id"]).intValue];
                NSString *end_stn_code = [self findStationCodeById:([rs2 stringForColumn:@"end_stn_id"]).intValue];
                NSString *special_info = [rs2 stringForColumn:@"special_info"];
                
                
                NSMutableString* trainTimeStr = [NSMutableString stringWithString:[self minutesToHrsMins:train_time asFormat:@"time"]];
                
                
                for(RouteStation *station in partJourney){
                    if(station.station_id.intValue == station_id.intValue){
                        station.stationName = stationName;
                        station.timeInMins = train_time;
                        station.time = trainTimeStr;
                        station.station_id = station_id;
                        station.speed = speed;
                        station.platform_no = platf_no;
                        station.platform_side = platf_side;
                        station.line = line;
                        station.no_of_car = no_of_car;
                        station.trainStartedFromStationName = start_stn_name;
                        station.trainGoingTowardsStationName = end_stn_name;
                        station.trainGoingTowardsStationCode = end_stn_code;
                        station.trainDirection = direction;
                        station.specialInfo = [special_info isEqualToString:@"LS"]?@"Ladies Special":@"";
                        NSLog(@"Station Name : %@" ,station.stationName);
                        NSLog(@"Station time: %@",station.time);
                        break;
                    }else{
                        //no station match, find its name
                        station.stationName = [self findStationNameById:station.station_id.intValue];
                    }
                }
            }
            [rs2 close];
        }
        
        if(!queryReturnedRows){
            [tempPartJournies removeAllObjects];
            return NO;
        }
        [tempPartJournies addObject:partJourney];
        [rs close];
    }
    
    for(NSArray *eachNewPartJourney in tempPartJournies)
    {
        for (NSArray *eachPartJourney in self.partJournies)
        {
            RouteStation *oldPartJourneyStartStation = [eachPartJourney objectAtIndex:0];
            RouteStation *newPartJourneyStartStation = [eachNewPartJourney objectAtIndex:0];
            
            if([oldPartJourneyStartStation.station_id isEqualToString: newPartJourneyStartStation.station_id])
            {
                [self.partJournies replaceObjectAtIndex: [self.partJournies indexOfObject:eachPartJourney]
                                             withObject:eachNewPartJourney];
                break;
            }

        }
        
    }
    return YES;
}



-(int) minutesSinceMidnight:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    return 60 * [components hour] + [components minute];
}

-(NSString *) minutesToHrsMins:(int) mins asFormat:(NSString *) format //"hours" or "time"
{
    int hr = mins / 60 ;
    int min = mins % 60;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    comps.hour = hr;
    comps.minute = min;
    NSDate *firstPeriodDate = [calendar dateFromComponents:comps];
    dateFormatter.locale = twelveHourLocale;
    if([format isEqualToString:@"hours"]){
        [dateFormatter setDateFormat:@"hh:mm"];
    }
    else if([format isEqualToString:@"time"]){
//        [dateFormatter setDateFormat:@"hh:mm a"];
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    
    return [dateFormatter stringFromDate:firstPeriodDate];
    
}
-(NSArray *) listTrainsFromStationId:(int)fromStationId
                         toStationId:(int)toStationId
                            onLineId:(int)lineId
                        atTimeInMins:(int)timeInMins
                         inDirection:(NSString *)trainDirection
{
    NSMutableArray *mutArray = [NSMutableArray new];
    
    NSString *tableName = @"";
    if(lineId== 5 || lineId == 6 ){
        tableName = @"TSD_L56";
    }
    else if(lineId== 3 || lineId == 4 ){
        tableName = @"TSD_L34";
    }
    else
    {
        tableName = [NSString stringWithFormat:@"TSD_L%d",lineId];
    }
    
    
        NSString *qry_allTrainsEnRoute = @"select * from %@ WHERE LINE_ID = ? AND STATION_ID = ? AND (TIME_IN_MINUTES >= ? OR TIME_IN_MINUTES < ?) AND DIRECTION = ? ORDER BY TIME_IN_MINUTES";
//        NSString *qry_allTrainsEnRoute = @"select * from %@ WHERE LINE_ID = ? AND STATION_ID = ? AND (TIME_IN_MINUTES >= ?) AND DIRECTION = ? ORDER BY TIME_IN_MINUTES";
    
    NSString *optimizedAllTrainsEnRoute = [NSString stringWithFormat:qry_allTrainsEnRoute,tableName];
    
        NSArray *paramArray = @[ @(lineId).stringValue, @(fromStationId).stringValue, [NSString stringWithFormat:@"%d", timeInMins],[NSString stringWithFormat:@"%d", timeInMins], trainDirection];
//        NSArray *paramArray = @[ @(lineId).stringValue, @(fromStationId).stringValue, [NSString stringWithFormat:@"%d", timeInMins], trainDirection];
    
        FMResultSet *rs = [self.appdelegate.db executeQuery:optimizedAllTrainsEnRoute withArgumentsInArray:paramArray];
    
    BOOL immediateNextTrain = YES;
    while ([rs next]){
        StationTrain *stationTrain = [[StationTrain alloc] init];
        NSString *platf_no = [rs stringForColumn:@"platform_no"];
        NSString *platf_side = [rs stringForColumn:@"platform_side"];
        NSString *speed = [rs stringForColumn:@"speed"];
        int train_time = [rs intForColumn:@"time_in_minutes"];
        int no_of_car = [rs intForColumn:@"no_of_car"];
        if(no_of_car == -1 )
            no_of_car = 12;
        
        NSString *direction = [rs stringForColumn:@"direction"];
        NSString *start_stn_name = [self findStationNameById:([rs stringForColumn:@"start_stn_id"]).intValue];
        NSString *end_stn_name = [self findStationNameById:([rs stringForColumn:@"end_stn_id"]).intValue];
        NSString *end_stn_code = [self findStationCodeById:([rs stringForColumn:@"end_stn_id"]).intValue];
        NSString *special_info = [rs stringForColumn:@"special_info"];
        int trainId = [rs intForColumn:@"train_id"];
        
        //mark future trains
        if(train_time > timeInMins)
        {
            stationTrain.isFutureTrain = YES;
            if(immediateNextTrain){
                stationTrain.isNextTrain = YES;
                immediateNextTrain = NO;
            }
        }
        else
        {
            stationTrain.isFutureTrain = NO;
        }
        
        stationTrain.platf_no = platf_no;
        stationTrain.platf_side = platf_side;
        stationTrain.speed = speed;
        stationTrain.train_time = train_time;
        stationTrain.trainTimeStr = [self minutesToHrsMins:train_time asFormat:@"time"];
        stationTrain.direction = direction;
        stationTrain.start_stn_name = start_stn_name;
        stationTrain.end_stn_name = end_stn_name;
        stationTrain.end_stn_code = end_stn_code;
        stationTrain.special_info = special_info;
        stationTrain.noOfCar = no_of_car;
        stationTrain.lineId = lineId;
        stationTrain.currentStationId = fromStationId;
        stationTrain.trainId = trainId;
        [mutArray addObject:stationTrain];
    }
    
    [rs close];
    return [[NSArray alloc] initWithArray:mutArray];
}

@end





















