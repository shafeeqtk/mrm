//
//  StationsList.m
//  MRM
//
//  Created by Shafeeq Rahiman on 11/16/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import "StationsList.h"
#import "FMDatabase.h"
#import "Station.h"

@interface StationsList ()

@property (strong, nonatomic) FMDatabase *db;

@end

@implementation StationsList

- (NSArray *)getListOfStations
{
    return @[@"Andheri", @"Malad", @"Kurla"];
}


- (NSArray *)getListOfStations:(NSString *)filteredWithLineName
{
    return @[@"Andheri", @"Malad", @"Kurla", @"Parel"];
}

-(Station *) stationWithStationId:(int)stationId
{
    if (!_db) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"mrm_v2" ofType:@"db3"];
        _db = [FMDatabase databaseWithPath:path];
    }
    
    if (![_db open]){
        [_db open];
    }
    
    FMResultSet *rs = [self.db executeQuery:@"SELECT distinct name,station_id,line_id FROM STATION where "];
    
    return nil;
}

@end
