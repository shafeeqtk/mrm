//
//  AppDelegate.h
//  MRMDatabaseEncryptor
//
//  Created by Shafeeq Rahiman on 12/25/14.
//  Copyright (c) 2014 Glass Box Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) FMDatabase* db;


@end

